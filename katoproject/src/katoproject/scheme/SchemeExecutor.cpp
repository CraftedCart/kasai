#include "katoproject/scheme/SchemeExecutor.hpp"
#include "kato/FileUtils.hpp"
#include <boost/filesystem/fstream.hpp>
#include <iostream>

namespace KatoProject {
    SchemeExecutor::SchemeExecutor() {
        sc = s7_init();
    }

    SchemeExecutor::~SchemeExecutor() {
        free(sc);
    }

    s7_scheme* SchemeExecutor::getContext() {
        return sc;
    }

    void SchemeExecutor::executeString(const std::string &code) {
        s7_eval_c_string(sc, ("(begin " + code + ")").c_str());
    }

    bool SchemeExecutor::executeFile(const boost::filesystem::path &filePath) {
        boost::filesystem::ifstream stream;
        stream.open(filePath);
        if (stream.fail()) {
            // Failed to open the file
            // TODO: Handle failing to open file - like throwing an exception or something
            std::cerr << "Failed to open file: " << filePath.string() << std::endl;
            return false;
        }
        std::string code = Kato::FileUtils::readIfstreamToString(stream);
        stream.close();

        s7_eval_c_string(sc, ("(begin " + code + ")").c_str());
        return true;
    }

    void SchemeExecutor::addToLoadPath(const boost::filesystem::path &filePath) {
        s7_add_to_load_path(sc, filePath.string().c_str());
    }

    void SchemeExecutor::defineNativeFunction(
            const char *name,
            s7_function func,
            const char *doc,
            s7_int requiredArgs,
            s7_int optionalArgs,
            bool restArgs
            ) {
        s7_define_function(sc, name, func, requiredArgs, optionalArgs, restArgs, doc);
    }

    void SchemeExecutor::defineNativeFunctionKwargs(
            const char *name,
            s7_function func,
            const char *args,
            const char *doc
            ) {
        s7_define_function_star(sc, name, func, args, doc);
    }

    void SchemeExecutor::defineConstInteger(const char *name, int64_t num, const char *doc) {
        s7_define_constant_with_documentation(sc, name, s7_make_integer(sc, num), doc);
    }

    void SchemeExecutor::defineConstReal(const char *name, double num, const char *doc) {
        s7_define_constant_with_documentation(sc, name, s7_make_real(sc, num), doc);
    }
}
