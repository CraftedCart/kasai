#include "katoproject/scheme/KasaiSchemeDefines.hpp"
#include "katoproject/entities/BulletEntity.hpp"
#include "katoproject/entities/EnemyEntity.hpp"
#include "katoproject/KasaiWorld.hpp"
#include "kato/Math.hpp"
#include "imgui.h"
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace KatoProject::KasaiSchemeDefines {
    s7_int typeImDrawList;
    s7_int typeEnemyEntity;
    s7_int typeBulletEntity;

    void defineAllGlobal(SchemeExecutor &executor) {
        //Types
        typeImDrawList = s7_make_c_type(executor.getContext(), "imgui/draw-list");
        s7_c_type_set_equal(executor.getContext(), typeImDrawList, arePointersEqual);

        typeEnemyEntity =s7_make_c_type(executor.getContext(), "game/enemy-entity");
        s7_c_type_set_equal(executor.getContext(), typeEnemyEntity, arePointersEqual);

        typeBulletEntity = s7_make_c_type(executor.getContext(), "game/bullet-entity");
        s7_c_type_set_equal(executor.getContext(), typeBulletEntity, arePointersEqual);

        //Constants
        executor.defineConstInteger("imgui/window-flags/none", ImGuiWindowFlags_None, "no window flags");
        executor.defineConstInteger("imgui/window-flags/no-title-bar", ImGuiWindowFlags_NoTitleBar, "disable title bar");
        executor.defineConstInteger("imgui/window-flags/no-resize", ImGuiWindowFlags_NoResize, "disable user resizing with the lower-right grip");
        executor.defineConstInteger("imgui/window-flags/no-move", ImGuiWindowFlags_NoMove, "disable user moving the window");
        executor.defineConstInteger("imgui/window-flags/no-scrollbar", ImGuiWindowFlags_NoScrollbar, "disable scrollbars (window can still scroll with mouse or programatically)");
        executor.defineConstInteger("imgui/window-flags/no-scroll-with-mouse", ImGuiWindowFlags_NoScrollWithMouse, "disable user vertically scrolling with mouse wheel. On child window, mouse wheel will be forwarded to the parent unless NoScrollbar is also set.");
        executor.defineConstInteger("imgui/window-flags/no-collapse", ImGuiWindowFlags_NoCollapse, "disable user collapsing window by double-clicking on it");
        executor.defineConstInteger("imgui/window-flags/always-auto-resize", ImGuiWindowFlags_AlwaysAutoResize, "resize every window to its content every frame");
        executor.defineConstInteger("imgui/window-flags/no-background", ImGuiWindowFlags_NoBackground, "disable drawing background color (WindowBg, etc.) and outside border. Similar as using SetNextWindowBgAlpha(0.0f).");
        executor.defineConstInteger("imgui/window-flags/no-saved-settings", ImGuiWindowFlags_NoSavedSettings, "never load/save settings in .ini file");
        executor.defineConstInteger("imgui/window-flags/no-mouse-inputs", ImGuiWindowFlags_NoMouseInputs, "disable catching mouse, hovering test with pass through.");
        executor.defineConstInteger("imgui/window-flags/menu-bar", ImGuiWindowFlags_MenuBar, "has a menu-bar");
        executor.defineConstInteger("imgui/window-flags/horizontal-scrollbar", ImGuiWindowFlags_HorizontalScrollbar, "allow horizontal scrollbar to appear (off by default). You may use SetNextWindowContentSize(ImVec2(width,0.0f)); prior to calling Begin() to specify width. Read code in imgui_demo in the \"Horizontal Scrolling\" section.");
        executor.defineConstInteger("imgui/window-flags/no-focus-on-appearing", ImGuiWindowFlags_NoFocusOnAppearing, "disable taking focus when transitioning from hidden to visible state");
        executor.defineConstInteger("imgui/window-flags/no-bring-to-front-on-focus", ImGuiWindowFlags_NoBringToFrontOnFocus, "disable bringing window to front when taking focus (e.g. clicking on it or programatically giving it focus)");
        executor.defineConstInteger("imgui/window-flags/always-vertical-scrollbar", ImGuiWindowFlags_AlwaysVerticalScrollbar, "always show vertical scrollbar (even if ContentSize.y < Size.y)");
        executor.defineConstInteger("imgui/window-flags/always-horizontal-scrollbar", ImGuiWindowFlags_AlwaysHorizontalScrollbar, "always show horizontal scrollbar (even if ContentSize.x < Size.x)");
        executor.defineConstInteger("imgui/window-flags/always-use-window-padding", ImGuiWindowFlags_AlwaysUseWindowPadding, "ensure child windows without border uses style.WindowPadding (ignored by default for non-bordered child windows, because more convenient)");
        executor.defineConstInteger("imgui/window-flags/no-nav-inputs", ImGuiWindowFlags_NoNavInputs, "no gamepad/keyboard navigation within the window");
        executor.defineConstInteger("imgui/window-flags/no-nav-focus", ImGuiWindowFlags_NoNavFocus, "no focusing toward this window with gamepad/keyboard navigation (e.g. skipped by CTRL+TAB)");
        executor.defineConstInteger("imgui/window-flags/unsaved-document", ImGuiWindowFlags_UnsavedDocument, "append '*' to title without affecting the ID, as a convenience to avoid using the ### operator. When used in a tab/docking context, tab is selected on closure and closure is deferred by one frame to allow code to cancel the closure (with a confirmation popup, etc.) without flicker.");
        executor.defineConstInteger("imgui/window-flags/no-docking", ImGuiWindowFlags_NoDocking, "disable docking of this window");

        executor.defineConstInteger("imgui/window-flags/no-nav", ImGuiWindowFlags_NoNav, "");
        executor.defineConstInteger("imgui/window-flags/no-decoration", ImGuiWindowFlags_NoDecoration, "");
        executor.defineConstInteger("imgui/window-flags/no-inputs", ImGuiWindowFlags_NoInputs, "");

        executor.defineConstInteger("imgui/window-flags//nav-flattened", ImGuiWindowFlags_NavFlattened, "[BETA] Allow gamepad/keyboard navigation to cross over parent border to this child (only use on child that have no scrolling!)");
        executor.defineConstInteger("imgui/window-flags//child-window", ImGuiWindowFlags_ChildWindow, "don't use! For internal use by BeginChild()");
        executor.defineConstInteger("imgui/window-flags//tooltip", ImGuiWindowFlags_Tooltip, "don't use! For internal use by BeginTooltip()");
        executor.defineConstInteger("imgui/window-flags//popup", ImGuiWindowFlags_Popup, "don't use! For internal use by BeginPopup()");
        executor.defineConstInteger("imgui/window-flags//modal", ImGuiWindowFlags_Modal, "don't use! For internal use by BeginPopupModal()");
        executor.defineConstInteger("imgui/window-flags//child-menu", ImGuiWindowFlags_ChildMenu, "don't use! For internal use by BeginMenu()");
        executor.defineConstInteger("imgui/window-flags//dock-node-host", ImGuiWindowFlags_DockNodeHost, "don't use! For internal use by Begin()/NewFrame()");

        executor.defineConstInteger("imgui/draw-corner-flags/top-left", ImDrawCornerFlags_TopLeft, "0x1");
        executor.defineConstInteger("imgui/draw-corner-flags/top-right", ImDrawCornerFlags_TopRight, "0x2");
        executor.defineConstInteger("imgui/draw-corner-flags/bot-left", ImDrawCornerFlags_BotLeft, "0x4");
        executor.defineConstInteger("imgui/draw-corner-flags/bot-right", ImDrawCornerFlags_BotRight, "0x8");
        executor.defineConstInteger("imgui/draw-corner-flags/top", ImDrawCornerFlags_Top, "0x3");
        executor.defineConstInteger("imgui/draw-corner-flags/bot", ImDrawCornerFlags_Bot, "0xC");
        executor.defineConstInteger("imgui/draw-corner-flags/left", ImDrawCornerFlags_Left, "0x5");
        executor.defineConstInteger("imgui/draw-corner-flags/right", ImDrawCornerFlags_Right, "0xA");
        executor.defineConstInteger("imgui/draw-corner-flags/all", ImDrawCornerFlags_All, "0xF");

        executor.defineConstInteger("imgui/input-text-flags/none", ImGuiInputTextFlags_None, "no input-text flags");
        executor.defineConstInteger("imgui/input-text-flags/chars-decimal", ImGuiInputTextFlags_CharsDecimal, "allow 0123456789.+-*/");
        executor.defineConstInteger("imgui/input-text-flags/chars-hexadecimal", ImGuiInputTextFlags_CharsHexadecimal, "allow 0123456789ABCDEFabcdef");
        executor.defineConstInteger("imgui/input-text-flags/chars-uppercase", ImGuiInputTextFlags_CharsUppercase, "turn a..z into A..Z");
        executor.defineConstInteger("imgui/input-text-flags/chars-no-blank", ImGuiInputTextFlags_CharsNoBlank, "filter out spaces, tabs");
        executor.defineConstInteger("imgui/input-text-flags/auto-select-all", ImGuiInputTextFlags_AutoSelectAll, "select entire text when first taking mouse focus");
        executor.defineConstInteger("imgui/input-text-flags/enter-returns-true", ImGuiInputTextFlags_EnterReturnsTrue, "return 'true' when Enter is pressed (as opposed to when the value was modified)");
        executor.defineConstInteger("imgui/input-text-flags/callback-completion", ImGuiInputTextFlags_CallbackCompletion , "callback on pressing TAB (for completion handling)");
        executor.defineConstInteger("imgui/input-text-flags/callback-history", ImGuiInputTextFlags_CallbackHistory, "callback on pressing Up/Down arrows (for history handling)");
        executor.defineConstInteger("imgui/input-text-flags/callback-always", ImGuiInputTextFlags_CallbackAlways, "callback on each iteration. User code may query cursor position, modify text buffer.");
        executor.defineConstInteger("imgui/input-text-flags/callback-char-filter", ImGuiInputTextFlags_CallbackCharFilter , "callback on character inputs to replace or discard them. Modify 'EventChar' to replace or discard, or return 1 in callback to discard.");
        executor.defineConstInteger("imgui/input-text-flags/allow-tab-input", ImGuiInputTextFlags_AllowTabInput, "pressing TAB input a '\t' character into the text field");
        executor.defineConstInteger("imgui/input-text-flags/ctrl-enter-for-new-line", ImGuiInputTextFlags_CtrlEnterForNewLine, "in multi-line mode, unfocus with Enter, add new line with Ctrl+Enter (default is opposite: unfocus with Ctrl+Enter, add line with Enter).");
        executor.defineConstInteger("imgui/input-text-flags/no-horizontal-scroll", ImGuiInputTextFlags_NoHorizontalScroll , "disable following the cursor horizontally");
        executor.defineConstInteger("imgui/input-text-flags/always-insert-mode", ImGuiInputTextFlags_AlwaysInsertMode, "insert mode");
        executor.defineConstInteger("imgui/input-text-flags/read-only", ImGuiInputTextFlags_ReadOnly, "read-only mode");
        executor.defineConstInteger("imgui/input-text-flags/password", ImGuiInputTextFlags_Password, "password mode, display all characters as '*'");
        executor.defineConstInteger("imgui/input-text-flags/no-undo-redo", ImGuiInputTextFlags_NoUndoRedo, "disable undo/redo. Note that input text owns the text data while active, if you want to provide your own undo/redo stack you need e.g. to call ClearActiveID().");
        executor.defineConstInteger("imgui/input-text-flags/chars-scientific", ImGuiInputTextFlags_CharsScientific, "allow 0123456789.+-*/eE (Scientific notation input)");
        executor.defineConstInteger("imgui/input-text-flags/callback-resize", ImGuiInputTextFlags_CallbackResize, "callback on buffer capacity changes request (beyond 'buf_size' parameter value), allowing the string to grow. Notify when the string wants to be resized (for string types which hold a cache of their Size). You will be provided a new BufSize in the callback and NEED to honor it. (see misc/cpp/imgui_stdlib.h for an example of using this)");

        executor.defineConstInteger("imgui/input-text-flags//multiline", ImGuiInputTextFlags_Multiline, "for internal use by InputTextMultiline()");

        //Functions
        executor.defineNativeFunction(
                "fsin",
                fastSin,
                "(fsin x) fast sin",
                1
                );

        executor.defineNativeFunction(
                "fcos",
                fastCos,
                "(fcos x) fast cos",
                1
                );

        executor.defineNativeFunctionKwargs(
                "imgui/begin",
                imguiBegin,
                "name (flags imgui/window-flags/none)",
                "(imgui/begin name (flags imgui/window-flags/none)) create a new ImGui window with the title NAME and flags FLAGS"
                );

        executor.defineNativeFunction(
                "imgui/end",
                imguiEnd,
                "(imgui/end) finish adding components to the current ImGui window"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/begin-child",
                imguiBeginChild,
                "name (size '(0.0 0.0)) (border #f) (flags imgui/window-flags/none)",
                "(imgui/begin-child name (size '(0.0 0.0)) (border #f) (flags imgui/window-flags/none)) "
                "create a new ImGui window"
                );

        executor.defineNativeFunction(
                "imgui/end-child",
                imguiEndChild,
                "(imgui/end-child) finish adding components to the current ImGui child window"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/tree-node",
                imguiTreeNode,
                "name",
                "(imgui/tree-node name) "
                "return true when the node is open, in which case you need to also call (imgui/tree-pop) when you are finished "
                "displaying the tree node contents"
                );

        executor.defineNativeFunction(
                "imgui/tree-pop",
                imguiTreePop,
                "(imgui/tree-pop) finish displaying the tree node's contents"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/text",
                imguiText,
                "text",
                "(imgui/text text) add text TEXT to the GUI"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/text-wrapped",
                imguiTextWrapped,
                "text",
                "(imgui/text-wrapped text) add text TEXT to the GUI, with auto wrapping"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/button",
                imguiButton,
                "text (size '(0.0 0.0))",
                "(imgui/button text (size '(0.0 0.0)) add a button labeled TEXT with an optional size of SIZE to the GUI and "
                "return #t if the button was clicked, #f otherwise"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/small-button",
                imguiSmallButton,
                "text",
                "(imgui/small-button text (size '(0.0 0.0)) add a button labeled TEXT to the GUI and "
                "return #t if the button was clicked, #f otherwise"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/input-text",
                imguiInputText,
                "label text (flags imgui/input-text-flags/none)",
                "(imgui/input-text label text (flags imgui/input-text-flags/none)) "
                "add a text input field containing TEXT and return a cons cell where car is the new value of text and cdr is the boolean returned "
                "by the ImGui InputText"
                );

        executor.defineNativeFunction(
                "imgui/get-cursor-screen-pos",
                imguiGetCursorScreenPos,
                "(imgui/get-cursor-screen-pos) return a list of 2 elements: the X and Y position of the ImGui cursor"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/set-next-window-pos",
                imguiSetNextWindowPos,
                "pos",
                "(imgui/set-next-window-pos pos) set the position of the next window"
                );

        executor.defineNativeFunction(
                "imgui/get-window-draw-list",
                imguiGetWindowDrawList,
                "(imgui/get-window-draw-list) get draw list associated to the current window, to append your own drawing primitives"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/draw-list/add-line",
                imguiDrawListAddLine,
                "draw-list a b col (thickness 1.0)",
                "(imgui/draw-list/add-line draw-list a b col (thickness 1.0)) add a rectangle to the draw list DRAW-LIST\n"
                "A is the top-left point\n"
                "B is the bottom-right point\n"
                "ROUNDING is a number of how rounded the window corners should be\n"
                "ROUNDING-CORNERS-FLAGS are the flags dictating which corners should be rounded\n"
                "THICKNESS is how thick the outline of the rect should be"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/draw-list/add-rect", imguiDrawListAddRect,
                "draw-list a b col (rounding 0.0) (rounding-corners-flags imgui/draw-corner-flags/all) (thickness 1.0)",
                "(imgui/draw-list/add-rect draw-list a b col (rounding 0.0) (rounding-corners-flags imgui/draw-corner-flags/all) (thickness 1.0)) "
                "add a rectangle to the draw list DRAW-LIST\n"
                "A is the top-left point\n"
                "B is the bottom-right point\n"
                "ROUNDING is a number of how rounded the window corners should be\n"
                "ROUNDING-CORNERS-FLAGS are the flags dictating which corners should be rounded\n"
                "THICKNESS is how thick the outline of the rect should be"
                );

        executor.defineNativeFunctionKwargs(
                "imgui/draw-list/add-rect-filled",
                imguiDrawListAddRectFilled,
                "draw-list a b col (rounding 0.0) (rounding-corners-flags imgui/draw-corner-flags/all)",
                "(imgui/draw-list/add-rect-filled draw-list a b col (rounding 0.0) (rounding-corners-flags imgui/draw-corner-flags/all)) "
                "add a rectangle to the draw list DRAW-LIST\n"
                "A is the top-left point\n"
                "B is the bottom-right point\n"
                "ROUNDING is a number of how rounded the window corners should be\n"
                "ROUNDING-CORNERS-FLAGS are the flags dictating which corners should be rounded"
                );

        executor.defineNativeFunctionKwargs(
                "entity/get-position",
                entityGetPosition,
                "entity",
                "(entity/get-position entity) "
                "get the position of ENTITY as a list of 2 numbers"
                );

        executor.defineNativeFunctionKwargs(
                "entity/add-position",
                entityAddPosition,
                "entity delta",
                "(entity/add-position entity delta) "
                "moves ENTITY's position by DELTA\n"
                "ENTITY is an entity\n"
                "DELTA is a list of 2 elements"
                );

        executor.defineNativeFunctionKwargs(
                "entity/destroy",
                entityDestroy,
                "entity",
                "(entity/destroy entity) "
                "remove ENTITY from the world"
                );

        executor.defineNativeFunctionKwargs(
                "entity/enemy/was-player-killed?",
                entityEnemyWasPlayerKilled,
                "entity",
                "(entity/enemy/was-player-killed? entity) "
                "return a Boolean stating whether the entity was player killed"
                );

        executor.defineNativeFunction(
                "world/get-all-enemy-bullet-entities",
                worldGetAllEnemyBulletEntities,
                "(world/get-all-enemy-bullet-entities) return a list of all enemy bullet entities"
                );

        executor.defineNativeFunction(
                "engine/is-using-editor?",
                engineIsUsingEditor,
                "(engine/is-using-editor?) return whether the game is in editor mode"
                );

        executor.defineNativeFunction(
                "engine/request-quit",
                engineRequestQuit,
                "(engine/request-quit) request to quit the application"
                );

        executor.defineNativeFunctionKwargs(
                "vec2/add",
                vec2Add,
                "a b",
                "(vec2/add a b) add A to B and return the result\n"
                "A and B are lists of 2 numbers"
                );

        executor.defineNativeFunctionKwargs(
                "string-contains?",
                stringContains,
                "str contained",
                "(string-contains? str contained) return whether CONTAINED is a substring of STR"
                );
    }

    bool arePointersEqual(void *a, void *b) {
        return a == b;
    }

    s7_pointer fastSin(s7_scheme *sc, s7_pointer args) {
        s7_pointer val = s7_car(args);
        if (s7_is_number(val)) {
            double x = s7_number_to_real(sc, val);
            return s7_make_real(sc, Kato::Math::fastSin(x));
        }

        return s7_wrong_type_arg_error(sc, "fsin", 1, val, "a number");
    }

    s7_pointer fastCos(s7_scheme *sc, s7_pointer args) {
        s7_pointer val = s7_car(args);
        if (s7_is_number(val)) {
            double x = s7_number_to_real(sc, val);
            return s7_make_real(sc, Kato::Math::fastCos(x));
        }

        return s7_wrong_type_arg_error(sc, "fcos", 1, val, "a number");
    }

    s7_pointer imguiBegin(s7_scheme *sc, s7_pointer args) {
        s7_pointer name = s7_car(args);
        s7_pointer flags = s7_cadr(args);

        //Arg checking
        if (!s7_is_string(name)) {
            return s7_wrong_type_arg_error(sc, "imgui/begin", 1, name, "a string");
        } else if (!s7_is_integer(flags)) {
          return s7_wrong_type_arg_error(sc, "imgui/begin", 2, flags, "an integer");
        }

        ImGui::Begin(s7_string(name), nullptr, s7_integer(flags));
        return s7_nil(sc);
    }

    s7_pointer imguiEnd(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        ImGui::End();
        return s7_nil(sc);
    }

    s7_pointer imguiBeginChild(s7_scheme *sc, s7_pointer args) {
        s7_pointer name = s7_car(args);
        s7_pointer size = s7_cadr(args);
        s7_pointer border = s7_caddr(args);
        s7_pointer flags = s7_cadddr(args);

        ImVec2 sizeVec;

        //Arg checking
        if (!s7_is_string(name)) {
            return s7_wrong_type_arg_error(sc, "imgui/begin-child", 1, name, "a string");
        } else if (!listToVec2(sc, size, sizeVec.x, sizeVec.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/begin-child", 2, size, "a list of 2 numbers");
        } else if (!s7_is_boolean(border)) {
            return s7_wrong_type_arg_error(sc, "imgui/begin-child", 3, border, "a boolean");
        } else if (!s7_is_integer(flags)) {
            return s7_wrong_type_arg_error(sc, "imgui/begin-child", 4, flags, "an integer");
        }

        bool ret = ImGui::BeginChild(s7_string(name), sizeVec, s7_boolean(sc, border), s7_integer(flags));
        return s7_make_boolean(sc, ret);
    }

    s7_pointer imguiEndChild(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        ImGui::EndChild();
        return s7_nil(sc);
    }

    s7_pointer imguiTreeNode(s7_scheme *sc, s7_pointer args) {
        s7_pointer name = s7_car(args);

        //Arg checking
        if (!s7_is_string(name)) {
            return s7_wrong_type_arg_error(sc, "imgui/tree-node", 1, name, "a string");
        }

        bool ret = ImGui::TreeNode(s7_string(name));
        return s7_make_boolean(sc, ret);
    }

    s7_pointer imguiTreePop(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        ImGui::TreePop();
        return s7_nil(sc);
    }

    s7_pointer imguiText(s7_scheme *sc, s7_pointer args) {
        s7_pointer text = s7_car(args);

        //Arg checking
        if (!s7_is_string(text)) {
            return s7_wrong_type_arg_error(sc, "imgui/text", 1, text, "a string");
        }

        ImGui::Text("%s", s7_string(text));
        return s7_nil(sc);
    }

    s7_pointer imguiTextWrapped(s7_scheme *sc, s7_pointer args) {
        s7_pointer text = s7_car(args);

        //Arg checking
        if (!s7_is_string(text)) {
            return s7_wrong_type_arg_error(sc, "imgui/text-wrapped", 1, text, "a string");
        }

        ImGui::TextWrapped("%s", s7_string(text));
        return s7_nil(sc);
    }

    s7_pointer imguiButton(s7_scheme *sc, s7_pointer args) {
        s7_pointer text = s7_car(args);
        s7_pointer size = s7_cadr(args);

        ImVec2 sizeVec;

        //Arg checking
        if (!s7_is_string(text)) {
            return s7_wrong_type_arg_error(sc, "imgui/button", 1, text, "a string");
        } else if (!listToVec2(sc, size, sizeVec.x, sizeVec.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/button", 2, size, "a list of 2 numbers");
        }

        return s7_make_boolean(sc, ImGui::Button(s7_string(text), sizeVec));
    }

    s7_pointer imguiSmallButton(s7_scheme *sc, s7_pointer args) {
        s7_pointer text = s7_car(args);

        //Arg checking
        if (!s7_is_string(text)) {
            return s7_wrong_type_arg_error(sc, "imgui/small-button", 1, text, "a string");
        }

        return s7_make_boolean(sc, ImGui::SmallButton(s7_string(text)));
    }

    s7_pointer imguiInputText(s7_scheme *sc, s7_pointer args) {
        s7_pointer label = s7_car(args);
        s7_pointer text = s7_cadr(args);
        s7_pointer flags = s7_caddr(args);

        //Arg checking
        if (!s7_is_string(label)) {
            return s7_wrong_type_arg_error(sc, "imgui/input-text", 1, label, "a string");
        } else if (!s7_is_string(text)) {
            return s7_wrong_type_arg_error(sc, "imgui/input-text", 2, text, "a string");
        } else if (!s7_is_integer(flags)) {
            return s7_wrong_type_arg_error(sc, "imgui/input-text", 2, text, "an integer");
        }

        char buffer[512];
        strcpy(buffer, s7_string(text));
        bool ret = ImGui::InputText(s7_string(label), buffer, 512, s7_integer(flags));

        return s7_cons(sc, s7_make_string(sc, buffer), s7_make_boolean(sc, ret));
    }

    s7_pointer imguiGetCursorScreenPos(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        ImVec2 pos = ImGui::GetCursorScreenPos();
        return s7_list(sc, 2, s7_make_real(sc, pos.x), s7_make_real(sc, pos.y));
    }

    s7_pointer imguiSetNextWindowPos(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        s7_pointer pos = s7_car(args);

        ImVec2 posVec;

        //Arg checking
        if (!listToVec2(sc, pos, posVec.x, posVec.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/set-next-window-pos", 1, pos, "a list of 2 numbers");
        }

        ImGui::SetNextWindowPos(posVec);

        return s7_nil(sc);
    }

    s7_pointer imguiGetWindowDrawList(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        return s7_make_c_object(sc, typeImDrawList, ImGui::GetWindowDrawList());
    }

    s7_pointer imguiDrawListAddLine(s7_scheme *sc, s7_pointer args) {
        s7_pointer drawListPtr = s7_car(args);
        s7_pointer aPtr = s7_cadr(args);
        s7_pointer bPtr = s7_caddr(args);
        s7_pointer colPtr = s7_cadddr(args);
        s7_pointer thicknessPtr = s7_cadddr(s7_cdr(args));

        ImVec2 a;
        ImVec2 b;
        uint8_t colR;
        uint8_t colG;
        uint8_t colB;
        uint8_t colA;

        //Arg checking
        if (!(s7_is_c_object(drawListPtr) && s7_c_object_type(drawListPtr) == typeImDrawList)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-line", 1, drawListPtr, "an imgui/draw-list");
        }  else if (!listToVec2(sc, aPtr, a.x, a.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-line", 2, aPtr, "a list of 2 numbers");
        } else if (!listToVec2(sc, bPtr, b.x, b.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-line", 3, bPtr, "a list of 2 numbers");
        } else if (!listToUInt8Vec4(sc, colPtr, colR, colG, colB, colA)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-line", 4, colPtr, "a list of 4 integers");
        } else if (!s7_is_number(thicknessPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-line", 5, thicknessPtr, "a number");
        }

        ImDrawList *drawList = static_cast<ImDrawList*>(s7_c_object_value(drawListPtr));
        ImU32 col = IM_COL32(colR, colG, colB, colA);
        float thickness = s7_number_to_real(sc, thicknessPtr);

        drawList->AddLine(a, b, col, thickness);

        return s7_nil(sc);
    }

    s7_pointer imguiDrawListAddRect(s7_scheme *sc, s7_pointer args) {
        s7_pointer drawListPtr = s7_car(args);
        s7_pointer aPtr = s7_cadr(args);
        s7_pointer bPtr = s7_caddr(args);
        s7_pointer colPtr = s7_cadddr(args);
        s7_pointer roundingPtr = s7_cadddr(s7_cdr(args));
        s7_pointer roundingFlagsPtr = s7_cadddr(s7_cddr(args));
        s7_pointer thicknessPtr = s7_cadddr(s7_cdddr(args));

        ImVec2 a;
        ImVec2 b;
        uint8_t colR;
        uint8_t colG;
        uint8_t colB;
        uint8_t colA;

        //Arg checking
        if (!(s7_is_c_object(drawListPtr) && s7_c_object_type(drawListPtr) == typeImDrawList)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 1, drawListPtr, "an imgui/draw-list");
        }  else if (!listToVec2(sc, aPtr, a.x, a.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 2, aPtr, "a list of 2 numbers");
        } else if (!listToVec2(sc, bPtr, b.x, b.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 3, bPtr, "a list of 2 numbers");
        } else if (!listToUInt8Vec4(sc, colPtr, colR, colG, colB, colA)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 4, colPtr, "a list of 4 integers");
        } else if (!s7_is_number(roundingPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 5, roundingPtr, "a number");
        } else if (!s7_is_integer(roundingFlagsPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 6, roundingFlagsPtr, "an integer");
        } else if (!s7_is_number(thicknessPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect", 7, thicknessPtr, "a number");
        }

        ImDrawList *drawList = static_cast<ImDrawList*>(s7_c_object_value(drawListPtr));
        ImU32 col = IM_COL32(colR, colG, colB, colA);
        float rounding = s7_number_to_real(sc, roundingPtr);
        int roundingFlags = s7_integer(roundingFlagsPtr);
        float thickness = s7_number_to_real(sc, thicknessPtr);

        drawList->AddRect(a, b, col, rounding, roundingFlags, thickness);

        return s7_nil(sc);
    }

    s7_pointer imguiDrawListAddRectFilled(s7_scheme *sc, s7_pointer args) {
        s7_pointer drawListPtr = s7_car(args);
        s7_pointer aPtr = s7_cadr(args);
        s7_pointer bPtr = s7_caddr(args);
        s7_pointer colPtr = s7_cadddr(args);
        s7_pointer roundingPtr = s7_cadddr(s7_cdr(args));
        s7_pointer roundingFlagsPtr = s7_cadddr(s7_cddr(args));

        ImVec2 a;
        ImVec2 b;
        uint8_t colR;
        uint8_t colG;
        uint8_t colB;
        uint8_t colA;

        //Arg checking
        if (!(s7_is_c_object(drawListPtr) && s7_c_object_type(drawListPtr) == typeImDrawList)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 1, drawListPtr, "an imgui/draw-list");
        }  else if (!listToVec2(sc, aPtr, a.x, a.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 2, aPtr, "a list of 2 numbers");
        } else if (!listToVec2(sc, bPtr, b.x, b.y)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 3, bPtr, "a list of 2 numbers");
        } else if (!listToUInt8Vec4(sc, colPtr, colR, colG, colB, colA)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 4, colPtr, "a list of 4 integers");
        } else if (!s7_is_number(roundingPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 5, roundingPtr, "a number");
        } else if (!s7_is_integer(roundingFlagsPtr)) {
            return s7_wrong_type_arg_error(sc, "imgui/draw-list/add-rect-filled", 6, roundingFlagsPtr, "an integer");
        }

        ImDrawList *drawList = static_cast<ImDrawList*>(s7_c_object_value(drawListPtr));
        ImU32 col = IM_COL32(colR, colG, colB, colA);
        float rounding = s7_number_to_real(sc, roundingPtr);
        int roundingFlags = s7_integer(roundingFlagsPtr);

        drawList->AddRectFilled(a, b, col, rounding, roundingFlags);

        return s7_nil(sc);
    }

    s7_pointer entityGetPosition(s7_scheme *sc, s7_pointer args) {
        s7_pointer ent = s7_car(args);

        //Arg checking
        if (!(s7_is_c_object(ent) && isEntity(ent))) {
            return s7_wrong_type_arg_error(sc, "entity/get-position", 1, ent, "an entity");
        }

        Kato::Entity *entity = static_cast<Kato::Entity*>(s7_c_object_value(ent));
        const glm::vec3 &pos = entity->getTransform().getPosition();

        return s7_list(sc, 2, s7_make_real(sc, pos.x), s7_make_real(sc, pos.y));
    }

    s7_pointer entityAddPosition(s7_scheme *sc, s7_pointer args) {
        s7_pointer ent = s7_car(args);
        s7_pointer delta = s7_cadr(args);

        glm::vec3 deltaVec = glm::vec3();

        //Arg checking
        if (!(s7_is_c_object(ent) && isEntity(ent))) {
            return s7_wrong_type_arg_error(sc, "entity/add-position", 1, ent, "an entity");
        } else if (!listToVec2(sc, delta, deltaVec.x, deltaVec.y)) {
            return s7_wrong_type_arg_error(sc, "entity/add-position", 2, delta, "a list of 2 numbers");
        }

        Kato::Entity *entity = static_cast<Kato::Entity*>(s7_c_object_value(ent));
        entity->getTransform().setPosition(entity->getTransform().getPosition() + deltaVec);

        return s7_nil(sc);
    }

    s7_pointer entityDestroy(s7_scheme *sc, s7_pointer args) {
        s7_pointer ent = s7_car(args);

        //Arg checking
        if (!(s7_is_c_object(ent) && isEntity(ent))) {
          return s7_wrong_type_arg_error(sc, "entity/destroy", 1, ent, "an entity");
        }

        Kato::Entity *entity = static_cast<Kato::Entity*>(s7_c_object_value(ent));
        KasaiWorld *world = static_cast<KasaiWorld*>(Kato::EngineInstance::getInstance()->getProject()->getWorld());
        world->entitiesToErase.insert(entity);

        return s7_nil(sc);
    }

    s7_pointer entityEnemyWasPlayerKilled(s7_scheme *sc, s7_pointer args) {
        s7_pointer ent = s7_car(args);

        //Arg checking
        if (!(s7_is_c_object(ent) && s7_c_object_type(ent) == typeEnemyEntity)) {
            return s7_wrong_type_arg_error(sc, "entity/enemy/was-player-killed?", 1, ent, "an enemy entity");
        }

        EnemyEntity *entity = static_cast<EnemyEntity*>(s7_c_object_value(ent));

        return s7_make_boolean(sc, entity->wasPlayerKilled());
    }

    s7_pointer worldGetAllEnemyBulletEntities(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        Kato::World *world = Kato::EngineInstance::getInstance()->getProject()->getWorld();

        s7_pointer list = s7_nil(sc);

        for (Kato::Entity *ent : world->getRootEntity()->getChildren()) {
            if (BulletEntity *bulletEnt = dynamic_cast<BulletEntity*>(ent)) {
                if (!bulletEnt->isFriendly()) {
                    list = s7_cons(sc, s7_make_c_object(sc, typeBulletEntity, bulletEnt), list);
                }
            }
        }

        return list;
    }

    s7_pointer engineIsUsingEditor(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        return s7_make_boolean(sc, Kato::EngineInstance::getInstance()->isUsingEditor());
    }

    s7_pointer engineRequestQuit(s7_scheme *sc, [[maybe_unused]] s7_pointer args) {
        Kato::EngineInstance::getInstance()->requestQuit();
        return s7_nil(sc);
    }

    s7_pointer vec2Add(s7_scheme *sc, s7_pointer args) {
        //TODO: Varags?
        s7_pointer aPtr = s7_car(args);
        s7_pointer bPtr = s7_cadr(args);

        glm::vec2 a;
        glm::vec2 b;

        //Arg checking
        if (!listToVec2(sc, aPtr, a.x, a.y)) {
            return s7_wrong_type_arg_error(sc, "vec2/add", 1, aPtr, "a list of 2 numbers");
        } else if (!listToVec2(sc, bPtr, b.x, b.y)) {
            return s7_wrong_type_arg_error(sc, "vec2/add", 2, bPtr, "a list of 2 numbers");
        }

        glm::vec2 added = a + b;

        return s7_list(sc, 2, s7_make_real(sc, added.x), s7_make_real(sc, added.y));
    }

    s7_pointer stringContains(s7_scheme *sc, s7_pointer args) {
        s7_pointer strPtr = s7_car(args);
        s7_pointer substrPtr = s7_cadr(args);

        //Arg checking
        if (!s7_is_string(strPtr)) {
            return s7_wrong_type_arg_error(sc, "string-contains?", 1, strPtr, "a list of 2 numbers");
        } else if (!s7_is_string(substrPtr)) {
            return s7_wrong_type_arg_error(sc, "string-contains?", 2, substrPtr, "a list of 2 numbers");
        }

        return s7_make_boolean(sc, strstr(s7_string(strPtr), s7_string(substrPtr)) != nullptr);
    }

    bool listToVec2(s7_scheme *sc, s7_pointer list, float &outX, float &outY) {
        if (!s7_is_proper_list(sc, list) || s7_list_length(sc, list) != 2) return false;

        s7_pointer xPtr = s7_car(list);
        s7_pointer yPtr = s7_cadr(list);

        if (!(s7_is_number(xPtr) && s7_is_number(yPtr))) return false;

        outX = s7_number_to_real(sc, xPtr);
        outY = s7_number_to_real(sc, yPtr);

        return true;
    }

    bool listToUInt8Vec4(
            s7_scheme *sc,
            s7_pointer list,
            uint8_t &outX,
            uint8_t &outY,
            uint8_t &outZ,
            uint8_t &outW
            ) {
        if (!s7_is_proper_list(sc, list) || s7_list_length(sc, list) != 4) return false;

        s7_pointer xPtr = s7_car(list);
        s7_pointer yPtr = s7_cadr(list);
        s7_pointer zPtr = s7_caddr(list);
        s7_pointer wPtr = s7_cadddr(list);

        if (!(s7_is_integer(xPtr) && s7_is_integer(yPtr) && s7_is_integer(zPtr) && s7_is_integer(wPtr))) return false;

        outX = s7_integer(xPtr);
        outY = s7_integer(yPtr);
        outZ = s7_integer(zPtr);
        outW = s7_integer(wPtr);

        return true;
    }

    bool isEntity(s7_pointer obj) {
        s7_int type = s7_c_object_type(obj);
        return type == typeEnemyEntity ||
            type == typeBulletEntity;
    }
}
