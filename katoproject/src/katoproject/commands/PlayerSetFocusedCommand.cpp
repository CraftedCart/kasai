#include "katoproject/commands/PlayerSetFocusedCommand.hpp"

namespace KatoProject {
    PlayerSetFocusedCommand::PlayerSetFocusedCommand(
            PlayerEntity &playerEnt,
            bool focused
            ) :
        playerEnt(playerEnt),
        focused(focused) {}

    void PlayerSetFocusedCommand::execute() {
        playerEnt.setFocused(focused);
    }
}
