#include "katoproject/commands/PlayerMoveDeltaCommand.hpp"

namespace KatoProject {
    PlayerMoveDeltaCommand::PlayerMoveDeltaCommand(
            PlayerEntity &playerEnt,
            const glm::vec3 &direction
            ) :
        playerEnt(playerEnt),
        direction(direction) {}

    void PlayerMoveDeltaCommand::execute() {
      playerEnt.getTransform().setPosition(
              glm::clamp(
                      playerEnt.getTransform().getPosition() + direction * (playerEnt.isFocused() ? 1.5f : 4.0f),
                      glm::vec3(4.0f, 4.0f, 0.0f),
                      glm::vec3(596.0f, 696.0f, 0.0f)
                      )
              );
    }
}
