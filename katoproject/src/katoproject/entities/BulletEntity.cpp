#include "katoproject/entities/BulletEntity.hpp"
#include "katoproject/scheme/KasaiSchemeDefines.hpp"

namespace KatoProject {
    BulletEntity::BulletEntity(
            const std::string &name,
            SchemeExecutor &schemeExec,
            bool friendly
            ) :
        StaticMeshEntity(name),
        schemeExec(schemeExec),
        friendly(friendly) {}

    void BulletEntity::tick([[maybe_unused]] float deltaSeconds) {
        s7_scheme *sc = schemeExec.getContext();

        s7_call_with_location(
                sc,
                schemeTickFunc,
                s7_list(sc, 1, s7_make_c_object(sc, KasaiSchemeDefines::typeBulletEntity, this)),
                name.c_str(),
                "BulletEntity.cpp",
                __LINE__ - 6
                );
    }

    BulletEntity::~BulletEntity() {
        if (this->schemeTickFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeTickFuncGcLoc);
        }
    }

    float BulletEntity::getCollisionRadius() {
        return collisionRadius;
    }

    void BulletEntity::setSchemeTickFunc(s7_pointer schemeTickFunc) {
        if (this->schemeTickFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeTickFuncGcLoc);
        }

        this->schemeTickFunc = schemeTickFunc;
        schemeTickFuncGcLoc = s7_gc_protect(schemeExec.getContext(), schemeTickFunc);
    }

    bool BulletEntity::isFriendly() {
        return friendly;
    }
}
