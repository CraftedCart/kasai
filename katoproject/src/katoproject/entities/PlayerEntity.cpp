#include "katoproject/entities/PlayerEntity.hpp"
#include "katoproject/entities/BulletEntity.hpp"
#include "katoproject/KasaiWorld.hpp"
#include "kato/EngineInstance.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

namespace KatoProject {
    PlayerEntity::PlayerEntity(
            const std::string &name,
            Kato::MaterialAsset *playerMaterial,
            Kato::MaterialAsset *hitboxMaterial
            ) : Kato::Entity(name) {
        Kato::EngineInstance &engine = *Kato::EngineInstance::getInstance();
        Kato::MaterialParameter tintParam = {"tint", Kato::VEC4, {glm::vec4(1.5f, 1.5f, 1.5f, 1.0f)}};

        //Spawn our player
        meshEnt = new Kato::StaticMeshEntity("meshEnt");
        meshEnt->setStaticMeshAsset(engine.getRenderManager().planeStaticMesh);
        meshEnt->setOverriddenMaterialAsset(playerMaterial);
        meshEnt->getTransform().setScale(glm::vec3(90.0f, 90.0f, 1.0f));
        meshEnt->addMaterialParameter(tintParam);
        meshEnt->setRenderLayer(20);
        addChild(meshEnt);

        //Spawn our player hitbox indicator
        visualHitboxEnt = new Kato::StaticMeshEntity("visualHitboxEnt");
        visualHitboxEnt->setStaticMeshAsset(engine.getRenderManager().planeStaticMesh);
        visualHitboxEnt->setOverriddenMaterialAsset(hitboxMaterial);
        visualHitboxEnt->addMaterialParameter(tintParam);
        visualHitboxEnt->getTransform().setScale(glm::vec3(0.0f)); //HACK: Hacky way of making this invis
        visualHitboxEnt->setRenderLayer(25);
        meshEnt->addChild(visualHitboxEnt);
    }

    void PlayerEntity::setFocused(bool isFocused) {
        focused = isFocused;
        if (isFocused) {
            visualHitboxEnt->getTransform().setScale(glm::vec3(1.0f));
        } else {
            visualHitboxEnt->getTransform().setScale(glm::vec3(0.0f)); //HACK: Hacky way of making this invis
        }
    }

    void PlayerEntity::tick([[maybe_unused]] float deltaSeconds) {
        hitboxRot += 0.003f;
        shootCooldown--;

        visualHitboxEnt->getTransform().setRotationQuat(glm::angleAxis(hitboxRot, glm::vec3(0.0f, 0.0f, 1.0f)));

        if (shootCooldown == 0) {
            shootCooldown = 8;

            //Spawn a new bullet
            KasaiWorld *world = static_cast<KasaiWorld*>(Kato::EngineInstance::getInstance()->getProject()->getWorld());
            BulletEntity *ent = new BulletEntity("#BulletEntity#:friendly", world->getSchemeExecutor(), true);
            ent->setStaticMeshAsset(Kato::EngineInstance::getInstance()->getRenderManager().planeStaticMesh);
            ent->setOverriddenMaterialAsset(world->playerBulletMaterial);
            ent->getTransform().setPosition(getTransform().getPosition());
            ent->getTransform().setScale(glm::vec3(10.0f, 70.0f, 1.0f));
            ent->setSchemeTickFunc(s7_name_to_value(world->getSchemeExecutor().getContext(), "player-bullet-tick"));
            Kato::MaterialParameter tintParam = {"tint", Kato::VEC4, {glm::vec4(4.0f, 4.0f, 4.0f, 1.0f)}};
            ent->addMaterialParameter(tintParam);
            ent->setRenderLayer(30);
            world->addEntity(ent);

            //Spawn a new bullet (right)
            ent = new BulletEntity("#BulletEntity#:friendly", world->getSchemeExecutor(), true);
            ent->setStaticMeshAsset(Kato::EngineInstance::getInstance()->getRenderManager().planeStaticMesh);
            ent->setOverriddenMaterialAsset(world->playerBulletMaterial);
            ent->getTransform().setPosition(getTransform().getPosition());
            ent->getTransform().setRotationQuat(glm::angleAxis(-0.0523599f, glm::vec3(0.0f, 0.0f, 1.0f)));
            ent->getTransform().setScale(glm::vec3(10.0f, 70.0f, 1.0f));
            ent->setSchemeTickFunc(s7_name_to_value(world->getSchemeExecutor().getContext(), "player-bullet-right-tick"));
            ent->addMaterialParameter(tintParam);
            ent->setRenderLayer(30);
            world->addEntity(ent);

            //Spawn a new bullet (left)
            ent = new BulletEntity("#BulletEntity#:friendly", world->getSchemeExecutor(), true);
            ent->setStaticMeshAsset(Kato::EngineInstance::getInstance()->getRenderManager().planeStaticMesh);
            ent->setOverriddenMaterialAsset(world->playerBulletMaterial);
            ent->getTransform().setPosition(getTransform().getPosition());
            ent->getTransform().setRotationQuat(glm::angleAxis(0.0523599f, glm::vec3(0.0f, 0.0f, 1.0f)));
            ent->getTransform().setScale(glm::vec3(10.0f, 70.0f, 1.0f));
            ent->setSchemeTickFunc(s7_name_to_value(world->getSchemeExecutor().getContext(), "player-bullet-left-tick"));
            ent->addMaterialParameter(tintParam);
            ent->setRenderLayer(30);
            world->addEntity(ent);
        }
    }

    bool PlayerEntity::isFocused() {
        return focused;
    }

    Kato::StaticMeshEntity* PlayerEntity::getMeshEntity() {
        return meshEnt;
    }
}
