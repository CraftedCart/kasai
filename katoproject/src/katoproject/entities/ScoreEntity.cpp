#include "katoproject/entities/ScoreEntity.hpp"
#include "katoproject/KasaiWorld.hpp"

namespace KatoProject {
    ScoreEntity::ScoreEntity(const std::string &name) : StaticMeshEntity(name) {}

    void ScoreEntity::tick([[maybe_unused]] float deltaSeconds) {
        KasaiWorld *world = static_cast<KasaiWorld*>(Kato::EngineInstance::getInstance()->getProject()->getWorld());

        glm::vec3 direction = world->playerEnt->getTransform().getPosition() - getTransform().getPosition();

        if (fabs(direction.x + direction.y) < 5.5f) {
            world->addScore(10);
            world->entitiesToErase.insert(this);
        }

        direction = glm::normalize(direction);
        getTransform().setPosition(getTransform().getPosition() + direction * 5.0f);
    }
}
