#include "katoproject/entities/EnemyEntity.hpp"
#include "katoproject/scheme/KasaiSchemeDefines.hpp"
#include "katoproject/KasaiWorld.hpp"
#include "kato/EngineInstance.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

namespace KatoProject {
    EnemyEntity::EnemyEntity(
            const std::string &name,
            Kato::MaterialAsset *playerMaterial,
            SchemeExecutor &schemeExec
            ) :
        Kato::Entity(name),
        schemeExec(schemeExec) {
        Kato::EngineInstance &engine = *Kato::EngineInstance::getInstance();
        Kato::MaterialParameter tintParam = {"tint", Kato::VEC4, {glm::vec4(1.5f, 1.5f, 1.5f, 1.0f)}};

        //Spawn our player
        meshEnt = new Kato::StaticMeshEntity("meshEnt");
        meshEnt->setStaticMeshAsset(engine.getRenderManager().planeStaticMesh);
        meshEnt->setOverriddenMaterialAsset(playerMaterial);
        meshEnt->getTransform().setScale(glm::vec3(128.0f, 128.0f, 1.0f));
        meshEnt->addMaterialParameter(tintParam);
        meshEnt->setRenderLayer(10);
        addChild(meshEnt);
    }

    EnemyEntity::~EnemyEntity() {
        s7_scheme *sc = schemeExec.getContext();
        if (schemeDestroyFunc != nullptr) {
            s7_call_with_location(
                    sc,
                    schemeDestroyFunc,
                    s7_list(sc, 1, s7_make_c_object(sc, KasaiSchemeDefines::typeEnemyEntity, this)),
                    name.c_str(),
                    "EnemyEntity.cpp",
                    __LINE__ - 6
                    );
        }

        if (this->schemeTickFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeTickFuncGcLoc);
        }

        if (this->schemeDestroyFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeDestroyFuncGcLoc);
        }
    }

    void EnemyEntity::tick([[maybe_unused]] float deltaSeconds) {
        s7_scheme *sc = schemeExec.getContext();

        s7_call_with_location(
                sc,
                schemeTickFunc,
                s7_list(sc, 1, s7_make_c_object(sc, KasaiSchemeDefines::typeEnemyEntity, this)),
                name.c_str(),
                "EnemyEntity.cpp",
                __LINE__ - 6
                );
    }

    Kato::StaticMeshEntity* EnemyEntity::getMeshEntity() {
        return meshEnt;
    }

    void EnemyEntity::setSchemeTickFunc(s7_pointer schemeTickFunc) {
        if (this->schemeTickFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeTickFuncGcLoc);
        }

        this->schemeTickFunc = schemeTickFunc;
        schemeTickFuncGcLoc = s7_gc_protect(schemeExec.getContext(), schemeTickFunc);
    }

    void EnemyEntity::setSchemeDestroyFunc(s7_pointer schemeDestroyFunc) {
        if (this->schemeDestroyFunc != nullptr) {
            s7_gc_unprotect_at(schemeExec.getContext(), schemeDestroyFuncGcLoc);
        }

        this->schemeDestroyFunc = schemeDestroyFunc;
        schemeDestroyFuncGcLoc = s7_gc_protect(schemeExec.getContext(), schemeDestroyFunc);
    }

    bool EnemyEntity::damage(float value) {
        KasaiWorld *world = static_cast<KasaiWorld*>(Kato::EngineInstance::getInstance()->getProject()->getWorld());
        world->addScore((int) value);

        health -= value;
        if (health <= 0.0f) {
            playerKilled = true;
            return true;
        }

        return false;
    }

    void EnemyEntity::setHealthAndMaxHealth(float health) {
        this->health = health;
        this->maxHealth = health;
    }

    float EnemyEntity::getHealth() {
        return health;
    }

    float EnemyEntity::getMaxHealth() {
        return maxHealth;
    }

    bool EnemyEntity::wasPlayerKilled() {
        return playerKilled;
    }
}
