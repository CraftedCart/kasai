#include "katoproject/Project.hpp"
#include "katoproject/entities/BulletEntity.hpp"
#include "katoproject/scheme/KasaiSchemeDefines.hpp"
#include "kato/EnginePaths.hpp"
#include "imgui_internal.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace KatoProject {
    void Project::init() {
        engine->setFixedTimestep(1.0f / 120.0f); //120 Hz

        schemeExec = new SchemeExecutor();
        KasaiSchemeDefines::defineAllGlobal(*schemeExec);

        world = new KasaiWorld(*engine, *schemeExec);

        //So we don't spam the log with tick not defined errors, before running the script
        // schemeExec->executeString("(define (tick delta-seconds) '())");

        boost::filesystem::path initPath = Kato::EnginePaths::getResourceDirectoryPath();
        initPath /= "kasai";
        initPath /= "scripts";
        schemeExec->addToLoadPath(initPath);
        initPath /= "init.scm";
        schemeExec->executeFile(initPath);
    }

    void Project::tick(float deltaSeconds) {
        world->tick(deltaSeconds);
        world->draw(deltaSeconds);

        if (engine->isUsingEditor()) {
            //Scheme evaluator
            ImGui::Begin("Scheme");

            static char text[1024 * 16] =
                ";; (display \"Hello, world!\") (newline)\n"
                "\n"
                "(define spawn-delay 0.025)\n"
                "(define spawn-timer spawn-delay)\n"
                "\n"
                "(define (scratch-tick delta-seconds)\n"
                "\t(set! spawn-timer (- spawn-timer delta-seconds))\n"
                "\n"
                "\t(when (< spawn-timer 0.0)\n"
                "\t\t(set! spawn-timer (+ spawn-timer spawn-delay))\n"
                "\t\t(spawn-bullet\n"
                "\t\t\t:position '(300 600)\n"
                "\t\t\t:velocity `(\n"
                "\t\t\t\t,(* 500 (fsin (* 80 elapsed-seconds)))\n"
                "\t\t\t\t,(* -300 (+ 1.5 (fcos elapsed-seconds)))\n"
                "\t\t\t)\n"
                "\t\t)\n"
                "\t)\n"
                ")\n"
                "\n"
                "(add-to-dynamic-hook! tick-hook 'scratch-tick)\n";

            ImGui::InputTextMultiline(
                    "##evalSchemeSource",
                    text,
                    IM_ARRAYSIZE(text),
                    ImVec2(-1.0f, ImGui::GetWindowHeight() - 58.0f),
                    ImGuiInputTextFlags_AllowTabInput
                    );

            if (ImGui::Button("Run")) {
                schemeExec->executeString(text);
            }

            ImGui::End();
        }

        // Check the GUI state to make sure a script doesn't stuff it up
        int imguiWindowStackSizeBefore = ImGui::GetCurrentContext()->CurrentWindowStack.size();
        schemeExec->executeString("(tick " + std::to_string(deltaSeconds) + ")");
        int imguiWindowStackSizeAfter = ImGui::GetCurrentContext()->CurrentWindowStack.size();

        if (imguiWindowStackSizeAfter > imguiWindowStackSizeBefore) {
            // TODO: Some proper logging system
            std::cerr << "ImGui window stack imbalance!" << std::endl;

            for (int i = 0; i < imguiWindowStackSizeAfter - imguiWindowStackSizeBefore; i++) {
                ImGui::Text("IMGUI WINDOW STACK IMBALANCE!");
                ImGui::Text("(Are you forgetting an (imgui/end), or is your script erroring?)");
                ImGui::End();
            }
        }
    }

    void Project::handleEvent(const SDL_Event &event) {
        //HACK: The world shouldn't be handling input, should it
        //Maybe some player controller class instead
        static_cast<KasaiWorld*>(world)->handleEvent(event);
    }

    void Project::shutdown() {
        delete world;
        delete schemeExec;
    }
}
