(define button-times-clicked 0)

(define (tick delta-seconds)
  (imgui/begin "Scheme ImGui example")

  (when (imgui/button "Click me!")
    (set! button-times-clicked (+ 1 button-times-clicked)))
  (imgui/text (format #f "The button has been clicked ~A times" button-times-clicked))

  (imgui/small-button "This small button does nothing")

  (when (imgui/begin-child :name "ChildWin"
                           :border #t
                           :flags (logior imgui/window-flags/no-scrollbar imgui/window-flags/no-move))
    (imgui/text "ohai there!")
    (imgui/text "This is a child window")

    (let ((screen-pos (imgui/get-cursor-screen-pos)))
      (imgui/draw-list/add-rect-filled :draw-list (imgui/get-window-draw-list)
                                       :a screen-pos
                                       :b (list (+ (car screen-pos) 256.0) (+ (cadr screen-pos) 32.0))
                                       :col '(242 94 174 255)
                                       :rounding 8.0
                                       :rounding-corners-flags (logior imgui/draw-corner-flags/bot-right imgui/draw-corner-flags/top-left))
      (imgui/draw-list/add-rect :draw-list (imgui/get-window-draw-list)
                                :a (list (car screen-pos) (+ (cadr screen-pos) 40.0))
                                :b (list (+ (car screen-pos) 256.0) (+ (cadr screen-pos) 72.0))
                                :col '(242 94 174 255)
                                :rounding 8.0
                                :rounding-corners-flags (logior imgui/draw-corner-flags/bot-right imgui/draw-corner-flags/top-left)
                                :thickness 4.0)
      (imgui/draw-list/add-line :draw-list (imgui/get-window-draw-list)
                                :a (list (car screen-pos) (+ (cadr screen-pos) 80.0))
                                :b (list (+ (car screen-pos) 256.0) (+ (cadr screen-pos) 112.0))
                                :col '(242 94 174 255)
                                :thickness 4.0)))
  (imgui/end-child)

  (imgui/end))
