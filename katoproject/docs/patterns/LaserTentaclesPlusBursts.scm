(define spawn-delay 0.025)
(define spawn-delay-circle 1)
(define spawn-timer spawn-delay)
(define spawn-timer-circle spawn-delay)
(define circle-density 8)
(define circle-2-density 30)

(define (scratch-tick delta-seconds)
  (set! spawn-timer (- spawn-timer delta-seconds))
  (set! spawn-timer-circle (- spawn-timer-circle delta-seconds))

  (when (< spawn-timer 0.0)
    (set! spawn-timer (+ spawn-timer spawn-delay))

    (let loop ((i (* 2 pi)))
      (spawn-bullet
       :position '(300 600)
       :tick-func (make-entity-constant-velocity-func `(,(* delta-seconds 300 (fsin (+ i (fsin (* 1.5 (* elapsed-seconds 0.1))))))
                                                        ,(* delta-seconds 300 (fcos (+ i (fsin (* 1.5 (* elapsed-seconds 0.1)))))))))

      (if (> i (/ (* pi 2) circle-density))
          (loop (- i (/ (* pi 2) circle-density))))))

  (when (< spawn-timer-circle 0.0)
    (set! spawn-timer-circle (+ spawn-timer-circle spawn-delay-circle))

    (let loop ((i (* 2 pi)))
      (spawn-bullet
       :position '(300 600)
       :tick-func (make-entity-constant-velocity-func `(,(* delta-seconds 80 (sin (+ i (* 4.5 elapsed-seconds))))
                                                        ,(* delta-seconds 80 (cos (+ i (* 4.5 elapsed-seconds)))))))

      (if (> i (/ (* pi 2) circle-2-density))
          (loop (- i (/ (* pi 2) circle-2-density)))))))

(add-to-dynamic-hook! tick-hook 'scratch-tick)
