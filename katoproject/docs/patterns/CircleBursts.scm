(define spawn-delay 1)
(define spawn-timer spawn-delay)
(define circle-density 80)

(define (scratch-tick delta-seconds)
  (set! spawn-timer (- spawn-timer delta-seconds))

  (when (< spawn-timer 0.0)
    (set! spawn-timer (+ spawn-timer spawn-delay))

    (let loop ((i (* 2 pi)))
      (spawn-bullet
       :position '(300 600)
       :velocity `(,(* 100 (sin (+ i (* 4.5 elapsed-seconds))))
                   ,(* 100 (cos (+ i (* 4.5 elapsed-seconds))))))

      (if (> i (/ (* pi 2) circle-density))
          (loop (- i (/ (* pi 2) circle-density)))))))

(add-to-dynamic-hook! tick-hook 'scratch-tick)
