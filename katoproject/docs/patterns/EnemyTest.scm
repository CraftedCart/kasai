(define (scratch-tick delta-seconds)
  (when (= (modulo elapsed-ticks 45) 0)
    (let ((spawn-time elapsed-ticks))
      (spawn-enemy
       :position '(500 -50)
       :tick-func (lambda (self)
                    (entity/add-position self '(0 4))

                    ;; Spawn bullets every now and then
                    (when (= (modulo (+ elapsed-ticks spawn-time) (modulo spawn-time 30)) 0)
                      (spawn-bullet
                       :position (vec2/add (entity/get-position self) '(48 48))
                       :velocity '(-100 0)))

                    ;; Destroy self when past y 800
                    (when (> (cadr (entity/get-position self)) 800)
                      (entity/destroy self)))))))

(add-to-dynamic-hook! tick-hook 'scratch-tick)
