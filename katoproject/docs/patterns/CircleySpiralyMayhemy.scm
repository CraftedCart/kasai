(define spawn-delay 0.025)
(define spawn-timer spawn-delay)

(define (scratch-tick delta-seconds)
  (set! elapsed-seconds (+ elapsed-seconds delta-seconds))
  (set! spawn-timer (- spawn-timer delta-seconds))

  (when (< spawn-timer 0.0)
    (set! spawn-timer (+ spawn-timer spawn-delay))

    (spawn-bullet
     :position `(,(+ 300 (* (sin (* elapsed-seconds 16)) 200))
                 ,(+ 500 (* (cos (* elapsed-seconds 16)) 200)))
     :velocity `(,(* (sin (* elapsed-seconds 8)) 800)
                 ,(* (cos (* elapsed-seconds 8)) 800)))))

(add-to-dynamic-hook! tick-hook 'scratch-tick)
