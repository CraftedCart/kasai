(define spawn-delay 0.25)
(define spawn-timer spawn-delay)
(define circle-density 30)

(define (scratch-tick delta-seconds)
  (set! spawn-timer (- spawn-timer delta-seconds))

  (when (< spawn-timer 0.0)
    (set! spawn-timer (+ spawn-timer spawn-delay))

    (let loop ((i (* 2 pi)))
      (spawn-bullet
       :position '(300 600)
       :velocity `(,(* 100 (fsin (+ i (* (fsin (* 4 (* elapsed-seconds 0.1))) elapsed-seconds))))
                   ,(* 100 (fcos (+ i (* (fsin (* 4 (* elapsed-seconds 0.1))) elapsed-seconds))))))

      (if (> i (/ (* pi 2) circle-density))
          (loop (- i (/ (* pi 2) circle-density)))))))

(add-to-dynamic-hook! tick-hook 'scratch-tick)
