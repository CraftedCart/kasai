#ifndef KATOPROJECT_KASAIWORLD_HPP
#define KATOPROJECT_KASAIWORLD_HPP

#include "katoproject_export.h"
#include "katoproject/scheme/SchemeExecutor.hpp"
#include "katoproject/entities/PlayerEntity.hpp"
#include "kato/World.hpp"
#include "kato/commands/ICommand.hpp"
#include "kato/EngineInstance.hpp"
#include <set>

namespace KatoProject {
    class KATOPROJECT_EXPORT KasaiWorld : public Kato::World {
        private:
            SchemeExecutor &schemeExec;

            Kato::Texture2D *bulletTexture;
            Kato::MaterialAsset *bulletMaterial;
            Kato::Texture2D *playerBulletTexture;
            Kato::Texture2D *playerTexture;
            Kato::MaterialAsset *playerMaterial;
            Kato::Texture2D *enemyTexture;
            Kato::MaterialAsset *enemyMaterial;
            Kato::Texture2D *hitboxTexture;
            Kato::MaterialAsset *hitboxMaterial;
            Kato::Texture2D *gradientTexture;
            Kato::MaterialAsset *gradientMaterial;
            Kato::MaterialAsset *bgMaterial;
            Kato::Texture2D *scoreTexture;
            Kato::MaterialAsset *scoreMaterial;

            Kato::StaticMeshEntity *leftWallEnt;
            Kato::StaticMeshEntity *rightWallEnt;
            Kato::StaticMeshEntity *leftBgEnt;
            Kato::StaticMeshEntity *rightBgEnt;
            Kato::StaticMeshEntity *healthBarEnt;

            int score = 0;
            bool gameActive = false;

            bool isReplayMode = false;
            unsigned int replayTick = 0;
            unsigned int lives = 5;
            std::vector<std::vector<Kato::ICommand*>> replayBuffer;

        public:
            PlayerEntity *playerEnt;
            Kato::MaterialAsset *playerBulletMaterial;
            std::set<Kato::Entity*> entitiesToErase;

        private:
            /**
             * @brief Defines all world native defines with the given executor
             */
            void schemeDefineAllWorld();

        public:
            KasaiWorld(Kato::EngineInstance &engine, SchemeExecutor &executor);
            ~KasaiWorld();

            virtual void tick(float deltaSeconds) override;

            SchemeExecutor& getSchemeExecutor();

            /**
             * @param command The command to execute and consume
             *
             * @note This takes ownership of the command passed in
             */
            void consumeCommand(Kato::ICommand *command, bool addToReplay = true);

            //HACK: The world shouldn't be handling input, should it
            //Maybe some player controller class instead
            void handleEvent(const SDL_Event &event);

            /**
             * @note Don't forget to delete the command when you're done
             * @note Can return nullptr if there's no input to handle
             */
            Kato::ICommand* handleInput();

            void restartGame();
            void initGame();
            void startReplay();
            void endGame();

            void addScore(int score);

            //////////////// PUBLIC SCHEME SCRIPTING ////////////////

            /**
             * @brief Spawns a bullet entity in the world
             *
             * @note This is not thread safe! But you shouldn't be calling this from outside the main thread anyway
             */
            static s7_pointer schemeSpawnBullet(s7_scheme *sc, s7_pointer args);

            /**
             * @brief Spawns an enemy entity in the world
             *
             * @note This is not thread safe! But you shouldn't be calling this from outside the main thread anyway
             */
            static s7_pointer schemeSpawnEnemy(s7_scheme *sc, s7_pointer args);

            /**
             * @brief Spawns an score pickup entity in the world
             *
             * @note This is not thread safe! But you shouldn't be calling this from outside the main thread anyway
             */
            static s7_pointer schemeSpawnScore(s7_scheme *sc, s7_pointer args);

            /**
             * @brief Restarts the game
             *
             * @note This is not thread safe! But you shouldn't be calling this from outside the main thread anyway
             */
            static s7_pointer schemeRestartGame(s7_scheme *sc, s7_pointer args);

            /**
             * @brief Starts a replay
             *
             * @note This is not thread safe! But you shouldn't be calling this from outside the main thread anyway
             */
            static s7_pointer schemeStartReplay(s7_scheme *sc, s7_pointer args);
    };
}

#endif
