#ifndef KATOPROJECT_PROJECT_HPP
#define KATOPROJECT_PROJECT_HPP

#include "katoproject_export.h"
#include "katoproject/KasaiWorld.hpp"
#include "katoproject/scheme/SchemeExecutor.hpp"
#include "kato/entities/StaticMeshEntity.hpp"
#include "kato/rendering/Texture2D.hpp"
#include "kato/ProjectInstance.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT Project : public Kato::ProjectInstance {
        //Inherit the ctor
        using Kato::ProjectInstance::ProjectInstance;

        protected:
            SchemeExecutor *schemeExec;

        public:
            virtual void init() override;
            virtual void tick(float deltaSeconds) override;
            virtual void handleEvent(const SDL_Event &event) override;
            virtual void shutdown() override;
    };
}

#endif
