#ifndef KATOPROJECT_ENTITIES_PLAYERENTITY_HPP
#define KATOPROJECT_ENTITIES_PLAYERENTITY_HPP

#include "katoproject_export.h"
#include "kato/entities/StaticMeshEntity.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT PlayerEntity : public Kato::Entity {
        protected:
            Kato::StaticMeshEntity *meshEnt;
            Kato::StaticMeshEntity *visualHitboxEnt;
            float hitboxRot = 0.0f;
            unsigned int shootCooldown = 8;

            bool focused = false;

        public:
            PlayerEntity(
                    const std::string &name,
                    Kato::MaterialAsset *playerMaterial,
                    Kato::MaterialAsset *hitboxMaterial
                    );

            virtual void tick(float deltaSeconds) override;

            void setFocused(bool isFocused);
            bool isFocused();

            Kato::StaticMeshEntity* getMeshEntity();
    };
}

#endif
