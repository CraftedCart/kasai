#ifndef KATOPROJECT_ENTITIES_BULLETENTITY_HPP
#define KATOPROJECT_ENTITIES_BULLETENTITY_HPP

#include "katoproject_export.h"
#include "katoproject/scheme/SchemeExecutor.hpp"
#include "kato/entities/StaticMeshEntity.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT BulletEntity : public Kato::StaticMeshEntity {
        protected:
            float collisionRadius = 22.0f; //TODO: Make this configurable
            SchemeExecutor &schemeExec;
            s7_pointer schemeTickFunc = nullptr;
            s7_int schemeTickFuncGcLoc;
            bool friendly = false;

        public:
            BulletEntity(
                    const std::string &name,
                    SchemeExecutor &schemeExec,
                    bool friendly = false
                    );

            ~BulletEntity();

            virtual void tick(float deltaSeconds) override;

            float getCollisionRadius();
            void setSchemeTickFunc(s7_pointer schemeTickFunc);
            bool isFriendly();
    };
}

#endif
