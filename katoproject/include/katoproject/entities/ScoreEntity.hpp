#ifndef KATOPROJECT_ENTITIES_SCOREENTITY_HPP
#define KATOPROJECT_ENTITIES_SCOREENTITY_HPP

#include "katoproject_export.h"
#include "kato/entities/StaticMeshEntity.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT ScoreEntity : public Kato::StaticMeshEntity {
        public:
            ScoreEntity(const std::string &name);

            virtual void tick(float deltaSeconds) override;
    };
}

#endif
