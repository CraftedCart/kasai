#ifndef KATOPROJECT_ENTITIES_ENEMYENTITY_HPP
#define KATOPROJECT_ENTITIES_ENEMYENTITY_HPP

#include "katoproject_export.h"
#include "katoproject/scheme/SchemeExecutor.hpp"
#include "kato/entities/StaticMeshEntity.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT EnemyEntity : public Kato::Entity {
        protected:
            Kato::StaticMeshEntity *meshEnt;
            SchemeExecutor &schemeExec;
            s7_pointer schemeTickFunc = nullptr;
            s7_int schemeTickFuncGcLoc;
            s7_pointer schemeDestroyFunc = nullptr;
            s7_int schemeDestroyFuncGcLoc;
            float health = 20.0f;
            float maxHealth = 20.0f;
            bool playerKilled = false;

        public:
            EnemyEntity(
                    const std::string &name,
                    Kato::MaterialAsset *playerMaterial,
                    SchemeExecutor &schemeExec
                    );

            ~EnemyEntity();

            virtual void tick(float deltaSeconds) override;

            Kato::StaticMeshEntity* getMeshEntity();
            void setSchemeTickFunc(s7_pointer schemeTickFunc);
            void setSchemeDestroyFunc(s7_pointer schemeDestroyFunc);

            void setHealthAndMaxHealth(float health);
            float getHealth();
            float getMaxHealth();
            bool damage(float value);

            bool wasPlayerKilled();
    };
}

#endif
