#ifndef KATOPROJECT_COMMANDS_PLAYERSETFOCUSEDCOMMAND_HPP
#define KATOPROJECT_COMMANDS_PLAYERSETFOCUSEDCOMMAND_HPP

#include "kato/commands/ICommand.hpp"
#include "katoproject/entities/PlayerEntity.hpp"
#include "katoproject_export.h"

namespace KatoProject {
    class KATOPROJECT_EXPORT PlayerSetFocusedCommand : public Kato::ICommand {
        protected:
            PlayerEntity &playerEnt;
            bool focused;

        public:
            PlayerSetFocusedCommand(PlayerEntity &playerEnt, bool focused);

            virtual void execute() override;
    };
}

#endif
