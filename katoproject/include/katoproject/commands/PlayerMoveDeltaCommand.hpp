#ifndef KATOPROJECT_COMMANDS_PLAYERMOVEDELTACOMMAND_HPP
#define KATOPROJECT_COMMANDS_PLAYERMOVEDELTACOMMAND_HPP

#include "katoproject_export.h"
#include "katoproject/entities/PlayerEntity.hpp"
#include "kato/commands/ICommand.hpp"

namespace KatoProject {
    class KATOPROJECT_EXPORT PlayerMoveDeltaCommand : public Kato::ICommand {
        protected:
            PlayerEntity &playerEnt;
            const glm::vec3 direction;

        public:
            PlayerMoveDeltaCommand(
                    PlayerEntity &playerEnt,
                    const glm::vec3 &direction
                    );

            virtual void execute() override;
    };
}

#endif
