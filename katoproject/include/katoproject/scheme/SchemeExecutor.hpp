#ifndef KATOPROJECT_SCHEME_SCHEMEEXECUTOR_HPP
#define KATOPROJECT_SCHEME_SCHEMEEXECUTOR_HPP

#include "katoproject_export.h"
#include "s7.h"
#include <boost/filesystem/path.hpp>
#include <string>

namespace KatoProject {
    class KATOPROJECT_EXPORT SchemeExecutor {
        private:
            s7_scheme *sc;

        public:
            SchemeExecutor();
            ~SchemeExecutor();

            s7_scheme* getContext();

            /**
             * @brief Executes a string of Scheme code
             */
            void executeString(const std::string &code);

            /**
             * @brief Execute Scheme code in a file
             *
             * @return Whether it successfully read the file
             */
            bool executeFile(const boost::filesystem::path &filePath);

            void addToLoadPath(const boost::filesystem::path &filePath);

            void defineNativeFunction(
                    const char *name,
                    s7_function func,
                    const char *doc,
                    s7_int requiredArgs = 0,
                    s7_int optionalArgs = 0,
                    bool restArgs = false
                    );

            void defineNativeFunctionKwargs(
                    const char *name,
                    s7_function func,
                    const char *args,
                    const char *doc
                    );

            void defineConstInteger(const char *name, int64_t num, const char *doc);
            void defineConstReal(const char *name, double num, const char *doc);
    };
}

#endif
