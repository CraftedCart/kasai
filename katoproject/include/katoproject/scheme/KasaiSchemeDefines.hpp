#ifndef KATOPROJECT_SCHEME_KASAISCHEMEDEFINES_HPP
#define KATOPROJECT_SCHEME_KASAISCHEMEDEFINES_HPP

#include "katoproject_export.h"
#include "katoproject/scheme/SchemeExecutor.hpp"
#include "kato/entities/StaticMeshEntity.hpp"

namespace KatoProject::KasaiSchemeDefines {
    //////////////// TYPE TAGS ////////////////
    KATOPROJECT_EXPORT extern s7_int typeImDrawList;
    KATOPROJECT_EXPORT extern s7_int typeEnemyEntity;
    KATOPROJECT_EXPORT extern s7_int typeBulletEntity;

    //////////////// TYPE FUNCTIONS ////////////////
    KATOPROJECT_EXPORT bool arePointersEqual(void *a, void *b);

    //////////////// GLOBAL DEFINES ////////////////

    /**
     * @brief Defines all global native defines with the given executor
     */
    KATOPROJECT_EXPORT void defineAllGlobal(SchemeExecutor &executor);

    //Math
    KATOPROJECT_EXPORT s7_pointer fastSin(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer fastCos(s7_scheme *sc, s7_pointer args);

    //ImGui
    KATOPROJECT_EXPORT s7_pointer imguiBegin(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiEnd(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiBeginChild(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiEndChild(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiTreeNode(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiTreePop(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiText(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiTextWrapped(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiButton(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiSmallButton(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiInputText(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiGetCursorScreenPos(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiSetNextWindowPos(s7_scheme *sc, s7_pointer args);

    //ImGui draw lists
    KATOPROJECT_EXPORT s7_pointer imguiGetWindowDrawList(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiDrawListAddLine(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiDrawListAddRect(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer imguiDrawListAddRectFilled(s7_scheme *sc, s7_pointer args);

    //Entity
    KATOPROJECT_EXPORT s7_pointer entityGetPosition(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer entityAddPosition(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer entityDestroy(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer entityEnemyWasPlayerKilled(s7_scheme *sc, s7_pointer args);

    //World
    KATOPROJECT_EXPORT s7_pointer worldGetAllEnemyBulletEntities(s7_scheme *sc, s7_pointer args);

    //Engine
    KATOPROJECT_EXPORT s7_pointer engineIsUsingEditor(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer engineRequestQuit(s7_scheme *sc, s7_pointer args);

    //Scheme util
    KATOPROJECT_EXPORT s7_pointer vec2Add(s7_scheme *sc, s7_pointer args);
    KATOPROJECT_EXPORT s7_pointer stringContains(s7_scheme *sc, s7_pointer args);

    //C++ util
    /**
     * @brief Converts a Scheme list of two elements to X and Y floats
     *
     * @return Whether it successfully converted or not
     */
    KATOPROJECT_EXPORT bool listToVec2(s7_scheme *sc, s7_pointer list, float &outX, float &outY);

    /**
     * @brief Converts a Scheme list of four elements to X, Y, Z, and W uint8s
     *
     * @return Whether it successfully converted or not
     */
    KATOPROJECT_EXPORT bool listToUInt8Vec4(
            s7_scheme *sc,
            s7_pointer list,
            uint8_t &outX,
            uint8_t &outY,
            uint8_t &outZ,
            uint8_t &outW
            );

    /**
     * @param obj The scheme object to check for whether it's an entity
     *
     * @return Whether the obj is an entity or not
     */
    KATOPROJECT_EXPORT bool isEntity(s7_pointer obj);
}

#endif
