(define* (spawn-score-circle position circle-density)
  (let loop ((i (* 2 pi)))
    (spawn-score
     :position (list (+ (car position) (* (fsin i) 50))
                     (+ (cadr position) (* (fcos i) 50))))

    (if (> i (/ (* pi 2) circle-density))
        (loop (- i (/ (* pi 2) circle-density))))))

(define (make-spawn-score-circle-func density)
  (lambda (self)
    (when (entity/enemy/was-player-killed? self)
      (spawn-score-circle (entity/get-position self) density))))

(define* (spawn-circle-burst position circle-density speed rotation-offset)
  (let loop ((i (* 2 pi)))
    (spawn-bullet
     :position position
     :tick-func (make-entity-constant-velocity-func `(,(* speed (fsin (+ i rotation-offset)))
                                                      ,(* speed (fcos (+ i rotation-offset))))))

    (if (> i (/ (* pi 2) circle-density))
        (loop (- i (/ (* pi 2) circle-density))))))

(define (finish-boss-section self)
  ;; Spawn score for all bullets
  (let loop ((bullets (world/get-all-enemy-bullet-entities)))
    (when (not (null? bullets))
      (spawn-score :position (entity/get-position (car bullets))))

    (when (not (null? bullets))
      (loop (cdr bullets))))

  ;; Destroy all bullets
  (run-in-ticks 1 (lambda ()
                    (let loop ((bullets (world/get-all-enemy-bullet-entities)))
                      (when (not (null? bullets))
                        (entity/destroy (car bullets)))

                      (when (not (null? bullets))
                        (loop (cdr bullets)))))))

(define (make-stage1-wave1-tick-func)
  (let ((x-velocity 0)
        (y-velocity -10)
        (y-velocity-accel 0.2)
        (life-tick 0))
    (lambda (self)
      ;; Move down and slow down over time
      (entity/add-position self (list x-velocity y-velocity))
      (if (< y-velocity -0.5)
          (begin
            (set! y-velocity (+ y-velocity y-velocity-accel))
            (set! y-velocity-accel (max 0.01 (- y-velocity-accel 0.002))))
          (set! y-velocity -0.5))

      ;; Spawn circles periodically
      (when (and (> life-tick 60) (= (modulo life-tick 80) 0))
        (spawn-circle-burst
         :position (entity/get-position self)
         :circle-density 5
         :speed 1.5
         :rotation-offset life-tick))

      (when (< (cadr (entity/get-position self)) -50)
        (entity/destroy self))

      (set! life-tick (+ 1 life-tick)))))

(define (make-stage1-wave2-tick-func x-vel)
  (let ((life-tick 0))
    (lambda (self)
      ;; Move down
      (entity/add-position self '(0 -4))

      ;; Spawn circles periodically
      (when (= (modulo life-tick 40) 0)
        (spawn-bullet
         :position (entity/get-position self)
         :tick-func (make-entity-constant-velocity-func (list x-vel 0))))

      ;; Destroy self after time
      (when (> life-tick 360)
        (entity/destroy self))

      (set! life-tick (+ 1 life-tick)))))

(define (make-stage1-wave3-tick-func)
  (let ((x-velocity 0)
        (y-velocity 20)
        (y-velocity-accel -0.2)
        (life-tick 0))
    (lambda (self)
      ;; Move down and slow down over time
      (entity/add-position self (list x-velocity y-velocity))
      (if (< y-velocity 2)
          (begin
            (set! y-velocity (+ y-velocity y-velocity-accel))
            (set! y-velocity-accel (min -0.01 (+ y-velocity-accel 0.002))))
          (set! y-velocity 2))

      (when (> (cadr (entity/get-position self)) 850)
        (entity/destroy self))

      ;; Spawn circles periodically
      (when (and (> life-tick 60) (= (modulo life-tick 160) 0))
        (spawn-circle-burst
         :position (entity/get-position self)
         :circle-density 20
         :speed 0.8
         :rotation-offset life-tick))

      (set! life-tick (+ 1 life-tick)))))

(define (make-stage1-boss-tick-func)
  (let ((x-velocity 0)
        (y-velocity -10)
        (y-velocity-accel 0.185)
        (life-tick 0)
        (time-vel-mult 1)
        (spawn-delay 120)
        (spawn-timer 120))
    (lambda (self)
      ;; Move down and slow down over time
      (entity/add-position self (list x-velocity y-velocity))
      (if (< y-velocity 0)
          (begin
            (set! y-velocity (+ y-velocity y-velocity-accel))
            (set! y-velocity-accel (max 0.01 (- y-velocity-accel 0.002))))
          (set! y-velocity 0))

      ;; Spawn circles periodically
      (when (and (> life-tick 180) (< life-tick 7300) (<= spawn-timer 0))
        (set! spawn-delay (- spawn-delay 0.01))
        (set! spawn-timer spawn-delay)
        (let loop ((i 0))
          (let ((velocity-mult 1)
                (spawn-tick life-tick))
            (spawn-bullet
             :position (entity/get-position self)
             :tick-func (lambda (self)
                          (entity/add-position self (list (* velocity-mult (fsin (+ i spawn-tick)))
                                                          (* velocity-mult (fcos (+ i spawn-tick)))))
                          (set! velocity-mult (- velocity-mult (* time-vel-mult 0.003))))))

          (when (< i (- (* pi 2) (* pi 0.1)))
            (loop (+ i (* pi 0.1)))))

        (set! time-vel-mult (+ time-vel-mult 0.021))
        (set! spawn-delay (- spawn-delay 1)))

      ;; Move off-screen over time
      (when (> life-tick 7300)
        (set! x-velocity (+ 0.05 x-velocity)))

      ;; Destroy self after time
      (when (> life-tick 7500)
        (entity/destroy self))

      (set! spawn-timer (- spawn-timer 1))
      (set! life-tick (+ 1 life-tick)))))

(run-at-tick 240 (lambda ()
                   (spawn-enemy
                    :position '(300 800)
                    :tick-func (make-stage1-wave1-tick-func)
                    :destroy-func (make-spawn-score-circle-func 5))))

(run-at-tick 260 (lambda ()
                   (spawn-enemy
                    :position '(150 850)
                    :tick-func (make-stage1-wave1-tick-func)
                    :destroy-func (make-spawn-score-circle-func 5))))

(run-at-tick 260 (lambda ()
                   (spawn-enemy
                    :position '(450 850)
                    :tick-func (make-stage1-wave1-tick-func)
                    :destroy-func (make-spawn-score-circle-func 5))))

(let loop ((i 5))
  (run-at-tick (+ 800 (* i 20)) (lambda ()
                                  (spawn-enemy
                                   :position '(580 850)
                                   :tick-func (make-stage1-wave2-tick-func -1.5)
                                   :destroy-func (make-spawn-score-circle-func 5))))
  (run-at-tick (+ 800 (* i 20)) (lambda ()
                                  (spawn-enemy
                                   :position '(20 850)
                                   :tick-func (make-stage1-wave2-tick-func 1.5)
                                   :destroy-func (make-spawn-score-circle-func 5))))

  (when (> i 0)
    (loop (- i 1))))

(run-at-tick 1200 (lambda ()
                   (spawn-enemy
                    :position '(450 -50)
                    :tick-func (make-stage1-wave3-tick-func)
                    :destroy-func (make-spawn-score-circle-func 5))))

(run-at-tick 1200 (lambda ()
                    (spawn-enemy
                     :position '(150 -50)
                     :tick-func (make-stage1-wave3-tick-func)
                     :destroy-func (make-spawn-score-circle-func 5))))

(run-at-tick 1800 (lambda ()
                   (spawn-enemy
                    :position '(300 850)
                    :health 1500
                    :tick-func (make-stage1-boss-tick-func)
                    :destroy-func (lambda (self)
                                    (run-in-ticks 240 (lambda ()
                                                        (set! game-running #f)))
                                    (when (entity/enemy/was-player-killed? self)
                                      (spawn-score-circle (entity/get-position self) 150))

                                    (finish-boss-section self)))))

;; Restart the stage
(set! elapsed-ticks 0)
