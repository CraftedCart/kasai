(define scheme-docs-active-sym '())
(define scheme-docs-search-text "")
(define scheme-docs-repl-input-text "")
(define scheme-docs-repl-console '())
(define scheme-docs-enabled #f)

(define (doc-viewer-tick delta-seconds)
  (imgui/begin "Scheme documentation")

  (when (imgui/button "Toggle doc viewer enabled (Causes lag!)")
    (set! scheme-docs-enabled (not scheme-docs-enabled)))

  (when scheme-docs-enabled
    (set! scheme-docs-search-text (car (imgui/input-text "Search" scheme-docs-search-text)))

    (imgui/begin-child "Scheme procedure list" :size '(0 -512))
    (for-each
     (lambda (pair)
       (let ((sym (car pair))
             (sym-str (symbol->string (car pair)))
             (val (cdr pair)))
         (when (string-contains? sym-str scheme-docs-search-text)
           (let ((button-text (if (procedure? val)
                                  (if (eq? (signature val) #f)
                                      (format #f "~S    [procedure?] () -> #t" sym)
                                      (format #f "~S    [procedure?] ~S -> ~S" sym (cdr (signature val)) (car (signature val))))
                                  (format #f "~S    [~S]" sym (type-of val)))))
             (when (imgui/button button-text)
               (set! scheme-docs-active-sym sym))))))
     (let->list (rootlet)))
    (imgui/end-child)

    (imgui/begin-child "Scheme doc viewer" :size '(0 256))
    (unless (eq? scheme-docs-active-sym '())
      (imgui/text-wrapped (string-append (symbol->string scheme-docs-active-sym) "\n\n"))
      (imgui/text-wrapped (documentation scheme-docs-active-sym))
      (imgui/text-wrapped "\n")

      (if (procedure? (symbol->value scheme-docs-active-sym))
          (begin
            ;; Source toggle
            (when (imgui/tree-node "Source")
              (imgui/text-wrapped (object->string (procedure-source scheme-docs-active-sym)))
              (imgui/tree-pop))

            ;; Environment toggle
            (when (imgui/tree-node (format #f "Environment ~S###Environment" (funclet scheme-docs-active-sym)))
              (for-each (lambda (item)
                          (imgui/text (format #f "~A: ~A" (symbol->string (car item)) (object->string (cdr item)))))
                        (let->list (funclet scheme-docs-active-sym)))
              (imgui/tree-pop)))

          ;; Value toggle
          (when (imgui/tree-node "Value")
            (let ((symbol-val (symbol->value scheme-docs-active-sym)))
              (imgui/text-wrapped (object->string symbol-val))
              (when (and (number? symbol-val) (not (integer? symbol-val)) (exact? symbol-val))
                (imgui/text-wrapped (string-append "As inexact: " (number->string (exact->inexact symbol-val))))))
            (imgui/tree-pop))))
    (imgui/end-child))

  ;; Repl
  (imgui/begin-child "Scheme REPL output" :size '(0 -23))

  (for-each (lambda (item)
              (imgui/text-wrapped item))
            scheme-docs-repl-console)

  (imgui/end-child)

  (let ((repl-input (imgui/input-text "REPL" scheme-docs-repl-input-text :flags imgui/input-text-flags/enter-returns-true)))
    (set! scheme-docs-repl-input-text (car repl-input))
    (when (cdr repl-input)
      (set! scheme-docs-repl-console (cons (string-append "> "
                                                          scheme-docs-repl-input-text
                                                          "\n    "
                                                          (object->string (eval-string scheme-docs-repl-input-text (rootlet))))
                                           scheme-docs-repl-console))))

  (imgui/end))
