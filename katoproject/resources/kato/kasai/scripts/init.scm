;;; init.scm --- Initialization file for the base game



;;; General purpose utility functions/macros

(define contains?
  (let ((+documentation+ "(contains? list val) return #t if LIST contains VAL, otherwise return #f"))
    (lambda (list val)
      (cond ((null? list) #f)
            ((eq? (car list) val) #t)
            (else (contains? (cdr list) val))))))

;; Add VAL to LIST if LIST does not already contain VAL
(define-macro (add-to-list! list val)
  `(when (not (contains? ,list ,val))
     (set! ,list (cons ,val ,list))))



;;; Dynamic hooks (Hooks with functions defined as symbols - such that they can be changed easily)

(define make-dynamic-hook
  (let ((+signature+ '(procedure?))
        (+documentation+ "(make-dynamic-hook . pars) returns a new hook (a function) that passes the parameters to its function list."))
    (lambda hook-args
      (let ((body ()))
        (apply lambda* hook-args
               (copy '(let ((result #<unspecified>))
                        (let ((hook (curlet))
                              (hook-args-expanded (map symbol->value hook-args)))
                          (for-each (lambda (hook-function) (apply (symbol->value hook-function) hook-args-expanded)) body)
                          result))
                     :readable)
               ())))))

(define dynamic-hook-functions
  (let ((+signature+ '(#t procedure?))
        (+documentation+ "(dynamic-hook-functions hook) gets or sets the list of functions associated with the hook"))
    (dilambda
     (lambda (hook)
       ((funclet hook) 'body))
     (lambda (hook lst)
       (for-each (lambda (item)
                   (when (not (symbol? item))
                     (error 'wrong-type-arg "dynamic-hook-functions must be a list of symbols to functions: ~S" lst)))
                 lst)
       (set! ((funclet hook) 'body) lst)))))

(define (add-to-dynamic-hook! hook val)
  (add-to-list! (dynamic-hook-functions hook) val))



;;; Tick/event vars and functions

(define elapsed-ticks 0)
(define elapsed-seconds 0.0)
(define +tick-rate+ 120)

;; An alist of events scheduled to run at a certain tick
;; (tick . procedure) - eg: (144000 . spawn-the-boss) to run spawn-the-boss 144000 ticks / 20 minutes in
(define future-events '())

(define tick-hook (make-dynamic-hook 'delta-seconds))

(define (tick delta-seconds)
  (tick-hook delta-seconds))

(define (root-tick delta-seconds)
  (set! elapsed-ticks (+ elapsed-ticks 1))
  (set! elapsed-seconds (/ elapsed-ticks +tick-rate+))

  ;; Run future events
  (let ((new-future-events '()))
    (for-each (lambda (item)
                (if (= (car item) elapsed-ticks)
                    ((cdr item))
                    (set! new-future-events (cons item new-future-events))))
              future-events)

    (set! future-events new-future-events)))
(add-to-dynamic-hook! tick-hook 'root-tick)



(define (end-game)
  (set! future-events '())
  (set! game-running #f))



;;; Game utility functions

(define* (time->ticks (minutes 0) (seconds 0) (ticks 0))
  (+ (* (+ (* minutes 60) seconds) +tick-rate+) ticks))

(define* (seconds->ticks seconds (ticks 0))
  (time->ticks :seconds seconds :ticks ticks))

(define* (minutes->ticks minutes (seconds 0) (ticks 0))
  (time->ticks :minutes minutes :seconds seconds :ticks ticks))

(define run-at-tick
  (let ((+documentation+ "(run-at-tick tick proc) run PROC when elapsed-ticks hits TICK"))
    (lambda (tick proc)
      (set! future-events (cons (cons tick proc) future-events))))) ; Add (tick . proc) to the future events alist

(define run-in-ticks
  (let ((+documentation+ "(run-in-ticks ticks proc) run PROC in TICKS ticks"))
    (lambda (ticks proc)
      (run-at-tick (+ elapsed-ticks ticks) proc))))

(define (make-entity-constant-velocity-func velocity)
  (lambda (self)
    (entity/add-position self velocity)))

(define (destroy-all-enemy-bullets)
  (let loop ((bullets (world/get-all-enemy-bullet-entities)))
    (when (not (null? bullets))
      (entity/destroy (car bullets)))

    (when (not (null? (cdr bullets)))
      (loop (cdr bullets)))))



;;; Player bullet script

(define (player-bullet-tick self)
  (entity/add-position self '(0 10)))
(define (player-bullet-right-tick self)
  (entity/add-position self '(0.5233595624 9.9862953475)))
(define (player-bullet-left-tick self)
  (entity/add-position self '(-0.5233595624 9.9862953475)))



;;; Load other files

(when (engine/is-using-editor?)
  (load "doc-viewer.scm")
  (add-to-dynamic-hook! tick-hook 'doc-viewer-tick))



;;; Main menu

(define game-running #f)
(define has-replay #f)

(define (main-menu-tick delta-seconds)
  (when (not game-running)
    (imgui/set-next-window-pos '(48 256))
    (imgui/begin "Menu" (logior imgui/window-flags/no-docking
                                imgui/window-flags/no-move
                                imgui/window-flags/no-collapse
                                imgui/window-flags/no-scrollbar
                                imgui/window-flags/no-resize
                                imgui/window-flags/no-title-bar
                                imgui/window-flags/no-saved-settings))
    (when (imgui/button "Start")
      (set! game-running #t)
      (set! has-replay #t)
      (restart-game)
      (load "stage1.scm"))

    (when has-replay
      (when (imgui/button "Watch replay")
        (set! game-running #t)
        (start-replay)
        (load "stage1.scm")))

    (when (imgui/button "Quit")
      (engine/request-quit))

    (imgui/text "") ; Spacer
    (imgui/text "WASD: Move the player")
    (imgui/text "Shift: Focus movement")
    (imgui/text "    (Slow down and show hitbox)")

    (imgui/end)))

(add-to-dynamic-hook! tick-hook 'main-menu-tick)
