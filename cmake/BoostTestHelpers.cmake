find_package(Boost COMPONENTS unit_test_framework REQUIRED)

function(add_boost_test SOURCE_FILE_NAME DEPENDENCY_LIB)
    #Executables will be named the same as the source file
    get_filename_component(TEST_EXECUTABLE_NAME ${SOURCE_FILE_NAME} NAME_WE)

    add_executable(${TEST_EXECUTABLE_NAME} ${SOURCE_FILE_NAME})
    target_compile_definitions(${TEST_EXECUTABLE_NAME} PRIVATE BOOST_TEST_DYN_LINK)
    target_link_libraries(${TEST_EXECUTABLE_NAME}
        ${DEPENDENCY_LIB}
        ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
        )

    #Find each test
    file(READ "${SOURCE_FILE_NAME}" SOURCE_FILE_CONTENTS)
    string(REGEX MATCHALL "BOOST_AUTO_TEST_CASE\\( *([A-Za-z_0-9]+) *\\)" FOUND_TESTS ${SOURCE_FILE_CONTENTS})

    #Add it as a test
    foreach(HIT ${FOUND_TESTS})
        string(REGEX REPLACE ".*\\( *([A-Za-z_0-9]+) *\\).*" "\\1" TEST_NAME ${HIT})

        #No rpath :(
        if(WIN32)
            string(REPLACE ";" $<SEMICOLON> ENV_ESCAPED "$ENV{PATH}")
            add_test(NAME "${TEST_EXECUTABLE_NAME}.${TEST_NAME}"
                COMMAND ${CMAKE_COMMAND} -E env "PATH=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}$<SEMICOLON>${ENV_ESCAPED}"
                $<TARGET_FILE:${TEST_EXECUTABLE_NAME}> --run_test=${TEST_NAME} --catch_system_error=yes)
        else(WIN32)
            add_test(NAME "${TEST_EXECUTABLE_NAME}.${TEST_NAME}"
                COMMAND ${TEST_EXECUTABLE_NAME} --run_test=${TEST_NAME} --catch_system_error=yes)
        endif(WIN32)

        set_target_properties(${TEST_EXECUTABLE_NAME} PROPERTIES
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
            )
    endforeach()
endfunction()

