#include "kato/entities/Entity.hpp"

#define BOOST_TEST_MODULE TestEntity
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(ParentChildTest) {
    Kato::Entity *parent = new Kato::Entity();
    Kato::Entity *child = new Kato::Entity();

    //Should have empty children vectors and no parents
    BOOST_CHECK_EQUAL(parent->getChildren().size(), 0);
    BOOST_CHECK_EQUAL(child->getChildren().size(), 0);
    BOOST_CHECK_EQUAL(parent->getParent(), nullptr);
    BOOST_CHECK_EQUAL(child->getParent(), nullptr);

    parent->addChild(child);

    //parent should have gained a parent
    BOOST_CHECK_EQUAL(parent->getChildren().size(), 1);
    BOOST_CHECK_EQUAL(child->getChildren().size(), 0);
    BOOST_CHECK_EQUAL(child->getParent(), parent);

    delete child;

    //Deleting the child should have removed it from its parent
    BOOST_CHECK_EQUAL(parent->getChildren().size(), 0);

    child = new Kato::Entity();
    parent->addChild(child);
    parent->removeChild(child, false); //False passed so won't delete child

    //removeChild should also work
    BOOST_CHECK_EQUAL(parent->getChildren().size(), 0);

    parent->addChild(child);
    parent->clearChildren(); //Deletes child

    //clearChildren should also work
    BOOST_CHECK_EQUAL(parent->getChildren().size(), 0);

    //Check that nothing modified the parent's parent
    BOOST_CHECK_EQUAL(parent->getParent(), nullptr);

    delete parent;
}

