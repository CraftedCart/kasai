#include "kato/FileUtils.hpp"
#include "kato/EnginePaths.hpp"
#include <boost/filesystem/fstream.hpp>

#define BOOST_TEST_MODULE TestFileUtils
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(FileIOTest) {
    boost::filesystem::path file = Kato::EnginePaths::getExecutableDirectoryPath().parent_path();
    file /= "test";
    file /= "resources";
    file /= "textfile.txt";

    boost::filesystem::ifstream fileStream;
    fileStream.open(file);
    std::string fileStr = Kato::FileUtils::readIfstreamToString(fileStream);
    fileStream.close();

    std::string EXPECTED_STRING =
        "This is an example text file!\n"
        "\n"
        "\n"
        "with some new lines\n"
        "How about a few symbols? !@#$%*()\n"
        "\n";


    BOOST_CHECK_EQUAL(fileStr, EXPECTED_STRING);
}

