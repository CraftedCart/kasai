#ifndef KATO_ASSETS_DIRECTORYASSET_HPP
#define KATO_ASSETS_DIRECTORYASSET_HPP

#include "kato_export.h"
#include "kato/assets/AbstractAsset.hpp"
#include <vector>

namespace Kato {
    class KATO_EXPORT DirectoryAsset : public AbstractAsset {
        private:
            std::vector<AbstractAsset*> children;

        public:
            DirectoryAsset(const std::string &name);

            /**
             * @brief Deletes all child assets
             */
            ~DirectoryAsset();

            void addChild(AbstractAsset *child);
            std::vector<AbstractAsset*>& getChildren();
            const std::vector<AbstractAsset*>& getChildren() const;

            AbstractAsset* getAssetByPath(const std::string path);
            const AbstractAsset* getAssetByPath(const std::string path) const;

            template<typename T>
            T* getAssetByPath(const std::string path);
            template<typename T>
            const T* getAssetByPath(const std::string path) const;

            DirectoryAsset* getDirectoryByPath(const std::string path);
            const DirectoryAsset* getDirectoryByPath(const std::string path) const;

            /**
             * @brief Returns the child asset with the matching name, or nullptr if there is none
             */
            AbstractAsset* getAssetByName(const std::string name);

            /**
             * @brief Returns the child asset with the matching name, or nullptr if there is none - const edition
             */
            const AbstractAsset* getAssetByName(const std::string name) const;

            template<typename T>
            T* getAssetByName(const std::string path);
            template<typename T>
            const T* getAssetByName(const std::string path) const;
    };
}

#include "kato/assets/DirectoryAsset.ipp"

#endif

