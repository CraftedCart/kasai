#ifndef KATO_ASSETS_ASSETMANAGER_HPP
#define KATO_ASSETS_ASSETMANAGER_HPP

#include "kato_export.h"
#include "kato/assets/DirectoryAsset.hpp"

namespace Kato {
    class KATO_EXPORT AssetManager {
        private:
            DirectoryAsset *rootDir = new DirectoryAsset("");

        public:
            DirectoryAsset* getRootDir();
            const DirectoryAsset* getRootDir() const;

            AbstractAsset* getAssetByPath(const std::string path);
            const AbstractAsset* getAssetByPath(const std::string path) const;

            DirectoryAsset* getDirectoryByPath(const std::string path);
            const DirectoryAsset* getDirectoryByPath(const std::string path) const;

            static AssetManager& getInstance();
    };
}

#endif

