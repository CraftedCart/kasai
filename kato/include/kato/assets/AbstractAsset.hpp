#ifndef KATO_ASSETS_ABSTRACTASSET_HPP
#define KATO_ASSETS_ABSTRACTASSET_HPP

#include "kato_export.h"
#include <string>

namespace Kato {
    class KATO_EXPORT AbstractAsset {
        private:
            std::string name = "";

        public:
            AbstractAsset() = default;
            AbstractAsset(const std::string &name);
            virtual ~AbstractAsset() {}

            const std::string& getName() const;
            void setName(const std::string &name);
    };
}

#endif

