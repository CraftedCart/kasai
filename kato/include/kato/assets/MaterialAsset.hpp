#ifndef KATO_ASSETS_MATERIALASSET_HPP
#define KATO_ASSETS_MATERIALASSET_HPP

#include "kato_export.h"
#include "kato/assets/AbstractAsset.hpp"
#include "kato/rendering/ShaderProgram.hpp"
#include "kato/rendering/Texture2D.hpp"

namespace Kato {
    class KATO_EXPORT MaterialAsset : public AbstractAsset {
        private:
            ShaderProgram *shader; //TODO not this - use some shader asset
            Texture2D *texture = nullptr; //TODO not this - use some texture asset

        public:
            MaterialAsset(ShaderProgram *shader, Texture2D *texture = nullptr);

            ShaderProgram* getShader();
            const ShaderProgram* getShader() const;
            Texture2D* getTexture();
            const Texture2D* getTexture() const;
    };
}

#endif

