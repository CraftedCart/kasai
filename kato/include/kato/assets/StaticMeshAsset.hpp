#ifndef KATO_ASSETS_STATICMESHASSET_HPP
#define KATO_ASSETS_STATICMESHASSET_HPP

#include "kato_export.h"
#include "kato/assets/MaterialAsset.hpp"
#include "kato/rendering/RenderableMesh.hpp"

namespace Kato {

    /**
     * @brief A static mesh asset is a mesh whose data cannot change
     *
     * StaticMeshAsset owns the RenderableMesh given to it and it will be deleted when the static mesh is deleted
     */
    class KATO_EXPORT StaticMeshAsset {
        private:
            RenderableMesh *renderableMesh;
            MaterialAsset *materialAsset;

        public:
            StaticMeshAsset() = default;
            StaticMeshAsset(RenderableMesh *renderableMesh, MaterialAsset *materialAsset);

            void setRenderableMesh(RenderableMesh *renderableMesh);
            RenderableMesh* getRenderableMesh();
            const RenderableMesh* getRenderableMesh() const;

            void setMaterialAsset(MaterialAsset* materialAsset);
            MaterialAsset* getMaterialAsset();
            const MaterialAsset* getMaterialAsset() const;
    };
}

#endif

