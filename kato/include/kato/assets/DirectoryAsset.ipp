#ifndef KATO_ASSETS_DIRECTORYASSET_IPP
#define KATO_ASSETS_DIRECTORYASSET_IPP

//We don't really need this include but it's nice for editor completion
#include "kato/assets/DirectoryAsset.hpp"

namespace Kato {
    template<typename T>
    T* DirectoryAsset::getAssetByPath(const std::string path) {
        return dynamic_cast<T*>(getAssetByPath(path));
    }

    template<typename T>
    const T* DirectoryAsset::getAssetByPath(const std::string path) const {
        return dynamic_cast<const T*>(getAssetByPath(path));
    }

    template<typename T>
    T* DirectoryAsset::getAssetByName(const std::string path) {
        return dynamic_cast<T*>(getAssetByName(path));
    }

    template<typename T>
    const T* DirectoryAsset::getAssetByName(const std::string path) const {
        return dynamic_cast<const T*>(getAssetByName(path));
    }
}

#endif

