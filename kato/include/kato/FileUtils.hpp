#ifndef KATO_FILEUTILS_HPP
#define KATO_FILEUTILS_HPP

#include "kato_export.h"
#include <boost/filesystem/path.hpp>

namespace Kato::FileUtils {
    /**
     * @brief Reads an ifstream into a string - useful for reading in text files
     *
     * @return The contents of the stream as a string
     */
    KATO_EXPORT std::string readIfstreamToString(std::ifstream &in);
}

#endif

