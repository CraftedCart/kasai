#ifndef KATO_TRANSFORM_HPP
#define KATO_TRANSFORM_HPP

#include "kato_export.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Kato {
    class KATO_EXPORT Transform {
        private:
            glm::vec3 position;
            glm::quat rotation;
            glm::vec3 scale;

            mutable glm::mat4 cachedTransformMatrix;
            mutable bool cachedMatrixDirty = true;

        public:
            Transform(
                    glm::vec3 position = glm::vec3(0.0f),
                    glm::quat rotation = glm::quat(),
                    glm::vec3 scale = glm::vec3(1.0f)
                    );

            glm::vec3& getPosition();
            const glm::vec3& getPosition() const;
            glm::quat& getRotationQuat();
            const glm::quat& getRotationQuat() const;
            glm::vec3& getScale();
            const glm::vec3& getScale() const;
            const glm::mat4 getTransformMatrix() const;

            void setPosition(const glm::vec3 &position);
            void setRotationQuat(const glm::quat &rotation);
            void setScale(const glm::vec3 &scale);
    };
}

#endif
