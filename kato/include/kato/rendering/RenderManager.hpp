#ifndef KATO_RENDERING_RENDERMANAGER_HPP
#define KATO_RENDERING_RENDERMANAGER_HPP

#include "kato_export.h"
#include "kato/rendering/ShaderProgram.hpp"
#include "kato/rendering/Texture2D.hpp"
#include "kato/rendering/VertexBuffer.hpp"
#include "kato/rendering/glplatform.hpp"
#include "kato/rendering/commands/IRenderCommand.hpp"
#include "kato/assets/StaticMeshAsset.hpp"
#include <queue>
#include <string>

namespace Kato {
    /**
     * @brief Don't forget to call init after constructig this
     *
     * init isn't called in the constructor as the correct OpenGL context may not be bound then
     */
    class KATO_EXPORT RenderManager {
        public:
            enum EnumVertexAttribs {
                VERTEX_POSITION = 0,
                VERTEX_NORMAL = 1,
                VERTEX_TEX_COORD = 2,
                VERTEX_COLOR = 3
            };

        public:
            //TEMP
            ShaderProgram *texturedShaderProg;
            ShaderProgram *coloredShaderProg;
            ShaderProgram *guiShaderProg;

            //Primitives
            StaticMeshAsset *planeStaticMesh;

            Texture2D *texture; //TEMP
            Texture2D *bgTexture; //TEMP
            MaterialAsset *mat; //TEMP
            MaterialAsset *bgMat; //TEMP

            //CPU caches of what's bound on the GPU, to prevent unnecessary GL calls
            const ShaderProgram *activeShaderProgram = nullptr;
            const Texture2D *activeTextures[32] = {0};
            GLuint activeVao = 0;

        protected:
            // static const uint64_t CACHE_TIMEOUT = 30 * 1000; //30s = 30 * 1000 ms

            std::deque<IRenderCommand*> renderFifo;

            //Fullscreen framebuffer
            GLuint fbo;
            GLuint fboColorTexture; //layout(location = 0)
            GLuint fboBrightTexture; //layout(location = 1) - For bloom
            GLuint fboDepthBuffer;

            GLuint fullscreenQuadVao;
            VertexBuffer *fullscreenQuadVbo;

            ShaderProgram *compositeShaderProg;
            GLuint compositeShaderTextureId;
            GLuint compositeShaderBloomTextureId;

            GLuint blurFbos[2]; //Used in a ping-pong when doing blur iterations
            GLuint blurColorTextures[2]; //Used in a ping-pong when doing blur iterations
            ShaderProgram *compositeBlurShaderProg;
            GLuint compositeBlurShaderTextureId;

            unsigned int bloomSpread = 10;

            int viewportWidth;
            int viewportHeight;

        protected:
            /**
             * @brief Generates textures/renderbuffers for the currently bound FBO
             *
             * @param fboWidth The width of the buffers to generate
             * @param fboHeight The height of the buffers to generate
             */
            void generateFboAttachments(int fboWidth, int fboHeight);
            void generateBlurFboAttachments(int fboWidth, int fboHeight);

        public:
            void init(int fboWidth, int fboHeight);

            /**
             * @brief Cleans up - note that the correct GL context must be bound
             */
            void destroy();

            /**
             * @brief Resizes the framebuffer attachments used for rendering
             *
             * @param width The width of the viewport
             * @param width The height of the viewport
             */
            void resizeViewport(int width, int height);

            /**
             * @brief Adds render commands to the render fifo for the node and all it's children recursively
             *
             * @param rootNode The node to start searching from
             * @param scene The scene
             */
            //void enqueueRenderScene(SceneNode *rootNode, const ResourceScene *scene);

            /**
             * @brief Adds the given render command to the render fifo
             *
             * The RenderManager will take ownership of the command and delete it after it's been executed
             *
             * @param command The render command
             */
            void enqueueRenderCommand(IRenderCommand *command);

            /**
             * @brief Recursively queues up the node and all the node's children to draw
             *
             * @param node The node to queue up and/or recursively iterate over its children to queue
             * @param selectionManager The scene
             * @param transform The world transform of the parent node
             */
            //void recursiveEnqueueSceneNode(
                    //WS2Common::Scene::SceneNode *node,
                    //const ResourceScene *scene,
                    //const glm::mat4 parentTransform
                    //);

            /**
             * @brief Adds a RenderMesh command to the render fifo, to be rendered later with renderQueue()
             *
             * @param mesh The mesh to render
             * @param transform The world transform of this mesh
             * @param renderCameraNormals Used for the selection outline
             */
            //void enqueueRenderMesh(
                    //const MeshSegment *mesh,
                    //glm::mat4 transform,
                    //glm::vec4 tint = glm::vec4(1.0f),
                    //bool renderCameraNormals = false
                    //);

            /**
             * @brief Renders all meshes in the render fifo
             *
             * @param targetFramebuffer The framebuffer to render to
             */
            void renderQueue(GLuint targetFramebuffer);

            /**
             * @brief Like renderQueue but doesn't configure framebuffers or do any other setup - just executes all queued command
             *
             * This should only generally be used to clean the queue of any pending cleanup commands
             */
            void flushQueue();

            /**
             * @brief Returns the GL texture for a ResourceTexture - will load one if it is not cached already
             *
             * @param tex The GL texture corresponding to the ResourceTexture provided
             */
            //CachedGlTexture* getTextureForResourceTexture(const ResourceTexture *tex);

            /**
             * @brief Unloads all shaders used in regular rendering (This does not include physics debug shaders)
             */
            void unloadShaders();

            //void addTexture(const QImage image, const ResourceTexture *tex);

            //void clearMeshCache();
            //void clearTextureCache();
            //void clearAllCaches();

            /**
             * @brief Checks for OpenGL errors and logs them if any are found
             *
             * @param location This text is tacked on to the end of the log message.
             *                 It's recommended you put where in the code the function is called, to aid with tracking
             *                 down issues.
             */
            static void checkErrors(const char *location);

            void setBloomSpread(unsigned int bloomSpread);
            unsigned int getBloomSpread();
    };
}

#endif
