#ifndef KATO_RENDERING_COMMANDS_IRENDERCOMMAND_HPP
#define KATO_RENDERING_COMMANDS_IRENDERCOMMAND_HPP

#include "kato_export.h"
#include "kato/commands/ICommand.hpp"

namespace Kato {
    class RenderManager;

    class KATO_EXPORT IRenderCommand : public ICommand {
        friend class RenderManager;

        protected:
            RenderManager *renderManager;
    };
}

#endif
