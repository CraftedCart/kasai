#ifndef KATO_RENDERING_COMMANDS_RENDERSTATICMESHENTITYCOMMAND_HPP
#define KATO_RENDERING_COMMANDS_RENDERSTATICMESHENTITYCOMMAND_HPP

#include "kato_export.h"
#include "kato/rendering/commands/IRenderCommand.hpp"
#include "kato/entities/StaticMeshEntity.hpp"
#include "kato/rendering/glplatform.hpp"

namespace Kato {
    class RenderManager;
}

namespace Kato {
    class KATO_EXPORT RenderStaticMeshEntityCommand : public IRenderCommand {
        private:
            const StaticMeshEntity *entity;
            const glm::mat4 projectionMatrix;

        public:
            RenderStaticMeshEntityCommand(
                    const StaticMeshEntity *entity,
                    const glm::mat4 projectionMatrix
                    );
            virtual void execute() override;
    };
}

#endif

