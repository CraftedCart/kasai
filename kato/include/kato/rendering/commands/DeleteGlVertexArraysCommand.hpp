#ifndef KATO_RENDERING_COMMANDS_DELETEGLVERTEXARRAYSCOMMAND_HPP
#define KATO_RENDERING_COMMANDS_DELETEGLVERTEXARRAYSCOMMAND_HPP

#include "kato_export.h"
#include "kato/rendering/commands/IRenderCommand.hpp"
#include "kato/rendering/glplatform.hpp"
#include <vector>

namespace Kato {
    class KATO_EXPORT DeleteGlVertexArraysCommand : public IRenderCommand {
        private:
            std::vector<GLuint> vertexArraysToDelete;

        public:
            DeleteGlVertexArraysCommand(std::vector<GLuint> vertexArraysToDelete);
            DeleteGlVertexArraysCommand(GLuint bufferToDelete);
            virtual void execute() override;
    };
}

#endif

