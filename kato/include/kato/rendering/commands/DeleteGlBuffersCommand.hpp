#ifndef KATO_RENDERING_COMMANDS_DELETEGLBUFFERSCOMMAND_HPP
#define KATO_RENDERING_COMMANDS_DELETEGLBUFFERSCOMMAND_HPP

#include "kato_export.h"
#include "kato/rendering/commands/IRenderCommand.hpp"
#include "kato/rendering/glplatform.hpp"
#include <vector>

namespace Kato {
    class KATO_EXPORT DeleteGlBuffersCommand : public IRenderCommand {
        private:
            std::vector<GLuint> buffersToDelete;

        public:
            DeleteGlBuffersCommand(std::vector<GLuint> buffersToDelete);
            DeleteGlBuffersCommand(GLuint bufferToDelete);
            virtual void execute() override;
    };
}

#endif

