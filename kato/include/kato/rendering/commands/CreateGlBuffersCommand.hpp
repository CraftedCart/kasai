#ifndef KATO_RENDERING_COMMANDS_CREATEGLBUFFERSCOMMAND_HPP
#define KATO_RENDERING_COMMANDS_CREATEGLBUFFERSCOMMAND_HPP

#include "kato_export.h"

namespace Kato {
    class KATO_EXPORT CreateGlBuffersCommand {
        public:
            CreateGlBuffersCommand();
            ~CreateGlBuffersCommand();
    };
}

#endif

