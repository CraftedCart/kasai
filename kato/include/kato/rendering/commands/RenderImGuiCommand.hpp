#ifndef KATO_RENDERING_COMMANDS_RENDERIMGUICOMMAND_HPP
#define KATO_RENDERING_COMMANDS_RENDERIMGUICOMMAND_HPP

#include "kato_export.h"
#include "kato/rendering/commands/IRenderCommand.hpp"
#include "kato/imguibinding/ImGuiSdlBinding.hpp"
#include "kato/rendering/glplatform.hpp"

namespace Kato {
    class RenderManager;
}

namespace Kato {
    class KATO_EXPORT RenderImGuiCommand : public IRenderCommand {
        private:
            ImGuiSdlBinding *binding;

        public:
            RenderImGuiCommand(ImGuiSdlBinding *binding);
            virtual void execute() override;
    };
}

#endif

