#ifndef KATO_RENDERING_VERTEXBUFFER_HPP
#define KATO_RENDERING_VERTEXBUFFER_HPP

#include "kato_export.h"
#include "kato/rendering/glplatform.hpp"

namespace Kato {
    //Forward declarations
    class RenderManager;

    class KATO_EXPORT VertexBuffer {
        private:
            GLuint bufferId;

            RenderManager &renderManager;

        public:
            /**
             * @note This will not create a vertex buffer on the GPU! - Use VertexBuffer::createVertexBuffer to do that
             */
            VertexBuffer(RenderManager &renderManager);
            ~VertexBuffer();

            GLuint getBufferId() const;
            void use() const;

            /**
             * @note Requires an OpenGL context to be bound
             */
            static VertexBuffer* createVertexBuffer(RenderManager &renderManager);
    };
}

#endif

