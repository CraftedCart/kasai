#ifndef KATO_RENDERING_TEXTURE2D_HPP
#define KATO_RENDERING_TEXTURE2D_HPP

#include "kato_export.h"
#include "kato/rendering/glplatform.hpp"
#include <boost/filesystem.hpp>

namespace Kato {
    class KATO_EXPORT Texture2D {
        private:
            GLuint textureId;

            glm::tvec2<uint32_t> size;

        public:
            Texture2D() = default;
            Texture2D(GLuint textureId, glm::tvec2<uint32_t> size);
            //TODO: Destructor to unload the texture

            GLuint getTextureId() const;
            void use() const;
            void use(GLenum activeTexture) const;

            glm::uvec2 getSize();
            uint32_t getSizeX();
            uint32_t getSizeY();

            static Texture2D* createTexture2D();

            static Texture2D* createTexture2DFromFile(
                    boost::filesystem::path &texturePath,
                    GLint internalFormat,
                    GLenum format,
                    GLint uWrapping = GL_REPEAT,
                    GLint vWrapping = GL_REPEAT
                    );
    };
}

#endif
