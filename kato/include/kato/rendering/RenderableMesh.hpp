#ifndef KATO_RENDERING_RENDERABLEMESH_HPP
#define KATO_RENDERING_RENDERABLEMESH_HPP

#include "kato_export.h"
#include "kato/rendering/VertexBuffer.hpp"
#include "kato/rendering/ElementBuffer.hpp"
#include "kato/rendering/glplatform.hpp"

namespace Kato {
    //Forward declarations
    class RenderManager;

    /**
     * Graphics buffer IDs to describe the vertex data of a mesh
     */
    class KATO_EXPORT RenderableMesh {
        private:
            GLuint vao;
            VertexBuffer *vbo;
            ElementBuffer *ebo;

            GLuint triangleCount;

            RenderManager &renderManager;

        public:
            RenderableMesh(RenderManager &renderManager);
            ~RenderableMesh();

            GLuint getVao() const;
            GLuint getTriangleCount() const;

            /**
             * @brief Creates a new RenderableMesh and gives the data passed to this to the GPU
             *
             * @param vboData Array of vertex data
             * @param vboSize How large the vertex data array is
             * @param eboData Array of vertex indices for triangles
             * @param eboSize How large the vertex indices array is
             * @param triangleCount How many triangles the mesh has (eboSize / 3)
             * @param renderManager The RenderManager for the GL context this will be created on
             */
            static RenderableMesh* createFromData(
                    GLfloat vboData[],
                    GLuint vboSize,
                    GLuint eboData[],
                    GLuint eboSize,
                    GLuint triangleCount,
                    RenderManager &renderManager
                    );
    };
}

#endif

