#ifndef KATO_RENDERING_ELEMENTBUFFER_HPP
#define KATO_RENDERING_ELEMENTBUFFER_HPP

#include "kato_export.h"
#include "kato/rendering/glplatform.hpp"

namespace Kato {
    //Forward declarations
    class RenderManager;

    class KATO_EXPORT ElementBuffer {
        private:
            GLuint bufferId;

            RenderManager &renderManager;

        public:
            /**
             * @note This will not create an element buffer on the GPU! - Use ElementBuffer::createElementBuffer to do that
             */
            ElementBuffer(RenderManager &renderManager);
            ~ElementBuffer();

            GLuint getBufferId() const;
            void use() const;

            /**
             * @note Requires an OpenGL context to be bound
             */
            static ElementBuffer* createElementBuffer(RenderManager &renderManager);
    };
}

#endif

