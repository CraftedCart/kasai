#ifndef KATO_RENDERING_SHADERPROGRAM_HPP
#define KATO_RENDERING_SHADERPROGRAM_HPP

#include "kato_export.h"
#include "kato/rendering/glplatform.hpp"
#include <boost/filesystem.hpp>
#include <string>

namespace Kato {
    class KATO_EXPORT ShaderProgram {
        private:
            GLuint programId;

        public:
            ShaderProgram(GLuint programId);

            GLuint getProgramId();
            void use() const;

            void setBool(const std::string &name, bool value) const;
            void setInt(const std::string &name, GLint value) const;
            void setFloat(const std::string &name, GLfloat value) const;
            void setVec4(const std::string &name, const glm::vec4 &value) const;
            void setMat4(const std::string &name, const glm::mat4 &value) const;

            /**
             * @brief Load, compile and link the shader files given
             *
             * @param vertFile The vertex shader file
             * @param fragFile The fragment shader file
             *
             * @return A ShaderProgram object
             *
             * @todo Handle errors
             */
            static ShaderProgram* createShaderProgram(
                    const boost::filesystem::path &vertFile,
                    const boost::filesystem::path &fragFile
                    );
    };
}

#endif

