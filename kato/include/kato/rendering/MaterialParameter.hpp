#ifndef KATO_RENDERING_MATERIALPARAMETER_HPP
#define KATO_RENDERING_MATERIALPARAMETER_HPP

#include "kato/rendering/EnumMaterialParameterType.hpp"
#include <glm/glm.hpp>

namespace Kato {
    union MaterialParameterType {
        glm::vec4 vec4;
    };

    struct MaterialParameter {
        std::string name;
        EnumMaterialParameterType type;
        MaterialParameterType value;
    };
}

#endif

