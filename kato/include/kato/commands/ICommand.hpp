#ifndef KATO_COMMANDS_ICOMMAND_HPP
#define KATO_COMMANDS_ICOMMAND_HPP

#include "kato_export.h"

namespace Kato {
    class KATO_EXPORT ICommand {
        public:
            virtual ~ICommand() {};
            virtual void execute() = 0;
    };
}

#endif
