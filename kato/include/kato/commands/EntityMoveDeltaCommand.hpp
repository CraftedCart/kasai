#ifndef KATO_COMMANDS_ENTITYMOVEDELTACOMMAND_HPP
#define KATO_COMMANDS_ENTITYMOVEDELTACOMMAND_HPP

#include "kato_export.h"
#include "kato/commands/ICommand.hpp"
#include "kato/entities/Entity.hpp"

namespace Kato {
    class KATO_EXPORT EntityMoveDeltaCommand : public ICommand {
        protected:
            Entity &ent;
            const glm::vec3 delta;

        public:
            EntityMoveDeltaCommand(Entity &ent, const glm::vec3 &delta);

            virtual void execute() override;
    };
}

#endif
