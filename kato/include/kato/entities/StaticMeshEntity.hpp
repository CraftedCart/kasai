#ifndef KATO_ENTITIES_STATICMESHENTITY_HPP
#define KATO_ENTITIES_STATICMESHENTITY_HPP

#include "kato_export.h"
#include "kato/entities/Entity.hpp"
#include "kato/assets/StaticMeshAsset.hpp"
#include "kato/rendering/MaterialParameter.hpp"

namespace Kato {
    class KATO_EXPORT StaticMeshEntity : public Entity {
        //Inherit ctors
        using Entity::Entity;

        protected:
            StaticMeshAsset *staticMeshAsset;
            MaterialAsset *overriddenMaterialAsset = nullptr;
            std::vector<MaterialParameter> materialParameters;

            int renderLayer = 0;

        public:
            StaticMeshEntity() = default;
            StaticMeshEntity(StaticMeshAsset *staticMeshAsset, MaterialAsset *overriddenMaterialAsset = nullptr);
            StaticMeshEntity(
                    const std::string &name,
                    StaticMeshAsset *staticMeshAsset,
                    MaterialAsset *overriddenMaterialAsset = nullptr
                    );

            void setStaticMeshAsset(StaticMeshAsset *staticMeshAsset);
            StaticMeshAsset* getStaticMeshAsset();
            const StaticMeshAsset* getStaticMeshAsset() const;

            //The static mesh entity can override materials
            void setOverriddenMaterialAsset(MaterialAsset *overriddenMaterialAsset);
            MaterialAsset* getMaterialAsset();
            const MaterialAsset* getMaterialAsset() const;

            void addMaterialParameter(MaterialParameter &param);
            std::vector<MaterialParameter>& getMaterialParameters();
            const std::vector<MaterialParameter>& getMaterialParameters() const;

            int getRenderLayer() const;
            void setRenderLayer(int renderLayer);
    };
}

#endif
