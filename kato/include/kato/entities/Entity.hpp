#ifndef KATO_ENTITIES_ENTITY_HPP
#define KATO_ENTITIES_ENTITY_HPP

#include "kato_export.h"
#include "kato/Transform.hpp"
#include <vector>
#include <string>

namespace Kato {
    class KATO_EXPORT Entity {
        protected:
            std::string name = "#Entity#";
            Transform transform = Transform();

            std::vector<Entity*> children;
            Entity *parent = nullptr;

        public:
            Entity() = default;
            Entity(const std::string &name);

            /**
              * @brief Deletes all children and removes entity from parent
              */
            virtual ~Entity();

            virtual void tick(float deltaSeconds);

            std::string& getName();
            const std::string& getName() const;
            void setName(const std::string &name);

            Transform& getTransform();
            const Transform& getTransform() const;
            void setTransform(const Transform &transform);

            std::vector<Entity*>& getChildren();
            const std::vector<Entity*>& getChildren() const;

            void addChild(Entity *child);
            void removeChild(Entity *child, bool shouldDelete = true);
            void removeFromParent(bool shouldDelete = true);

            /**
             * @brief Deletes all children and empties the children vector
             */
            void clearChildren();

            Entity* getParent();
            const Entity* getParent() const;

            const glm::mat4 getWorldTransformMatrix() const;
    };
}

#endif
