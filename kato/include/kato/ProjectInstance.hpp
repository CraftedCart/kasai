#ifndef KATO_PROJECTINSTANCE_HPP
#define KATO_PROJECTINSTANCE_HPP

#include "kato_export.h"
#include "kato/World.hpp"
#include <SDL2/SDL.h>

namespace Kato {
    //Forward declarations
    class EngineInstance;

    class KATO_EXPORT ProjectInstance {
        protected:
            World *world;
            EngineInstance *engine;

        public:
            ProjectInstance(EngineInstance &engine);
            virtual ~ProjectInstance();

            virtual void init();
            virtual void tick(float deltaSeconds);
            virtual void handleEvent(const SDL_Event &event);
            virtual void shutdown();

            World* getWorld();
    };
}

#endif
