#ifndef KATO_WORLD_HPP
#define KATO_WORLD_HPP

#include "kato_export.h"
#include "kato/entities/StaticMeshEntity.hpp"

namespace Kato {
    //Forward declarations
    class EngineInstance;

    class KATO_EXPORT World {
        protected:
            EngineInstance &engine;
            Entity *rootEntity;

            glm::vec2 fixedSceneSize = glm::vec2();

          protected:
            virtual void recursiveTickEntities(Entity *ent, float deltaSeconds);
            virtual void recursiveFindDrawableEntities(Entity *ent, float deltaSeconds, std::vector<StaticMeshEntity*> &found);

        public:
            World(Kato::EngineInstance &engine);
            virtual ~World();

            virtual void tick(float deltaSeconds);
            virtual void draw(float deltaSeconds);

            virtual void addEntity(Entity *ent);
            Entity* getRootEntity();

            const glm::vec2& getFixedSceneSize() const;
            glm::vec2& getFixedSceneSize();
            void setFixedSceneSize(glm::vec2 fixedSceneSize);
    };
}

#endif
