#ifndef KATO_ASSERTIONS_HPP
#define KATO_ASSERTIONS_HPP

#ifdef NDEBUG
#define KATO_ASSERT(...)
#else

#include "kato_export.h"
#include <string>

/**
 * Don't call any functions in this namespace! This namespace won't exist outside debug builds
 */
namespace Kato {
    KATO_EXPORT void assertFailed(const std::string &expr, const std::string &file, const unsigned int line);
    KATO_EXPORT void assertFailed(const std::string &expr, const std::string &message, const std::string &file, const unsigned int line);
}

#define KATO_ASSERT_GET_OVERLOAD(_1, _2, NAME, ...) NAME

#define KATO_ASSERT_NO_MESSAGE(expr) if (expr) {} else {::Kato::assertFailed(#expr, __FILE__, __LINE__);}
#define KATO_ASSERT_WITH_MESSAGE(expr, message) if (expr) {} else {::Kato::assertFailed(#expr, message, __FILE__, __LINE__);}

#define KATO_ASSERT(...) KATO_ASSERT_GET_OVERLOAD(__VA_ARGS__, KATO_ASSERT_WITH_MESSAGE, KATO_ASSERT_NO_MESSAGE)(__VA_ARGS__)

#endif

#endif

