#ifndef KATO_IMGUIBINDING_IMGUISDLBINDING_HPP
#define KATO_IMGUIBINDING_IMGUISDLBINDING_HPP

#include "kato_export.h"
#include "kato/rendering/RenderManager.hpp"
#include "kato/rendering/Texture2D.hpp"
#include "kato/rendering/ShaderProgram.hpp"
#include "kato/rendering/VertexBuffer.hpp"
#include "kato/rendering/ElementBuffer.hpp"
#include "imgui.h"
#include <SDL2/SDL.h>

namespace Kato {

    /**
     * @brief A binding for ImGui, enabling it to interact with the system (via SDL2) and render to the window
     */
    class KATO_EXPORT ImGuiSdlBinding {
        private:
            SDL_Window *window;
            SDL_Cursor* mouseCursors[ImGuiMouseCursor_COUNT] = {0};
            char *clipboardTextData = nullptr;
            bool mousePressed[3] = {false, false, false};

            RenderManager *renderManager;

            Texture2D *fontTexture = nullptr;
            ShaderProgram *guiShaderProg = nullptr;
            VertexBuffer *vbo = nullptr;
            ElementBuffer *ebo = nullptr;

        private:
            static const char* getClipboardText(void *instance);
            static void setClipboardText(void *instance, const char *text);

            void loadRenderables();
            void updateMouse();

        public:
            ImGuiSdlBinding(SDL_Window *window, RenderManager *renderManager);
            ~ImGuiSdlBinding();

            /**
             * @param event The SDL event
             *
             * @return Whether ImGui handled the event or not
             */
            bool handleEvent(const SDL_Event &event);

            /**
             * @brief Call at the beginning of every frame to setup ImGui for drawing
             *
             * @param deltaSeconds The delta time since the last frame, in seconds
             */
            void newFrame(float deltaSeconds);

            /**
             * @brief Renders draw data
             *
             * @param drawData The draw data to render
             */
            void draw(ImDrawData *drawData);
    };
}

#endif
