#ifndef KATO_EDITOR_EDITORMAINVIEW_HPP
#define KATO_EDITOR_EDITORMAINVIEW_HPP

#include "kato_export.h"
#include "kato/assets/DirectoryAsset.hpp"
#include "kato/EngineInstance.hpp"

namespace Kato {
    class KATO_EXPORT EditorMainView {
        private:
            EngineInstance *engine;

        private:
            //Used for the asset browser
            void recursiveDrawAssetTree(DirectoryAsset *dir);
            //Used for the world outliner
            void recursiveDrawWorldOutlinerTree(Entity *ent);

        public:
            EditorMainView(EngineInstance &engine);

            void tick(float deltaSeconds, float realDeltaSeconds);

            void drawFrameGraph(float realDeltaSeconds);
            void drawAssetBrowser();
            void drawWorldOutliner();
    };
}

#endif

