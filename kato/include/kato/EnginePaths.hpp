#ifndef KATO_ENGINEPATHS_HPP
#define KATO_ENGINEPATHS_HPP

#include "kato_export.h"
#include <boost/filesystem/path.hpp>

namespace Kato::EnginePaths {
    /**
     * @brief Fetches the executable path of the engine launcher
     *
     * @return engineInstallDir/bin/katolauncher(.exe)
     */
    KATO_EXPORT boost::filesystem::path getExecutablePath();

    /**
     * @brief Fetches the directory containing the engine launcher executable
     *
     * @return engineInstallDir/bin
     */
    KATO_EXPORT boost::filesystem::path getExecutableDirectoryPath();

    /**
     * @brief Fetches the directory containing the engine installation
     *
     * @return engineInstallDir
     */
    KATO_EXPORT boost::filesystem::path getInstallDirectoryPath();

    /**
     * @brief Fetches the directory containing various resources
     *
     * @return engineInstallDir/share/kato
     */
    KATO_EXPORT boost::filesystem::path getResourceDirectoryPath();

    /**
     * @brief Fetches the directory containing various engine resources
     *
     * @return engineInstallDir/share/kato/engine
     */
    KATO_EXPORT boost::filesystem::path getEngineResourceDirectoryPath();
}

#endif

