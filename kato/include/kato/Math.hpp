#ifndef KATO_MATH_HPP
#define KATO_MATH_HPP

#include "kato_export.h"

namespace Kato::Math {
    KATO_EXPORT double fastSin(double x);
    KATO_EXPORT double fastCos(double x);
}

#endif

