#ifndef KATO_MAIN_HPP
#define KATO_MAIN_HPP

#include "kato_export.h"

namespace Kato {
	KATO_EXPORT int launchEngine([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]);
}

#endif
