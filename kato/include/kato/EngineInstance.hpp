#ifndef KATO_ENGINEINSTANCE_HPP
#define KATO_ENGINEINSTANCE_HPP

#include "kato_export.h"
#include "kato/rendering/RenderManager.hpp"
#include "kato/imguibinding/ImGuiSdlBinding.hpp"
#include "kato/ProjectInstance.hpp"
#include <SDL2/SDL.h>

namespace Kato {
    //Forward declarations
    class EditorMainView;

    class KATO_EXPORT EngineInstance {
        private:
            static EngineInstance *instance;

            std::vector<std::string> commandArgs;

            SDL_Window *sdlWindow;
            RenderManager renderManager;
            ImGuiSdlBinding *imGuiBinding;

            /**
             * If negative, this is ignored
             */
            float fixedTimestep = -1.0f;

            ProjectInstance *project;

            //Editor stuffs
            bool usingEditor = false;
            EditorMainView *editorView;

        private:
            EngineInstance(int argc, char *argv[]);

        public:
            static EngineInstance& initInstance(int argc, char *argv[]);
            static EngineInstance* getInstance();

            /**
             * @param deltaSeconds The delta time between frames, or the fixed timestep if one is set
             * @param realDeltaSeconds The delta time between frames, regardless of any fixed timestep
             */
            void tick(float deltaSeconds, float realDeltaSeconds);

            void handleEvent(const SDL_Event &event);

            void requestQuit();
            void shutdown();

            bool isUsingEditor();

            ProjectInstance* getProject();

            SDL_Window* getSdlWindow();
            RenderManager& getRenderManager();
            ImGuiSdlBinding& getImGuiBinding();

            void setFixedTimestep(float fixedTimestep);
            float getFixedTimestep();

            glm::ivec2 getViewportSize();
            const std::vector<std::string>& getCommandArgs();
    };
}

#endif
