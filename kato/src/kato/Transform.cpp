#include "kato/Transform.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Kato {
    Transform::Transform(glm::vec3 position, glm::quat rotation, glm::vec3 scale) :
        position(position),
        rotation(rotation),
        scale(scale) {}

    glm::vec3& Transform::getPosition() {
        return position;
    }

    const glm::vec3& Transform::getPosition() const {
        return position;
    }

    glm::quat& Transform::getRotationQuat() {
        return rotation;
    }

    const glm::quat& Transform::getRotationQuat() const {
        return rotation;
    }

    glm::vec3& Transform::getScale() {
        return scale;
    }

    const glm::vec3& Transform::getScale() const {
        return scale;
    }

    const glm::mat4 Transform::getTransformMatrix() const {
        if (cachedMatrixDirty) {
            cachedTransformMatrix = glm::mat4(1.0f) * glm::translate(position) * glm::toMat4(rotation) * glm::scale(scale);
            cachedMatrixDirty = false;
        }

        return cachedTransformMatrix;
    }

    void Transform::setPosition(const glm::vec3 &position) {
        this->position = position;
        cachedMatrixDirty = true;
    }

    void Transform::setRotationQuat(const glm::quat &rotation) {
        this->rotation = rotation;
        cachedMatrixDirty = true;
    }

    void Transform::setScale(const glm::vec3 &scale) {
        this->scale = scale;
        cachedMatrixDirty = true;
    }
}
