#include "kato/assets/StaticMeshAsset.hpp"

namespace Kato {
    StaticMeshAsset::StaticMeshAsset(RenderableMesh *renderableMesh, MaterialAsset *materialAsset) :
        renderableMesh(renderableMesh),
        materialAsset(materialAsset) {}

    void StaticMeshAsset::setRenderableMesh(RenderableMesh *renderableMesh) {
        this->renderableMesh = renderableMesh;
    }

    RenderableMesh* StaticMeshAsset::getRenderableMesh() {
        return renderableMesh;
    }

    const RenderableMesh* StaticMeshAsset::getRenderableMesh() const {
        return renderableMesh;
    }

    void StaticMeshAsset::setMaterialAsset(MaterialAsset* materialAsset) {
        this->materialAsset = materialAsset;
    }

    MaterialAsset* StaticMeshAsset::getMaterialAsset() {
        return materialAsset;
    }

    const MaterialAsset* StaticMeshAsset::getMaterialAsset() const {
        return materialAsset;
    }
}

