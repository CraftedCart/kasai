#include "kato/assets/DirectoryAsset.hpp"

namespace Kato {
    DirectoryAsset::DirectoryAsset(const std::string &name) :
        AbstractAsset(name) {}

    DirectoryAsset::~DirectoryAsset() {
        for (AbstractAsset *child : children) delete child;
    }

    void DirectoryAsset::addChild(AbstractAsset *child) {
        children.push_back(child);
    }

    std::vector<AbstractAsset*>& DirectoryAsset::getChildren() {
        return children;
    }

    const std::vector<AbstractAsset*>& DirectoryAsset::getChildren() const {
        return children;
    }

    AbstractAsset* DirectoryAsset::getAssetByPath(const std::string path) {
        //Check if we're referring to this object
        if (path.length() == 0) return this;
        if (path == ".") return this;

        //See if there's a / in the path
        std::string::size_type pos = path.find('/');
        if (pos != std::string::npos) {
            //Found a /
            //Which means there must be more path
            std::string childName = path.substr(0, pos);
            if (pos + 2 < path.length()) {
                //We don't end with a slash
                if (DirectoryAsset *dir = dynamic_cast<DirectoryAsset*>(getAssetByName(childName))) {
                    //Found a subdirectory
                    return dir->getAssetByPath(path.substr(pos + 1));
                } else {
                    //Something invalid was given
                    return nullptr;
                }
            } else {
                //We are at the end of a path that ends with a /
                return getAssetByName(childName);
            }
        } else {
            std::string childName = path;
            return getAssetByName(childName);
        }
    }

    const AbstractAsset* DirectoryAsset::getAssetByPath(const std::string path) const {
        //See if there's a / in the path
        if (std::string::size_type pos = path.find('/') != std::string::npos) {
            //Found a /
            //Which means there must be more path
            std::string childName = path.substr(0, pos);

            if (pos + 2 < path.length()) {
                //We don't end with a slash
                if (const DirectoryAsset *dir = dynamic_cast<const DirectoryAsset*>(getAssetByName(childName))) {
                    //Found a subdirectory
                    return dir->getAssetByPath(path.substr(pos + 1));
                } else {
                    //Something invalid was given
                    return nullptr;
                }
            } else {
                //We are at the end of a path that ends with a /
                return getAssetByName(childName);
            }
        } else {
            std::string childName = path;
            return getAssetByName(childName);
        }
    }

    DirectoryAsset* DirectoryAsset::getDirectoryByPath(const std::string path) {
        return dynamic_cast<DirectoryAsset*>(getAssetByPath(path));
    }

    const DirectoryAsset* DirectoryAsset::getDirectoryByPath(const std::string path) const {
        return dynamic_cast<const DirectoryAsset*>(getAssetByPath(path));
    }

    AbstractAsset* DirectoryAsset::getAssetByName(const std::string name) {
        for (AbstractAsset *child : children) {
            if (child->getName() == name) return child;
        }

        //Nothing found
        return nullptr;
    }

    const AbstractAsset* DirectoryAsset::getAssetByName(const std::string name) const {
        for (AbstractAsset *child : children) {
            if (child->getName() == name) return child;
        }

        //Nothing found
        return nullptr;
    }
}

