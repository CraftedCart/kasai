#include "kato/assets/MaterialAsset.hpp"

namespace Kato {
    MaterialAsset::MaterialAsset(ShaderProgram *shader, Texture2D *texture) :
        shader(shader),
        texture(texture) {}

    ShaderProgram* MaterialAsset::getShader() {
        return shader;
    }

    const ShaderProgram* MaterialAsset::getShader() const {
        return shader;
    }

    Texture2D* MaterialAsset::getTexture() {
        return texture;
    }

    const Texture2D* MaterialAsset::getTexture() const {
        return texture;
    }
}

