#include "kato/assets/AbstractAsset.hpp"

namespace Kato {
    AbstractAsset::AbstractAsset(const std::string &name) : name(name) {}

    const std::string& AbstractAsset::getName() const {
        return name;
    }

    void AbstractAsset::setName(const std::string &name) {
        this->name = name;
    }
}

