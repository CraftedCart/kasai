#include "kato/assets/AssetManager.hpp"

namespace Kato {
    DirectoryAsset* AssetManager::getRootDir() {
        return rootDir;
    }

    const DirectoryAsset* AssetManager::getRootDir() const {
        return rootDir;
    }

    AbstractAsset* AssetManager::getAssetByPath(const std::string path) {
        //Strip off the leading slash if any
        if (!path.compare(0, 1, "/")) {
            return rootDir->getAssetByPath(path.substr(1));
        } else {
            return rootDir->getAssetByPath(path);
        }
    }

    const AbstractAsset* AssetManager::getAssetByPath(const std::string path) const {
        //Strip off the leading slash if any
        if (!path.compare(0, 1, "/")) {
            return rootDir->getAssetByPath(path.substr(1));
        } else {
            return rootDir->getAssetByPath(path);
        }
    }

    DirectoryAsset* AssetManager::getDirectoryByPath(const std::string path) {
        return dynamic_cast<DirectoryAsset*>(getAssetByPath(path));
    }

    const DirectoryAsset* AssetManager::getDirectoryByPath(const std::string path) const {
        return dynamic_cast<const DirectoryAsset*>(getAssetByPath(path));
    }

    AssetManager& AssetManager::getInstance() {
        static AssetManager instance;
        return instance;
    }
}

