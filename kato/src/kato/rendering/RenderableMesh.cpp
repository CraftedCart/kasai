#include "kato/rendering/RenderableMesh.hpp"
#include "kato/rendering/RenderManager.hpp"
#include "kato/rendering/commands/DeleteGlBuffersCommand.hpp"
#include "kato/rendering/commands/DeleteGlVertexArraysCommand.hpp"

namespace Kato {
    RenderableMesh::RenderableMesh(RenderManager &renderManager) : renderManager(renderManager) {}

    RenderableMesh::~RenderableMesh() {
        delete vbo;
        delete ebo;

        DeleteGlVertexArraysCommand *cmdVertexArrays = new DeleteGlVertexArraysCommand(vao);
        renderManager.enqueueRenderCommand(cmdVertexArrays);
    }

    GLuint RenderableMesh::getVao() const {
        return vao;
    }

    GLuint RenderableMesh::getTriangleCount() const {
        return triangleCount;
    }

    RenderableMesh* RenderableMesh::createFromData(
            GLfloat vboData[],
            GLuint vboSize,
            GLuint eboData[],
            GLuint eboSize,
            GLuint triangleCount,
            RenderManager &renderManager
            ) {
        RenderableMesh *mesh = new RenderableMesh(renderManager);

        mesh->triangleCount = triangleCount;

        //Generate our VAO
        glGenVertexArrays(1, &mesh->vao);
        glBindVertexArray(mesh->vao);

        //Generate our VBO
        mesh->vbo = VertexBuffer::createVertexBuffer(renderManager);
        mesh->vbo->use();
        glBufferData(GL_ARRAY_BUFFER, vboSize, vboData, GL_STATIC_DRAW);

        //Generate our EBO
        mesh->ebo = ElementBuffer::createElementBuffer(renderManager);
        mesh->ebo->use();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboSize, eboData, GL_STATIC_DRAW);

        glVertexAttribPointer(
                RenderManager::EnumVertexAttribs::VERTEX_POSITION,
                3,
                GL_FLOAT,
                GL_FALSE,
                5 * sizeof(float),
                (void*) 0
                ); //Pos

        glVertexAttribPointer(
                RenderManager::EnumVertexAttribs::VERTEX_TEX_COORD,
                2,
                GL_FLOAT,
                GL_FALSE,
                5 * sizeof(float),
                (void*) (3 * sizeof(float))
                ); //UV

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(2);

        return mesh;
    }
}

