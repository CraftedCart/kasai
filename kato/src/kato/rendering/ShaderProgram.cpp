#include "kato/rendering/ShaderProgram.hpp"
#include "kato/FileUtils.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

namespace Kato {
    ShaderProgram::ShaderProgram(GLuint programId) :
        programId(programId) {}

    GLuint ShaderProgram::getProgramId() {
        return programId;
    }

    void ShaderProgram::use() const {
        glUseProgram(programId);
    }

    void ShaderProgram::setBool(const std::string &name, bool value) const {
        glUniform1i(glGetUniformLocation(programId, name.c_str()), (GLint) value);
    }

    void ShaderProgram::setInt(const std::string &name, GLint value) const {
        glUniform1i(glGetUniformLocation(programId, name.c_str()), value);
    }

    void ShaderProgram::setFloat(const std::string &name, GLfloat value) const {
        glUniform1f(glGetUniformLocation(programId, name.c_str()), value);
    }

    void ShaderProgram::setVec4(const std::string &name, const glm::vec4 &value) const {
        glUniform4fv(glGetUniformLocation(programId, name.c_str()), 1, glm::value_ptr(value));
    }

    void ShaderProgram::setMat4(const std::string &name, const glm::mat4 &value) const {
        glUniformMatrix4fv(glGetUniformLocation(programId, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
    }

    ShaderProgram* ShaderProgram::createShaderProgram(
            const boost::filesystem::path &vertFile,
            const boost::filesystem::path &fragFile
            ) {
        //Create the shaders
        GLuint vertID = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragID = glCreateShader(GL_FRAGMENT_SHADER);

        //Read the vert shader code
        boost::filesystem::ifstream vertStream;
        vertStream.open(vertFile);
        if (vertStream.fail()) {
            //Failed to open the file
            //TODO: Handle failing to open file - like throwing an exception or something
            std::cerr << "Failed to open file: " << vertFile.string() << std::endl;
            return 0;
        }
        std::string vertCode = FileUtils::readIfstreamToString(vertStream);
        vertStream.close();

        //Read the frag shader code
        boost::filesystem::ifstream fragStream;
        fragStream.open(fragFile);
        if (fragStream.fail()) {
            //Failed to open the file
            //TODO: Handle failing to open file - like throwing an exception or something
            std::cerr << "Failed to open file: " << fragFile.string() << std::endl;
            return 0;
        }
        std::string fragCode = FileUtils::readIfstreamToString(fragStream);
        fragStream.close();

        GLint result = GL_FALSE;
        int infoLogLength;

        const char *cVertCode = vertCode.c_str();
        const char *cFragCode = fragCode.c_str();

        //Compile vert shader
        std::cout << "Compiling vertex shader: " << vertFile.string() << std::endl;
        glShaderSource(vertID, 1, &cVertCode, NULL);
        glCompileShader(vertID);

        //Check vert shader
        glGetShaderiv(vertID, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE) {
            glGetShaderiv(vertID, GL_INFO_LOG_LENGTH, &infoLogLength);
            std::vector<char> vertShaderErrMsg(infoLogLength + 1);
            glGetShaderInfoLog(vertID, infoLogLength, NULL, &vertShaderErrMsg[0]);
            std::cerr << &vertShaderErrMsg[0] << std::endl;

            //TODO: Handle error
            return 0;
        }

        //Compile frag shader
        std::cout << "Compiling fragment shader: " << fragFile.string() << std::endl;
        glShaderSource(fragID, 1, &cFragCode, NULL);
        glCompileShader(fragID);

        //Check frag shader
        glGetShaderiv(fragID, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE) {
            glGetShaderiv(fragID, GL_INFO_LOG_LENGTH, &infoLogLength);
            std::vector<char> fragShaderErrMsg(infoLogLength + 1);
            glGetShaderInfoLog(fragID, infoLogLength, NULL, &fragShaderErrMsg[0]);
            std::cerr << &fragShaderErrMsg[0] << std::endl;

            //TODO: Handle error
            return 0;
        }

        //Link the program
        std::cout << "Linking program" << std::endl;
        GLuint programID = glCreateProgram();
        glAttachShader(programID, vertID);
        glAttachShader(programID, fragID);
        glLinkProgram(programID);

        //Check program
        glGetProgramiv(programID, GL_LINK_STATUS, &result);
        if (result == GL_FALSE) {
            glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
            std::vector<char> progErrMsg(infoLogLength + 1);
            glGetProgramInfoLog(programID, infoLogLength, NULL, &progErrMsg[0]);
            std::cout << &progErrMsg[0] << std::endl;

            //TODO: Handle error
        }

        //Cleanup
        glDetachShader(programID, vertID);
        glDetachShader(programID, fragID);

        glDeleteShader(vertID);
        glDeleteShader(fragID);

        return new ShaderProgram(programID);
    }
}

