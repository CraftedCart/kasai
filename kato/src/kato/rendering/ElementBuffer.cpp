#include "kato/rendering/ElementBuffer.hpp"
#include "kato/rendering/RenderManager.hpp"
#include "kato/rendering/commands/DeleteGlBuffersCommand.hpp"

namespace Kato {
    ElementBuffer::ElementBuffer(RenderManager &renderManager) : renderManager(renderManager) {}

    ElementBuffer::~ElementBuffer() {
        std::vector buffersToDelete = {bufferId};

        DeleteGlBuffersCommand *cmdBuffers = new DeleteGlBuffersCommand(buffersToDelete);
        renderManager.enqueueRenderCommand(cmdBuffers);
    }

    GLuint ElementBuffer::getBufferId() const {
        return bufferId;
    }

    void ElementBuffer::use() const {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
    }

    ElementBuffer* ElementBuffer::createElementBuffer(RenderManager &renderManager) {
        ElementBuffer *vbo = new ElementBuffer(renderManager);
        glGenBuffers(1, &vbo->bufferId);

        return vbo;
    }
}

