#include "kato/rendering/VertexBuffer.hpp"
#include "kato/rendering/RenderManager.hpp"
#include "kato/rendering/commands/DeleteGlBuffersCommand.hpp"

namespace Kato {
    VertexBuffer::VertexBuffer(RenderManager &renderManager) : renderManager(renderManager) {}

    VertexBuffer::~VertexBuffer() {
        std::vector buffersToDelete = {bufferId};

        DeleteGlBuffersCommand *cmdBuffers = new DeleteGlBuffersCommand(buffersToDelete);
        renderManager.enqueueRenderCommand(cmdBuffers);
    }

    GLuint VertexBuffer::getBufferId() const {
        return bufferId;
    }

    void VertexBuffer::use() const {
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    }

    VertexBuffer* VertexBuffer::createVertexBuffer(RenderManager &renderManager) {
        VertexBuffer *vbo = new VertexBuffer(renderManager);
        glGenBuffers(1, &vbo->bufferId);

        return vbo;
    }
}

