#include "kato/rendering/commands/RenderImGuiCommand.hpp"
#include "kato/rendering/RenderManager.hpp"

namespace Kato {
    RenderImGuiCommand::RenderImGuiCommand(ImGuiSdlBinding *binding) : binding(binding) {}

    void RenderImGuiCommand::execute() {
        ImGui::Render();
        binding->draw(ImGui::GetDrawData());
    }
}
