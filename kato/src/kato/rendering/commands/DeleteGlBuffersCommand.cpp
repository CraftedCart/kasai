#include "kato/rendering/commands/DeleteGlBuffersCommand.hpp"

namespace Kato {
    DeleteGlBuffersCommand::DeleteGlBuffersCommand(std::vector<GLuint> buffersToDelete) :
        buffersToDelete(buffersToDelete) {}

    DeleteGlBuffersCommand::DeleteGlBuffersCommand(GLuint bufferToDelete) {
        buffersToDelete.push_back(bufferToDelete);
    }

    void DeleteGlBuffersCommand::execute() {
        glDeleteBuffers(buffersToDelete.size(), buffersToDelete.data());
    }
}

