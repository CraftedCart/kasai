#include "kato/rendering/commands/DeleteGlVertexArraysCommand.hpp"

namespace Kato {
    DeleteGlVertexArraysCommand::DeleteGlVertexArraysCommand(std::vector<GLuint> vertexArraysToDelete) :
        vertexArraysToDelete(vertexArraysToDelete) {}

    DeleteGlVertexArraysCommand::DeleteGlVertexArraysCommand(GLuint bufferToDelete) {
        vertexArraysToDelete.push_back(bufferToDelete);
    }

    void DeleteGlVertexArraysCommand::execute() {
        glDeleteVertexArrays(vertexArraysToDelete.size(), vertexArraysToDelete.data());
    }
}

