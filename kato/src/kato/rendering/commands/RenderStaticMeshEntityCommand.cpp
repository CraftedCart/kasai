#include "kato/rendering/commands/RenderStaticMeshEntityCommand.hpp"
#include "kato/rendering/RenderManager.hpp"

namespace Kato {
    RenderStaticMeshEntityCommand::RenderStaticMeshEntityCommand(
            const StaticMeshEntity *entity,
            const glm::mat4 projectionMatrix
            ) :
        entity(entity),
        projectionMatrix(projectionMatrix) {}

    void RenderStaticMeshEntityCommand::execute() {
        const MaterialAsset *mat = entity->getMaterialAsset();
        const RenderableMesh *mesh = entity->getStaticMeshAsset()->getRenderableMesh();
        const glm::mat4 modelMatrix = entity->getWorldTransformMatrix();

        if (renderManager->activeShaderProgram != mat->getShader()) {
            mat->getShader()->use();
            renderManager->activeShaderProgram = mat->getShader();
        }

        mat->getShader()->setMat4("transformMatrix", projectionMatrix * modelMatrix);

        //Set parameters
        for (const MaterialParameter &param : entity->getMaterialParameters()) {
            switch (param.type) {
                case VEC4:
                    mat->getShader()->setVec4(param.name, param.value.vec4);
                    break;
            }
        }

        // mat->getShader()->setVec4("tint", glm::vec4(16.0f, 16.0f, 16.0f, 1.0f));
        // mat->getShader()->setVec4("tint", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

        if (mat->getTexture() != nullptr && renderManager->activeTextures[0] != mat->getTexture()) {
            mat->getTexture()->use(GL_TEXTURE0);
            renderManager->activeTextures[0] = mat->getTexture();
        }

        if (renderManager->activeVao != mesh->getVao()) {
            glBindVertexArray(mesh->getVao());
            renderManager->activeVao = mesh->getVao();
        }

        glDrawElements(GL_TRIANGLES, mesh->getTriangleCount() * 3, GL_UNSIGNED_INT, 0);
    }
}
