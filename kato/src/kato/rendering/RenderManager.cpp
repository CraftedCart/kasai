#include "kato/rendering/RenderManager.hpp"
#include "kato/rendering/glplatform.hpp"
#include "kato/assets/AssetManager.hpp"
#include "kato/EnginePaths.hpp"
#include <iostream>

namespace Kato {
    void RenderManager::init(int fboWidth, int fboHeight) {
        std::cout << "Initializing RenderManager" << std::endl;

        viewportWidth = fboWidth;
        viewportHeight = fboHeight;

        //qDebug() << "Loading default texture";
        //QImage defaultImage = convertToGLFormat(QImage(":/Workshop2/Images/defaultgrid.png"));
        //defaultTexture = loadTexture(defaultImage);

        //Create fbo
        std::cout << "Creating framebuffers" << std::endl;
        //Main FBO
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        //Create fbo textures/buffers
        generateFboAttachments(fboWidth, fboHeight);

        //Blur FBOs for bloom
        glGenFramebuffers(2, blurFbos);
        glGenTextures(2, blurColorTextures);

        generateBlurFboAttachments(fboWidth, fboHeight);

        //Generate the fullscreen quad buffers, to draw the framebuffer
        std::cout << "Creating fullscreen quad buffers" << std::endl;

        glGenVertexArrays(1, &fullscreenQuadVao);
        glBindVertexArray(fullscreenQuadVao);

        static const GLfloat fullscreenQuadVerts[] = {
            -1.0f, -1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                -1.0f,  1.0f, 0.0f,
                -1.0f,  1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                1.0f,  1.0f, 0.0f,
        };

        fullscreenQuadVbo = VertexBuffer::createVertexBuffer(*this);
        fullscreenQuadVbo->use();
        glBufferData(GL_ARRAY_BUFFER, sizeof(fullscreenQuadVerts), fullscreenQuadVerts, GL_STATIC_DRAW);

        glEnableVertexAttribArray(EnumVertexAttribs::VERTEX_POSITION);
        glVertexAttribPointer(EnumVertexAttribs::VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

        std::cout << "Creating shaders" << std::endl;

        //Load the shaders
        boost::filesystem::path shadersDir = EnginePaths::getEngineResourceDirectoryPath();
        shadersDir /= "shaders";

        boost::filesystem::path texturedVertFile(shadersDir / "textured.vert");
        boost::filesystem::path texturedFragFile(shadersDir / "textured.frag");
        texturedShaderProg = ShaderProgram::createShaderProgram(texturedVertFile, texturedFragFile);

        boost::filesystem::path coloredVertFile(shadersDir / "colored.vert");
        boost::filesystem::path coloredFragFile(shadersDir / "colored.frag");
        coloredShaderProg = ShaderProgram::createShaderProgram(coloredVertFile, coloredFragFile);

        boost::filesystem::path guiVertFile(shadersDir / "gui.vert");
        boost::filesystem::path guiFragFile(shadersDir / "gui.frag");
        guiShaderProg = ShaderProgram::createShaderProgram(guiVertFile, guiFragFile);

        //Load texture
        boost::filesystem::path texturesDir = EnginePaths::getEngineResourceDirectoryPath();
        texturesDir /= "textures";
        boost::filesystem::path texturePath = texturesDir / "cursor.png";
        texture = Texture2D::createTexture2DFromFile(texturePath, GL_RGBA8, GL_RGBA);

        texturePath = texturesDir / "background.png";
        bgTexture = Texture2D::createTexture2DFromFile(texturePath, GL_RGBA8, GL_RGBA);

        boost::filesystem::path compositeVertFile(shadersDir / "composite.vert");
        boost::filesystem::path compositeFragFile(shadersDir / "composite.frag");
        compositeShaderProg = ShaderProgram::createShaderProgram(compositeVertFile, compositeFragFile);
        compositeShaderTextureId = glGetUniformLocation(compositeShaderProg->getProgramId(), "texSampler");
        compositeShaderBloomTextureId = glGetUniformLocation(compositeShaderProg->getProgramId(), "bloomTexSampler");
        compositeBlurShaderTextureId = glGetUniformLocation(compositeShaderProg->getProgramId(), "texSampler");

        boost::filesystem::path compositeBlurFragFile(shadersDir / "compositeBlur.frag");
        compositeBlurShaderProg = ShaderProgram::createShaderProgram(compositeVertFile, compositeBlurFragFile);

        //Create primitives
        GLfloat planeVertices[] = {
            //Positions        //Texture coords
            +0.5f, +0.5f, 0.0f,  1.0f, 1.0f, //Top right
            +0.5f, -0.5f, 0.0f,  1.0f, 0.0f, //Bottom right
            -0.5f, -0.5f, 0.0f,  0.0f, 0.0f, //Bottom left
            -0.5f, +0.5f, 0.0f,  0.0f, 1.0f  //Top left
        };

        GLuint planeIndices[] = {
            3, 1, 0,
            3, 2, 1
        };

        //Setup asset directories
        AssetManager::getInstance().getDirectoryByPath("/kato")->addChild(new DirectoryAsset("materials"));

        mat = new MaterialAsset(texturedShaderProg, texture);
        mat->setName("mat");
        AssetManager::getInstance().getDirectoryByPath("/kato/materials")->addChild(mat);

        bgMat = new MaterialAsset(texturedShaderProg, bgTexture);
        bgMat->setName("bgMat");
        AssetManager::getInstance().getDirectoryByPath("/kato/materials")->addChild(bgMat);

        RenderableMesh *mesh = RenderableMesh::createFromData(
                planeVertices,
                sizeof(planeVertices),
                planeIndices,
                sizeof(planeIndices),
                2,
                *this
                );
        planeStaticMesh = new StaticMeshAsset(mesh, bgMat);

        //Some OpenGL configuration
        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        checkErrors("After RenderManager::init()");
    }

    void RenderManager::destroy() {
        unloadShaders();

        //Delete textures
        GLuint texturesToDelete[] = {
            fboColorTexture,
                fboBrightTexture,
                blurColorTextures[0],
                blurColorTextures[1],
                texture->getTextureId(),
                bgTexture->getTextureId()
        };
        glDeleteTextures(4, texturesToDelete);

        delete planeStaticMesh;

        //Delete buffers
        delete fullscreenQuadVbo;

        delete texture;
        delete bgTexture;
        delete mat;
        delete bgMat;

        glDeleteRenderbuffers(1, &fboDepthBuffer);
        glDeleteFramebuffers(1, &fbo);
        glDeleteFramebuffers(2, blurFbos);

        glDeleteVertexArrays(1, &fullscreenQuadVao);

        //Render the queue one last time to run any deletion commands
        flushQueue();

        checkErrors("End of RenderManager::destroy()");
    }

    void RenderManager::generateFboAttachments(int fboWidth, int fboHeight) {
        //Now with ~ ~ H D R ~ ~

        //Color texture
        glGenTextures(1, &fboColorTexture);
        glBindTexture(GL_TEXTURE_2D, fboColorTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, fboWidth, fboHeight, 0, GL_RGB, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fboColorTexture, 0);

        //Bright texture (for bloom)
        glGenTextures(1, &fboBrightTexture);
        glBindTexture(GL_TEXTURE_2D, fboBrightTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, fboWidth, fboHeight, 0, GL_RGB, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, fboBrightTexture, 0);

        //Depth buffer
        glGenRenderbuffers(1, &fboDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, fboDepthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, fboWidth, fboHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fboDepthBuffer);

        //Set the draw buffers list
        GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
        glDrawBuffers(2, drawBuffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            //TODO: Throw an exception or something
            std::cerr << "fbo is incomplete" << std::endl;
        }
    }

    void RenderManager::generateBlurFboAttachments(int fboWidth, int fboHeight) {
        for (int i = 0; i < 2; i++) {
            glBindFramebuffer(GL_FRAMEBUFFER, blurFbos[i]);
            glBindTexture(GL_TEXTURE_2D, blurColorTextures[i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, fboWidth, fboHeight, 0, GL_RGB, GL_FLOAT, NULL);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, blurColorTextures[i], 0);
        }

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            //TODO: Throw an exception or something
            std::cerr << "blurFbo is incomplete" << std::endl;
        }
    }

    void RenderManager::resizeViewport(int width, int height) {
        viewportWidth = width;
        viewportHeight = height;

        //Color texture
        glBindTexture(GL_TEXTURE_2D, fboColorTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, 0);

        //Bright texture
        glBindTexture(GL_TEXTURE_2D, fboBrightTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, 0);

        //Blur textures
        glBindTexture(GL_TEXTURE_2D, blurColorTextures[0]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, 0);
        glBindTexture(GL_TEXTURE_2D, blurColorTextures[1]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, 0);

        //Depth buffer
        glBindRenderbuffer(GL_RENDERBUFFER, fboDepthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    }

    void RenderManager::enqueueRenderCommand(IRenderCommand *command) {
        renderFifo.push_back(command);
    }

    void RenderManager::renderQueue(GLuint targetFramebuffer) {
        //Bind the FBO
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glViewport(0, 0, viewportWidth, viewportHeight);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // glEnable(GL_DEPTH_TEST);

        //Render everything to the FBO
        for (IRenderCommand *cmd : renderFifo) {
            cmd->renderManager = this;
            cmd->execute();
            delete cmd;
        }

        glBindVertexArray(fullscreenQuadVao);
        activeVao = fullscreenQuadVao;

        //Blur the blur FBO for bloom
        glActiveTexture(GL_TEXTURE0);

        bool horizontal = true, firstIteration = true;
        compositeBlurShaderProg->use();
        for (unsigned int i = 0; i < bloomSpread; i++) {
            glBindFramebuffer(GL_FRAMEBUFFER, blurFbos[horizontal]);
            compositeBlurShaderProg->setInt("horizontal", horizontal);
            glBindTexture(GL_TEXTURE_2D, firstIteration ? fboBrightTexture : blurColorTextures[!horizontal]);
            glDrawArrays(GL_TRIANGLES, 0, 6); //Draw fullscreen quad
            horizontal = !horizontal;
            if (firstIteration) firstIteration = false;
        }

        //Render the FBO contents to the screen
        glBindFramebuffer(GL_FRAMEBUFFER, targetFramebuffer);
        glViewport(0, 0, viewportWidth, viewportHeight);

        compositeShaderProg->use();
        activeShaderProgram = compositeShaderProg;

        // glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fboColorTexture);
        glUniform1i(compositeShaderTextureId, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, blurColorTextures[bloomSpread % 2 != 0]);
        glUniform1i(compositeShaderBloomTextureId, 1);

        // glBindVertexArray(fullscreenQuadVao); //The quad VAO is already bound
        // activeVao = fullscreenQuadVao;

        //Draw the triangles
        glDrawArrays(GL_TRIANGLES, 0, 6); //6 verts for 2 tris (As it's a fullscreen **quad**)

        //glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
        //glBindFramebuffer(GL_DRAW_FRAMEBUFFER, targetFramebuffer);
        //glBlitFramebuffer(0, 0, 1920, 1080, 0, 0, 1920, 1080, GL_COLOR_BUFFER_BIT, GL_NEAREST);

        //Clear the fifo
        renderFifo.clear();
    }

    void RenderManager::flushQueue() {
        for (IRenderCommand *cmd : renderFifo) {
            cmd->execute();
            delete cmd;
        }

        //Clear the fifo
        renderFifo.clear();
    }

    void RenderManager::checkErrors(const char *location) {
        GLenum err;
        while ((err = glGetError()) != GL_NO_ERROR) {
            //Errors occurred
            std::string errString;

            switch(err) {
                case GL_INVALID_OPERATION:
                    errString = "GL_INVALID_OPERATION";
                    break;
                case GL_INVALID_ENUM:
                    errString = "GL_INVALID_ENUM";
                    break;
                case GL_INVALID_VALUE:
                    errString = "GL_INVALID_VALUE";
                    break;
                case GL_OUT_OF_MEMORY:
                    errString = "GL_OUT_OF_MEMORY";
                    break;
                case GL_INVALID_FRAMEBUFFER_OPERATION:
                    errString = "GL_INVALID_FRAMEBUFFER_OPERATION";
                    break;
            }

            std::cerr << "GL Error: " << err << " - " << errString << " - Found at: " << location << std::endl;
        }
    }

    void RenderManager::unloadShaders() {
        glDeleteProgram(texturedShaderProg->getProgramId());
        glDeleteProgram(coloredShaderProg->getProgramId());
        glDeleteProgram(guiShaderProg->getProgramId());
        glDeleteProgram(compositeShaderProg->getProgramId());
        glDeleteProgram(compositeBlurShaderProg->getProgramId());

        delete texturedShaderProg;
        delete coloredShaderProg;
        delete guiShaderProg;
        delete compositeShaderProg;
        delete compositeBlurShaderProg;
    }

    void RenderManager::setBloomSpread(unsigned int bloomSpread) {
        if (bloomSpread == 1 && this->bloomSpread != 1) {
            //Clear the 2nd bloom framebuffer texture
            glBindFramebuffer(GL_FRAMEBUFFER, blurFbos[1]);
            glBindTexture(GL_TEXTURE_2D, blurColorTextures[1]);
            glClear(GL_COLOR_BUFFER_BIT);
        } else if (bloomSpread == 0 && this->bloomSpread != 0) {
            //Clear both bloom framebuffer textures
            glBindFramebuffer(GL_FRAMEBUFFER, blurFbos[0]);
            glBindTexture(GL_TEXTURE_2D, blurColorTextures[0]);
            glClear(GL_COLOR_BUFFER_BIT);
            glBindFramebuffer(GL_FRAMEBUFFER, blurFbos[1]);
            glBindTexture(GL_TEXTURE_2D, blurColorTextures[1]);
            glClear(GL_COLOR_BUFFER_BIT);
        }

        this->bloomSpread = bloomSpread;
    }

    unsigned int RenderManager::getBloomSpread() {
        return bloomSpread;
    }
}
