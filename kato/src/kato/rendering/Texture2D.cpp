#include "kato/rendering/Texture2D.hpp"
#include "stb/stb_image.h"
#include <iostream>

namespace Kato {
    Texture2D::Texture2D(GLuint textureId, glm::tvec2<uint32_t> size) :
        textureId(textureId),
        size(size) {}

    GLuint Texture2D::getTextureId() const {
        return textureId;
    }

    void Texture2D::use() const {
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    void Texture2D::use(GLenum activeTexture) const {
        glActiveTexture(activeTexture);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    glm::uvec2 Texture2D::getSize() {
        return size;
    }

    uint32_t Texture2D::getSizeX() {
        return size.x;
    }

    uint32_t Texture2D::getSizeY() {
        return size.y;
    }

    Texture2D* Texture2D::createTexture2D() {
        Texture2D *tex = new Texture2D();
        glGenTextures(1, &tex->textureId);

        return tex;
    }

    Texture2D* Texture2D::createTexture2DFromFile(
            boost::filesystem::path &texturePath,
            GLint internalFormat,
            GLenum format,
            GLint uWrapping,
            GLint vWrapping
            ) {
        int width, height, numChannels;

        stbi_set_flip_vertically_on_load(true);
        unsigned char *data = stbi_load(texturePath.string().c_str(), &width, &height, &numChannels, 0);

        if (!data) {
            std::cerr << "Failed to load texture" << std::endl;
            //TODO: Throw an exception or something
        }

        GLuint id;
        glGenTextures(1, &id);
        glBindTexture(GL_TEXTURE_2D, id);

        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, uWrapping);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, vWrapping);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);

        return new Texture2D(id, glm::tvec2<uint32_t>(width, height));
    }
}
