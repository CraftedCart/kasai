#include "kato/imguibinding/ImGuiSdlBinding.hpp"
#include "kato/EnginePaths.hpp"
#include "kato/Assertions.hpp"
#include <SDL2/SDL_syswm.h>
#include <iostream>

namespace Kato {
    ImGuiSdlBinding::ImGuiSdlBinding(SDL_Window *window, RenderManager *renderManager) :
        window(window),
        renderManager(renderManager) {
        //Setup back-end capabilities flags
        ImGuiIO& io = ImGui::GetIO();
        io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors; //We can honor GetMouseCursor() values (optional)
        io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos; //We can honor io.WantSetMousePos requests (optional, rarely used)

        //Some config stuff
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableSetMousePos;
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
        io.ConfigDockingWithShift = true;

        //Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
        io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
        io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
        io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
        io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
        io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
        io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
        io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
        io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
        io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
        io.KeyMap[ImGuiKey_Insert] = SDL_SCANCODE_INSERT;
        io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
        io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
        io.KeyMap[ImGuiKey_Space] = SDL_SCANCODE_SPACE;
        io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
        io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
        io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
        io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
        io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
        io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
        io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
        io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;

        io.SetClipboardTextFn = ImGuiSdlBinding::setClipboardText;
        io.GetClipboardTextFn = ImGuiSdlBinding::getClipboardText;
        io.ClipboardUserData = this;

        mouseCursors[ImGuiMouseCursor_Arrow] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
        mouseCursors[ImGuiMouseCursor_TextInput] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
        mouseCursors[ImGuiMouseCursor_ResizeAll] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL);
        mouseCursors[ImGuiMouseCursor_ResizeNS] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
        mouseCursors[ImGuiMouseCursor_ResizeEW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
        mouseCursors[ImGuiMouseCursor_ResizeNESW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
        mouseCursors[ImGuiMouseCursor_ResizeNWSE] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
        mouseCursors[ImGuiMouseCursor_Hand] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

        //Hard to see stuff w/ HDR, so remove almost all transparency
        ImGuiStyle& style = ImGui::GetStyle();
        style.Colors[ImGuiCol_WindowBg] = ImVec4(style.Colors[ImGuiCol_WindowBg].x, style.Colors[ImGuiCol_WindowBg].y, style.Colors[ImGuiCol_WindowBg].z, 0.98f);
    }

    ImGuiSdlBinding::~ImGuiSdlBinding() {
        //Destroy last known clipboard data
        if (clipboardTextData) SDL_free(clipboardTextData);

        //Destroy SDL mouse cursors
        for (ImGuiMouseCursor cursor_n = 0; cursor_n < ImGuiMouseCursor_COUNT; cursor_n++) {
            SDL_FreeCursor(mouseCursors[cursor_n]);
        }

        //Destroy buffers
        delete vbo;
        delete ebo;
    }

    const char* ImGuiSdlBinding::getClipboardText(void *instance) {
        ImGuiSdlBinding *binding = static_cast<ImGuiSdlBinding*>(instance);

        if (binding->clipboardTextData != nullptr) {
            SDL_free(binding->clipboardTextData);
        }

        binding->clipboardTextData = SDL_GetClipboardText();

        return binding->clipboardTextData;
    }

    void ImGuiSdlBinding::setClipboardText([[maybe_unused]] void *instance, const char *text) {
        SDL_SetClipboardText(text);
    }

    bool ImGuiSdlBinding::handleEvent(const SDL_Event &event) {
        ImGuiIO& io = ImGui::GetIO();
        switch (event.type) {
            case SDL_MOUSEWHEEL:
                if (event.wheel.x > 0) io.MouseWheelH += 1;
                if (event.wheel.x < 0) io.MouseWheelH -= 1;
                if (event.wheel.y > 0) io.MouseWheel += 1;
                if (event.wheel.y < 0) io.MouseWheel -= 1;
                return true;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) mousePressed[0] = true;
                if (event.button.button == SDL_BUTTON_RIGHT) mousePressed[1] = true;
                if (event.button.button == SDL_BUTTON_MIDDLE) mousePressed[2] = true;
                return true;
            case SDL_TEXTINPUT:
                io.AddInputCharactersUTF8(event.text.text);
                return true;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                int key = event.key.keysym.scancode;
                KATO_ASSERT(key >= 0 && key < IM_ARRAYSIZE(io.KeysDown));
                io.KeysDown[key] = (event.type == SDL_KEYDOWN);
                io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
                io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
                io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
                io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
                return true;
        }
        return false;
    }

    void ImGuiSdlBinding::newFrame(float deltaSeconds) {
        if (fontTexture == nullptr) loadRenderables();

        ImGuiIO& io = ImGui::GetIO();

        //Setup display size (every frame to accommodate for window resizing)
        int w, h;
        int displayW, displayH;
        SDL_GetWindowSize(window, &w, &h);
        SDL_GL_GetDrawableSize(window, &displayW, &displayH);
        io.DisplaySize = ImVec2((float) w, (float) h);
        io.DisplayFramebufferScale = ImVec2(w > 0 ? ((float) displayW / w) : 0, h > 0 ? ((float) displayH / h) : 0);

        //Setup time step
        io.DeltaTime = deltaSeconds;
    }

    void ImGuiSdlBinding::loadRenderables() {
        //Load shaders
        boost::filesystem::path shadersDir = EnginePaths::getEngineResourceDirectoryPath();
        shadersDir /= "shaders";

        boost::filesystem::path guiVertFile(shadersDir / "gui.vert");
        boost::filesystem::path guiFragFile(shadersDir / "gui.frag");
        guiShaderProg = ShaderProgram::createShaderProgram(guiVertFile, guiFragFile);

        //Create buffers
        vbo = VertexBuffer::createVertexBuffer(*renderManager);
        ebo = ElementBuffer::createElementBuffer(*renderManager);

        //Load font texture
        ImGuiIO& io = ImGui::GetIO();
        unsigned char* pixels;
        int width, height;
        //Load as RGBA 32-bits (75% of the memory is wasted, but default font is so small) because it is more likely to
        //be compatible with user's existing shaders. If your ImTextureId represent a higher-level concept than just a
        //GL texture id, consider calling GetTexDataAsAlpha8() instead to save on GPU memory.
        io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

        fontTexture = Texture2D::createTexture2D();
        fontTexture->use();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

        io.Fonts->TexID = (ImTextureID)(intptr_t) fontTexture;
    }

    void ImGuiSdlBinding::updateMouse() {
        ImGuiIO& io = ImGui::GetIO();

        // Set OS mouse position if requested (rarely used, only when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
        if (io.WantSetMousePos) {
            SDL_WarpMouseInWindow(window, (int) io.MousePos.x, (int) io.MousePos.y);
        } else {
            io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
        }

        int mx, my;
        Uint32 mouseButtons = SDL_GetMouseState(&mx, &my);
        //If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        io.MouseDown[0] = mousePressed[0] || (mouseButtons & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
        io.MouseDown[1] = mousePressed[1] || (mouseButtons & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
        io.MouseDown[2] = mousePressed[2] || (mouseButtons & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
        mousePressed[0] = mousePressed[1] = mousePressed[2] = false;

#if SDL_HAS_CAPTURE_MOUSE && !defined(__EMSCRIPTEN__)
        SDL_Window* focusedWindow = SDL_GetKeyboardFocus();
        if (g_Window == focusedWindow)
        {
            //SDL_GetMouseState() gives mouse position seemingly based on the last window entered/focused(?)
            //The creation of a new windows at runtime and SDL_CaptureMouse both seems to severely mess up with that, so we retrieve that position globally.
            int wx, wy;
            SDL_GetWindowPosition(focusedWindow, &wx, &wy);
            SDL_GetGlobalMouseState(&mx, &my);
            mx -= wx;
            my -= wy;
            io.MousePos = ImVec2((float) mx, (float) my);
        }

        //SDL_CaptureMouse() let the OS know e.g. that our imgui drag outside the SDL window boundaries shouldn't e.g. trigger the OS window resize cursor.
        //The function is only supported from SDL 2.0.4 (released Jan 2016)
        bool any_mouse_button_down = ImGui::IsAnyMouseDown();
        SDL_CaptureMouse(any_mouse_button_down ? SDL_TRUE : SDL_FALSE);
#else
        if (SDL_GetWindowFlags(window) & SDL_WINDOW_INPUT_FOCUS) {
            io.MousePos = ImVec2((float) mx, (float) my);
        }
#endif
    }

    void ImGuiSdlBinding::draw(ImDrawData *drawData) {
        //Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
        ImGuiIO& io = ImGui::GetIO();
        int fb_width = (int)(drawData->DisplaySize.x * io.DisplayFramebufferScale.x);
        int fb_height = (int)(drawData->DisplaySize.y * io.DisplayFramebufferScale.y);
        if (fb_width <= 0 || fb_height <= 0) return;
        drawData->ScaleClipRects(io.DisplayFramebufferScale);

        //Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, polygon fill
        glEnable(GL_BLEND);
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_SCISSOR_TEST);
        // glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //Setup viewport, orthographic projection matrix Our visible imgui space lies from drawData->DisplayPos (top
        //left) to drawData->DisplayPos+data_data->DisplaySize (bottom right). DisplayMin is typically (0,0) for
        //single viewport apps.
        // glViewport(0, 0, (GLsizei) fb_width, (GLsizei) fb_height);
        float L = drawData->DisplayPos.x;
        float R = drawData->DisplayPos.x + drawData->DisplaySize.x;
        float T = drawData->DisplayPos.y;
        float B = drawData->DisplayPos.y + drawData->DisplaySize.y;
        const glm::mat4 orthoProj = {
            { 2.0f/(R-L),   0.0f,         0.0f,   0.0f },
            { 0.0f,         2.0f/(T-B),   0.0f,   0.0f },
            { 0.0f,         0.0f,        -1.0f,   0.0f },
            { (R+L)/(L-R),  (T+B)/(B-T),  0.0f,   1.0f },
        };

        if (renderManager->activeShaderProgram != guiShaderProg) {
            guiShaderProg->use();
            renderManager->activeShaderProgram = guiShaderProg;
        }

        guiShaderProg->setInt("tex", 0);
        guiShaderProg->setMat4("transformMatrix", orthoProj);
        // glBindSampler(0, 0); // We use combined texture/sampler state. Applications using GL 3.3 may set that otherwise.

        //Recreate the VAO every time (This is to easily allow multiple GL contexts. VAOs are not shared among GL
        //contexts, and we don't track creation/deletion of windows so we don't have an obvious key to use to cache
        //them.)
        GLuint vao = 0;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        renderManager->activeVao = vao;
        vbo->use();
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*) offsetof(ImDrawVert, pos));
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*) offsetof(ImDrawVert, uv));
        glVertexAttribPointer(3, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*) offsetof(ImDrawVert, col));

        //Draw
        ImVec2 pos = drawData->DisplayPos;
        for (int n = 0; n < drawData->CmdListsCount; n++) {
            const ImDrawList* cmd_list = drawData->CmdLists[n];
            const ImDrawIdx* idx_buffer_offset = 0;

            vbo->use();
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

            ebo->use();
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

            for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
                const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
                if (pcmd->UserCallback) {
                    //User callback (registered via ImDrawList::AddCallback)
                    pcmd->UserCallback(cmd_list, pcmd);
                } else {
                    ImVec4 clip_rect = ImVec4(pcmd->ClipRect.x - pos.x, pcmd->ClipRect.y - pos.y, pcmd->ClipRect.z - pos.x, pcmd->ClipRect.w - pos.y);
                    if (clip_rect.x < fb_width && clip_rect.y < fb_height && clip_rect.z >= 0.0f && clip_rect.w >= 0.0f) {
                        // Apply scissor/clipping rectangle
                        glScissor((int)clip_rect.x, (int)(fb_height - clip_rect.w), (int)(clip_rect.z - clip_rect.x), (int)(clip_rect.w - clip_rect.y));

                        // Bind texture, Draw
                        Texture2D *texture = ((Texture2D*)(intptr_t)pcmd->TextureId);
                        if (renderManager->activeTextures[0] != texture) {
                            texture->use(GL_TEXTURE0);
                            renderManager->activeTextures[0] = ((Texture2D*)(intptr_t)pcmd->TextureId);
                        }

                        glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
                    }
                }
                idx_buffer_offset += pcmd->ElemCount;
            }
        }
        glDeleteVertexArrays(1, &vao);

        glEnable(GL_CULL_FACE);
        glDisable(GL_SCISSOR_TEST);

        updateMouse();
    }
}
