#include "kato/commands/EntityMoveDeltaCommand.hpp"

namespace Kato {
    EntityMoveDeltaCommand::EntityMoveDeltaCommand(Entity &ent, const glm::vec3 &delta) :
        ent(ent),
        delta(delta) {}

    void EntityMoveDeltaCommand::execute() {
        ent.getTransform().setPosition(ent.getTransform().getPosition() + delta);
    }
}
