#include "kato/EnginePaths.hpp"
#include <boost/dll.hpp>

namespace Kato::EnginePaths {
        boost::filesystem::path getExecutablePath() {
            return boost::dll::program_location();
        }

        boost::filesystem::path getExecutableDirectoryPath() {
            return boost::dll::program_location().parent_path();
        }

        boost::filesystem::path getInstallDirectoryPath() {
            return boost::dll::program_location().parent_path().parent_path();
        }

        boost::filesystem::path getResourceDirectoryPath() {
            boost::filesystem::path p = boost::dll::program_location().parent_path().parent_path();
            p /= "share";
            p /= "kato";
            return p;
        }

        boost::filesystem::path getEngineResourceDirectoryPath() {
            boost::filesystem::path p = getResourceDirectoryPath();
            p /= "engine";
            return p;
        }
}

