#include "kato/main.hpp"
#include "kato/rendering/commands/RenderStaticMeshEntityCommand.hpp"
#include "kato/rendering/commands/RenderImGuiCommand.hpp"
#include "kato/rendering/glplatform.hpp"
#include "kato/entities/StaticMeshEntity.hpp"
#include "kato/imguibinding/ImGuiSdlBinding.hpp"
#include "kato/assets/AssetManager.hpp"
#include "kato/EnginePaths.hpp"
#include "kato/EngineInstance.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL.h>
#include <chrono>
#include <iostream>
#include <cmath>

//TEMP probably
#include <sstream>
#include <unordered_map>
#include <boost/algorithm/string.hpp>

namespace Kato {
    std::chrono::time_point<std::chrono::high_resolution_clock> prevFrameTime;

    int launchEngine(int argc, char *argv[]) {
        std::cout << "Konnichiwa, sekai" << std::endl;

        EngineInstance &engine = EngineInstance::initInstance(argc, argv);

        Kato::MaterialParameter tintParam = {"tint", Kato::VEC4, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)};

        StaticMeshEntity *bgEntity = new StaticMeshEntity(engine.getRenderManager().planeStaticMesh);
        StaticMeshEntity *cursorEntity = new StaticMeshEntity(engine.getRenderManager().planeStaticMesh, engine.getRenderManager().mat);
        cursorEntity->addMaterialParameter(tintParam);
        cursorEntity->getTransform().setScale(glm::vec3(engine.getRenderManager().texture->getSize(), 1.0f));

        //Render loop
        float currentSecond = 0.0f;
        bool loop = true;
        bool isFirst = true;
        float deltaSeconds;
        float realDeltaSeconds; //Usually the same as deltaSeconds unless we use a fixed timestep
        while (loop) {
            auto nowTime = std::chrono::high_resolution_clock::now();
            if (isFirst) prevFrameTime = nowTime;
            std::chrono::duration<double> deltaTime = nowTime - prevFrameTime;
            float deltaMilliseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(deltaTime).count() / 1000000.0f;
            realDeltaSeconds = deltaMilliseconds / 1000.0f;
            // std::cout << "milli: " << deltaMilliseconds
            //           << " fps: " << 1.0f / (std::chrono::duration_cast<std::chrono::nanoseconds>(deltaTime).count() / 1000000000.0f)
            //           << std::endl;
            prevFrameTime = nowTime;

            if (engine.getFixedTimestep() < 0.0f) {
                deltaSeconds = realDeltaSeconds;

                if (!isFirst) {
                    currentSecond += deltaTime.count();
                }
            } else {
                deltaSeconds = engine.getFixedTimestep();
                currentSecond += deltaSeconds;
            }

            isFirst = false;

            //Refresh ImGui
            engine.getImGuiBinding().newFrame(realDeltaSeconds);
            ImGui::NewFrame();

            //Poll for events
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                } else if (event.type == SDL_KEYDOWN) {
                    switch (event.key.keysym.sym) {
                        // case SDLK_ESCAPE:
                        //     loop = false;
                        //     break;
                    }
                } else if (event.type == SDL_WINDOWEVENT) {
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_RESIZED:
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            if (event.window.windowID == SDL_GetWindowID(engine.getSdlWindow())) {
                                engine.getRenderManager().resizeViewport(event.window.data1, event.window.data2);
                            }
                            break;
                    }
                }

                engine.handleEvent(event);
            }

            engine.tick(deltaSeconds, realDeltaSeconds);

            //Render
            int w, h;
            SDL_GetWindowSize(engine.getSdlWindow(), &w, &h);
            glViewport(0, 0, w, h);

            int mouseX;
            int mouseY;
            SDL_GetGlobalMouseState(&mouseX, &mouseY);
            int winX;
            int winY;
            SDL_GetWindowPosition(engine.getSdlWindow(), &winX, &winY);
            mouseX -= winX;
            mouseY -= winY;

            glm::mat4 projection = glm::ortho(0.0f, (float) w, 0.0f, (float) h, -1000.0f, 1000.0f);

            //Draw the GUI
            engine.getRenderManager().enqueueRenderCommand(new RenderImGuiCommand(&engine.getImGuiBinding()));

            //Draw the cursor
            cursorEntity->getTransform().setPosition(glm::vec3(
                                                             mouseX + (cursorEntity->getTransform().getScale().x * 0.5f),
                                                             h - mouseY - (cursorEntity->getTransform().getScale().y * 0.5f),
                                                             0.0f
                                                             )
                    );
            engine.getRenderManager().enqueueRenderCommand(new RenderStaticMeshEntityCommand(cursorEntity, projection));

            engine.getRenderManager().renderQueue(0);
            engine.getRenderManager().checkErrors("End of the render loop");

            SDL_GL_SwapWindow(engine.getSdlWindow()); //This also seems to limit the framerate at the monitor refresh rate unless we do SDL_GL_SetSwapInterval

            nowTime = std::chrono::high_resolution_clock::now();
            std::chrono::duration<float> frameTime = nowTime - prevFrameTime;

            if (engine.getFixedTimestep() > 0.0f && frameTime.count() < engine.getFixedTimestep()) {
                SDL_Delay((engine.getFixedTimestep() - frameTime.count()) * 1000.0f);
            }
        }

        engine.shutdown();

        return 0;
    }
}
