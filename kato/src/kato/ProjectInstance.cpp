#include "kato/ProjectInstance.hpp"

namespace Kato {
    ProjectInstance::ProjectInstance(EngineInstance &engine) : engine(&engine) {}
    ProjectInstance::~ProjectInstance() {}

    void ProjectInstance::init() {}
    void ProjectInstance::tick([[maybe_unused]] float deltaSeconds) {}
    void ProjectInstance::handleEvent([[maybe_unused]] const SDL_Event &event) {}
    void ProjectInstance::shutdown() {}

    World* ProjectInstance::getWorld() {
        return world;
    }
}
