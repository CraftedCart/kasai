#include "kato/entities/Entity.hpp"
#include "kato/Assertions.hpp"
#include <algorithm>
#include <iostream>

namespace Kato {
    Entity::Entity(const std::string &name) : name(name) {}

    Entity::~Entity() {
        //Make a copy of the vector as deleting children will modify our own vector
        //As deleting children will remove a child's entry from our vector
        std::vector<Entity*> toDelete(children);
        for (Entity *child : toDelete) delete child;

        //Remove from parent if we have one
        if (parent != nullptr) {
            removeFromParent(false);
        }
    }

    void Entity::tick([[maybe_unused]] float deltaSeconds) {}

    std::string& Entity::getName() {
        return name;
    }

    const std::string& Entity::getName() const {
        return name;
    }

    void Entity::setName(const std::string &name) {
        this->name = name;
    }

    Transform& Entity::getTransform() {
        return transform;
    }

    const Transform& Entity::getTransform() const {
        return transform;
    }

    void Entity::setTransform(const Transform &transform) {
        this->transform = transform;
    }

    std::vector<Entity*>& Entity::getChildren() {
        return children;
    }

    const std::vector<Entity*>& Entity::getChildren() const {
        return children;
    }

    void Entity::addChild(Entity *child) {
        KATO_ASSERT(child != this, "Attempted to add an entity as a child of itself");

        children.push_back(child);
        child->parent = this;
    }

    void Entity::removeChild(Entity *child, bool shouldDelete) {
        auto it = std::find(children.begin(), children.end(), child);
        if (it != children.end()) children.erase(it);
        if (shouldDelete) delete child;
    }

    void Entity::removeFromParent(bool shouldDelete) {
        parent->removeChild(this, shouldDelete);
    }

    void Entity::clearChildren() {
        for (Entity *child : children) delete child;
        children.clear();
    }

    Entity* Entity::getParent() {
        return parent;
    }

    const Entity* Entity::getParent() const {
        return parent;
    }

    const glm::mat4 Entity::getWorldTransformMatrix() const {
        glm::mat4 mtx = transform.getTransformMatrix();

        if (parent != nullptr) {
            mtx = parent->getWorldTransformMatrix() * mtx;
        }

        return mtx;
    }
}
