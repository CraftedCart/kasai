#include "kato/entities/StaticMeshEntity.hpp"

namespace Kato {
    StaticMeshEntity::StaticMeshEntity(StaticMeshAsset *staticMeshAsset, MaterialAsset *overriddenMaterialAsset) :
        staticMeshAsset(staticMeshAsset),
        overriddenMaterialAsset(overriddenMaterialAsset) {}

    StaticMeshEntity::StaticMeshEntity(
            const std::string &name,
            StaticMeshAsset *staticMeshAsset,
            MaterialAsset *overriddenMaterialAsset
            ) :
        Entity(name),
        staticMeshAsset(staticMeshAsset),
        overriddenMaterialAsset(overriddenMaterialAsset) {
    }

    void StaticMeshEntity::setStaticMeshAsset(StaticMeshAsset *staticMeshAsset) {
        this->staticMeshAsset = staticMeshAsset;
    }

    StaticMeshAsset* StaticMeshEntity::getStaticMeshAsset() {
        return staticMeshAsset;
    }

    const StaticMeshAsset* StaticMeshEntity::getStaticMeshAsset() const {
        return staticMeshAsset;
    }

    void StaticMeshEntity::setOverriddenMaterialAsset(MaterialAsset *overriddenMaterialAsset) {
        this->overriddenMaterialAsset = overriddenMaterialAsset;
    }

    MaterialAsset* StaticMeshEntity::getMaterialAsset() {
        if (overriddenMaterialAsset == nullptr) {
            return staticMeshAsset->getMaterialAsset();
        } else {
            return overriddenMaterialAsset;
        }
    }

    const MaterialAsset* StaticMeshEntity::getMaterialAsset() const {
        if (overriddenMaterialAsset == nullptr) {
            return staticMeshAsset->getMaterialAsset();
        } else {
            return overriddenMaterialAsset;
        }
    }

    void StaticMeshEntity::addMaterialParameter(MaterialParameter &param) {
        materialParameters.push_back(param);
    }

    std::vector<MaterialParameter>& StaticMeshEntity::getMaterialParameters() {
        return materialParameters;
    }

    const std::vector<MaterialParameter>& StaticMeshEntity::getMaterialParameters() const {
        return materialParameters;
    }

    int StaticMeshEntity::getRenderLayer() const {
        return renderLayer;
    }

    void StaticMeshEntity::setRenderLayer(int renderLayer) {
        this->renderLayer = renderLayer;
    }
}
