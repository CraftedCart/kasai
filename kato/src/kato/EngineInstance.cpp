#include "kato/EngineInstance.hpp"
#include "kato/assets/AssetManager.hpp"
#include "kato/editor/EditorMainView.hpp"
#include "kato/Assertions.hpp"
#include "katoproject/Project.hpp"
#include <iostream>

namespace Kato {
    //Static consts
    static const unsigned int SCREEN_WIDTH = 640;
    static const unsigned int SCREEN_HEIGHT = 480;

    //Init class statics
    EngineInstance *EngineInstance::instance = nullptr;

    EngineInstance::EngineInstance(int argc, char *argv[]) {
        for (int i = 0; i < argc; i++) {
            commandArgs.push_back(argv[i]);
        }

        //Check if we launched with --editor
        if (std::find(commandArgs.begin(), commandArgs.end(), "--editor") != commandArgs.end()) {
            usingEditor = true;
            editorView = new EditorMainView(*this);
        }
    }

    EngineInstance& EngineInstance::initInstance(int argc, char *argv[]) {
        KATO_ASSERT(instance == nullptr); //You shouldn't instantiate the engine twice
        instance = new EngineInstance(argc, argv);

        //Setup asset directories
        AssetManager::getInstance().getRootDir()->addChild(new DirectoryAsset("kato"));

        //Init SDL
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
            std::cerr << "Failed to initialize SDL - " << SDL_GetError() << std::endl;
            std::abort();
        }

        instance->sdlWindow = SDL_CreateWindow(
                "Kato",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                SCREEN_WIDTH,
                SCREEN_HEIGHT,
                SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
                );
        if (instance->sdlWindow == nullptr) {
            std::cerr << "Failed to create SDL window: " << SDL_GetError() << std::endl;
            std::abort();
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        SDL_GL_CreateContext(instance->sdlWindow);

        glewExperimental = true;
        glewInit();

        int w, h;
        SDL_GetWindowSize(instance->sdlWindow, &w, &h);

        instance->renderManager.init(w, h);

        ImGui::CreateContext();
        instance->imGuiBinding = new ImGuiSdlBinding(instance->sdlWindow, &instance->renderManager);

        SDL_GL_SetSwapInterval(0);
        SDL_ShowCursor(SDL_DISABLE);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        instance->project = new KatoProject::Project(*instance);
        instance->project->init();

        return *instance;
    }

    EngineInstance* EngineInstance::getInstance() {
        return instance;
    }

    void EngineInstance::tick(float deltaSeconds, float realDeltaSeconds) {
        if (usingEditor) editorView->tick(deltaSeconds, realDeltaSeconds);
        project->tick(deltaSeconds);
    }

    void EngineInstance::handleEvent(const SDL_Event &event) {
        imGuiBinding->handleEvent(event);
        project->handleEvent(event);
    }

    void EngineInstance::requestQuit() {
        SDL_Event sdlevent;
        sdlevent.type = SDL_QUIT;

        SDL_PushEvent(&sdlevent);
    }

    void EngineInstance::shutdown() {
        project->shutdown();
        delete project;

        if (usingEditor) delete editorView;

        delete imGuiBinding;
        renderManager.destroy();

        SDL_DestroyWindow(sdlWindow);
        SDL_Quit();
    }

    bool EngineInstance::isUsingEditor() {
        return usingEditor;
    }

    ProjectInstance* EngineInstance::getProject() {
        return project;
    }

    SDL_Window* EngineInstance::getSdlWindow() {
        return sdlWindow;
    }

    RenderManager& EngineInstance::getRenderManager() {
        return renderManager;
    }

    ImGuiSdlBinding& EngineInstance::getImGuiBinding() {
        return *imGuiBinding;
    }

    glm::ivec2 EngineInstance::getViewportSize() {
        glm::ivec2 size;
        SDL_GetWindowSize(sdlWindow, &size.x, &size.y);
        return size;
    }

    void EngineInstance::setFixedTimestep(float fixedTimestep) {
        this->fixedTimestep = fixedTimestep;
    }

    float EngineInstance::getFixedTimestep() {
        return fixedTimestep;
    }

    const std::vector<std::string>& EngineInstance::getCommandArgs() {
        return commandArgs;
    }
}
