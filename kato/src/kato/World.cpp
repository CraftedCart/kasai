#include "kato/World.hpp"
#include "kato/rendering/commands/RenderStaticMeshEntityCommand.hpp"
#include "kato/EngineInstance.hpp"

namespace Kato {
    World::World(EngineInstance &engine) : engine(engine) {
        rootEntity = new Entity("rootEntity");
    }

    World::~World() {
        delete rootEntity;
    }

    void World::recursiveTickEntities(Entity *ent, float deltaSeconds) {
        ent->tick(deltaSeconds);

        std::vector<Entity*> children = ent->getChildren(); // Copy the children vector in case this vec changes in size while iterating over it
        for (Entity *childEnt : children) {
            recursiveTickEntities(childEnt, deltaSeconds);
        }
    }

    void World::recursiveFindDrawableEntities(Entity *ent, float deltaSeconds, std::vector<StaticMeshEntity*> &found) {
        if (StaticMeshEntity *meshEnt = dynamic_cast<StaticMeshEntity*>(ent)) {
            found.push_back(meshEnt);
        }

        for (Entity *childEnt : ent->getChildren()) {
            recursiveFindDrawableEntities(childEnt, deltaSeconds, found);
        }
    }

    void World::tick(float deltaSeconds) {
        recursiveTickEntities(rootEntity, deltaSeconds);
    }

    void World::draw(float deltaSeconds) {
        //Find drawable entities
        std::vector<StaticMeshEntity*> toDraw;
        recursiveFindDrawableEntities(rootEntity, deltaSeconds, toDraw);

        //Sort drawable entities, back-to-front
        std::sort(
                toDraw.begin(),
                toDraw.end(),
                [](const StaticMeshEntity *lhs, const StaticMeshEntity *rhs) {
                    return lhs->getRenderLayer() < rhs->getRenderLayer();
                }
                );

        //TODO: Get proj matrix from a camera or something
        glm::mat4 projection;
        const glm::ivec2 viewport = engine.getViewportSize();

        if (fixedSceneSize.x > 0.0f) {
            const float aspectRatio = ((float) viewport.x) / viewport.y;
            const float width = fixedSceneSize.y * aspectRatio;
            const float xOffset = (width - fixedSceneSize.x) * 0.5f; //For centering
            projection = glm::ortho(-xOffset, width - xOffset, 0.0f, fixedSceneSize.y, -1000.0f, 1000.0f);
        } else {
            projection = glm::ortho(0.0f, (float) viewport.x, 0.0f, (float) viewport.y, -1000.0f, 1000.0f);
        }

        //Draw everything
        for (StaticMeshEntity *meshEnt : toDraw) {
            engine.getRenderManager().enqueueRenderCommand(new RenderStaticMeshEntityCommand(meshEnt, projection));
        }
    }

    void World::addEntity(Entity *ent) {
        rootEntity->addChild(ent);
    }

    Entity* World::getRootEntity() {
        return rootEntity;
    }

    const glm::vec2& World::getFixedSceneSize() const {
        return fixedSceneSize;
    }

    glm::vec2& World::getFixedSceneSize() {
        return fixedSceneSize;
    }

    void World::setFixedSceneSize(glm::vec2 fixedSceneSize) {
        this->fixedSceneSize = fixedSceneSize;
    }
}
