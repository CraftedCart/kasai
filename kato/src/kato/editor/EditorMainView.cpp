#include "kato/editor/EditorMainView.hpp"
#include "kato/assets/AssetManager.hpp"
#include "imgui.h"
#include <glm/glm.hpp>
#include <vector>
#include <algorithm>

namespace Kato {
    EditorMainView::EditorMainView(EngineInstance &engine) : engine(&engine) {}

    void EditorMainView::tick([[maybe_unused]] float deltaSeconds, float realDeltaSeconds) {
        //Set up the global dock space
        ImGui::DockSpaceOverViewport(nullptr, ImGuiDockNodeFlags_PassthruDockspace | ImGuiDockNodeFlags_NoDockingInCentralNode);

        drawFrameGraph(realDeltaSeconds);
        drawAssetBrowser();
        drawWorldOutliner();
        ImGui::ShowDemoWindow();
    }

    void EditorMainView::drawFrameGraph(float realDeltaSeconds) {
        static std::vector<float> vec;
        vec.push_back(realDeltaSeconds);
        if (vec.size() > 500) {
            vec.erase(vec.begin());
        }

        float maxFrameTime = *std::max_element(std::begin(vec), std::end(vec));
        float minFrameTime = *std::min_element(std::begin(vec), std::end(vec));
        static float interpMax = 1.0f;
        static float interpMin = 0.0f;

        if (minFrameTime < interpMin) {
            interpMin = glm::mix(interpMin, minFrameTime, 4.0f * realDeltaSeconds);
        } else {
            interpMin = glm::mix(interpMin, minFrameTime, 4.0f * realDeltaSeconds);
        }

        if (maxFrameTime > interpMax) {
            interpMax = glm::mix(interpMax, maxFrameTime, 4.5f * realDeltaSeconds);
        } else {
            interpMax = glm::mix(interpMax, maxFrameTime, 4.0f * realDeltaSeconds);
        }

        //Draw frame times
        ImGui::Begin("Frames");
        ImGui::PlotLines("Frame Times", vec.data(), vec.size(), 0.0f, "", interpMin, interpMax, ImVec2(0, 256), sizeof(float));
        ImGui::Text("Min: %f, Max: %f", interpMin, interpMax);

        int bloomSpread = engine->getRenderManager().getBloomSpread();
        ImGui::SliderInt("Bloom spread", &bloomSpread, 0, 64);
        engine->getRenderManager().setBloomSpread(bloomSpread);

        float fixedTimestep = engine->getFixedTimestep();
        ImGui::SliderFloat("Fixed timestep", &fixedTimestep, -0.0001f, 0.1f, "%.8f");
        engine->setFixedTimestep(fixedTimestep);

        ImGui::End();
    }

    void EditorMainView::recursiveDrawAssetTree(DirectoryAsset *dir) {
        if (ImGui::TreeNode(dir->getName().c_str())) {
            for (AbstractAsset *asset : dir->getChildren()) {
                if (DirectoryAsset *childDir = dynamic_cast<DirectoryAsset*>(asset)) {
                    recursiveDrawAssetTree(childDir);
                } else {
                    ImGui::Text("%s", asset->getName().c_str());
                }
            }

            ImGui::TreePop();
        }
    }

    void EditorMainView::recursiveDrawWorldOutlinerTree(Entity *ent) {
        ImGui::PushID(ent);

        if (ImGui::TreeNode(ent->getName().c_str())) {
            for (Entity *childEnt : ent->getChildren()) {
                recursiveDrawWorldOutlinerTree(childEnt);
            }

            ImGui::TreePop();
        }
        ImGui::PopID();
    }

    void EditorMainView::drawAssetBrowser() {
        ImGui::Begin("Assets");
        recursiveDrawAssetTree(AssetManager::getInstance().getRootDir());
        ImGui::End();
    }

    void EditorMainView::drawWorldOutliner() {
        ImGui::Begin("World outliner");
        recursiveDrawWorldOutlinerTree(engine->getProject()->getWorld()->getRootEntity());
        ImGui::End();
    }
}
