#include "kato/Assertions.hpp"
#include <boost/stacktrace.hpp>
#include <iostream>
#include <cstdlib>

#ifndef NDEBUG

namespace Kato {
    void assertFailed(const std::string &expr, const std::string &file, const unsigned int line) {
        std::cerr << file << ":" << line << ": Assertion failed: " << expr << std::endl;
        std::cerr << "Backtrace:\n" << boost::stacktrace::stacktrace() << std::endl;
        std::abort();
    }

    void assertFailed(const std::string &expr, const std::string &message, const std::string &file, const unsigned int line) {
        std::cerr << file << ":" << line << ": Assertion failed: " << expr << ": " << message << std::endl;
        std::cerr << "Backtrace:\n" << boost::stacktrace::stacktrace() << std::endl;
        std::abort();
    }
}

#endif

