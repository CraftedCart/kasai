#include "kato/FileUtils.hpp"
#include <fstream>
#include <sstream>
#include <iostream>

namespace Kato::FileUtils {
    std::string readIfstreamToString(std::ifstream &in) {
        std::stringstream sstr;
        sstr << in.rdbuf();
        return sstr.str();
    }
}

