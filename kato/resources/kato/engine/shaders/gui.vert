#version 330 core

layout(location = 0) in vec2 inPos;
layout(location = 2) in vec2 inUv;
layout(location = 3) in vec4 inColor;

uniform mat4 transformMatrix;

out vec2 uv;
out vec4 color;

void main() {
    uv = inUv;
    color = inColor;
    gl_Position = transformMatrix * vec4(inPos.xy, 0,1);
}

