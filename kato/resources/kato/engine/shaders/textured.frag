#version 330 core

in vec2 uv;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 brightColor;

uniform sampler2D sampler;
uniform vec4 tint;

void main() {
    fragColor = texture(sampler, uv) * tint;

    if (fragColor.a == 0) discard;

    //Check whether fragment output is higher than threshold, if so output as brightness color
    float brightness = dot(fragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1) {
        brightColor = fragColor;
    } else {
        brightColor = vec4(0.0, 0.0, 0.0, fragColor.a);
    }
}
