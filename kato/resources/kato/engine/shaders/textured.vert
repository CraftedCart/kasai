#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 2) in vec2 inUv;

out vec2 uv;

uniform mat4 transformMatrix;

void main() {
    gl_Position = transformMatrix * vec4(pos, 1.0);

    uv = inUv;
}
