#version 330 core

in vec2 uv;

out vec4 fragColor;

uniform sampler2D texSampler;
uniform sampler2D bloomTexSampler;

void main() {
    // const float gamma = 2.2;
    const float gamma = 1.05;
    const float exposure = 1.0;

    vec3 hdrColor = texture(texSampler, uv).rgb;
    vec3 bloomColor = texture(bloomTexSampler, uv).rgb;
    hdrColor += bloomColor; //Additive blending
    //Tone mapping
    vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
    //Also gamma correct while we're at it
    result = pow(result, vec3(1.0 / gamma));
    fragColor = vec4(result, 1.0);
}

