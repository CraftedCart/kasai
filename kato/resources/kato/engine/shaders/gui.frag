#version 330 core

in vec2 uv;
in vec4 color;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 brightColor;

uniform sampler2D tex;

void main() {
    fragColor = color * texture(tex, uv.st);
    brightColor = vec4(0.0, 0.0, 0.0, 1.0);
}

